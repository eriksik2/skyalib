

macro(skyalib_add_project project_name source_dir header_dir link include cflags ldflags)
project(${project_name})

# Find the source code files for this project
file(GLOB_RECURSE SOURCES "${source_dir}/*.c" "${source_dir}/*.cpp")
file(GLOB_RECURSE HEADERS "${header_dir}/*.h" "${header_dir}/*.hpp")
file(GLOB_RECURSE INLINES "${header_dir}/*.inl")

# Generate a dummy source file if this is a header only library
set(CMAKE_DUMMY "${CMAKE_CURRENT_SOURCE_DIR}/${source_dir}/_cmake_dummy.cpp")
if("${SOURCES}" STREQUAL "")
    if(NOT EXISTS ${CMAKE_DUMMY})
        file(WRITE ${CMAKE_DUMMY} "// File generated by Cmake. Do not use this in your project.\n")
    endif()
    set(SOURCES ${CMAKE_DUMMY})
elseif(NOT "${SOURCES}" STREQUAL ${CMAKE_DUMMY})
    if(EXISTS ${CMAKE_DUMMY})
        file(REMOVE ${CMAKE_DUMMY})
    endif()
    file(GLOB_RECURSE SOURCES "${source_dir}/*.c" "${source_dir}/*.cpp")
endif()

# add a target for this project
add_library(${project_name} STATIC ${SOURCES} ${HEADERS} ${INLINES})

# Define source groups for IDEs
source_group(TREE "${CMAKE_CURRENT_SOURCE_DIR}/${header_dir}" PREFIX "include" FILES ${HEADERS})
source_group(TREE "${CMAKE_CURRENT_SOURCE_DIR}/${header_dir}" PREFIX "source" FILES ${INLINES})
source_group(TREE "${CMAKE_CURRENT_SOURCE_DIR}/${source_dir}" PREFIX "source" FILES ${SOURCES})

# Add ldflags and link libs to target
target_link_libraries(${project_name} ${ldflags} ${link})

# Add includes to target
target_include_directories(${project_name} ${include})

# Add cflags to target
if("${cflags}")
    target_compile_options(${project_name} ${cflags})
endif()

# Add debug defines based on build type
target_compile_options(${project_name} PUBLIC $<$<OR:$<CONFIG:DEBUG>,$<CONFIG:RELWITHDEBINFO>>:-UNDEBUG -DDEBUG>)
target_compile_options(${project_name} PUBLIC $<$<OR:$<CONFIG:RELEASE>,$<CONFIG:MINSIZERELEASE>>:-UDEBUG -DNDEBUG>)

# Add test target if test dir exists
if(IS_DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}/test")
    file(GLOB TESTS "test/*.cpp" "test/*.h")
    if(NOT "${TESTS}" STREQUAL "")
        add_executable(${project_name}_test ${TESTS})
        target_link_libraries(${project_name}_test ${project_name} gtest)
        enable_testing()
        add_test(NAME ${project_name}_test COMMAND ${project_name}_test)
    endif()
endif()

# Add performance test target if perf dir exists
if(IS_DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}/perf")
    file(GLOB PERFS "perf/*.cpp" "perf/*.h")
    if(NOT "${PERFS}" STREQUAL "")
        add_executable(${project_name}_perf ${PERFS})
        target_link_libraries(${project_name}_perf ${project_name} picobench)
        enable_testing()
        add_test(NAME ${project_name}_perf COMMAND ${project_name}_perf)
    endif()
endif()
endmacro(skyalib_add_project)
