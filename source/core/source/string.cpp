#include "sk/string.h"

#include <assert.h>

#include <stdio.h>
#include <string.h> // memcpy, memmove
#include <wchar.h> // wchar_t
#include <ctype.h> // toupper, tolower
#include <wctype.h> // towupper, towlower


namespace sk {
    namespace detail {
        auto strlen(const char* s1) { return ::strlen(s1); }
        auto strlen(const wchar_t* s1) { return ::wcslen(s1); }
        auto strcmp(const char* s1, const char* s2) { return ::strcmp(s1, s2); }
        auto strcmp(const wchar_t* s1, const wchar_t* s2) { return ::wcscmp(s1, s2); }
        auto toupper(char c1) { return ::toupper(c1); }
        auto toupper(wchar_t c1) { return ::towupper(c1); }
        auto tolower(char c1) { return ::tolower(c1); }
        auto tolower(wchar_t c1) { return ::towlower(c1); }
        auto strspn(const char* s1, const char* s2) { return ::strspn(s1, s2); }
        auto strspn(const wchar_t* s1, const wchar_t* s2) { return ::wcsspn(s1, s2); }
        auto strchr(const char* s1, const char c1) { return ::strchr(s1, c1); }
        auto strchr(char* s1, char c1) { return ::strchr(s1, c1); }
        auto strchr(const wchar_t* s1, const wchar_t c1) { return ::wcschr(s1, c1); }
        auto strchr(wchar_t* s1, wchar_t c1) { return ::wcschr(s1, c1); }
        auto strstr(const char* s1, const char* s2) { return ::strstr(s1, s2); }
        auto strstr(char* s1, const char* s2) { return ::strstr(s1, s2); }
        auto strstr(const wchar_t* s1, const wchar_t* s2) { return ::wcsstr(s1, s2); }
        auto strstr(wchar_t* s1, const wchar_t* s2) { return ::wcsstr(s1, s2); }
    }
    template<> const char* string_template<char>::nothing = "";
    template<> const wchar_t* string_template<wchar_t>::nothing = L"";

    template<> const char* string_template<char>::whitespace = "\u0009\u000A\u000B\u000C\u000D\u0020\u00A0";
    template<> const wchar_t* string_template<wchar_t>::whitespace = L"\u0009\u000A\u000B\u000C\u000D\u0020\u00A0\u1680\u180E\u2000\u2001\u2002\u2003\u2004\u2005\u2006\u2007\u2008\u2009\u200A\u200B\u202F\u205F\u3000\uFEFF";

    template<class Tchar>
    string_template<Tchar>::string_template() {
        this->set_capacity(16);
        this->m_size = 1;
        this->m_array[0] = '\0';
    }

    template<class Tchar>
    string_template<Tchar>::string_template(int capacity) : dynarray<Tchar>::dynarray(capacity) {
        if(this->m_capacity <= 0) return;
        this->m_size = 1;
        this->m_array[0] = '\0';
    }

    template<class Tchar>
    string_template<Tchar>::string_template(const Tchar* from)
        : string_template(from, static_cast<int>(detail::strlen(from))) {}

    template<class Tchar>
    string_template<Tchar>::string_template(const Tchar* from, int length) {
        this->set_capacity(length + 1);
        this->m_size = length + 1;
        memcpy(this->m_array, from, this->m_size*sizeof(Tchar));
        this->m_array[length] = '\0';
    }

    template<class Tchar>
    string_template<Tchar>& string_template<Tchar>::operator=(const Tchar* from) {
        this->set_capacity(static_cast<int>(detail::strlen(from)) + 1);
        this->m_size = this->m_capacity;
        memcpy(this->m_array, from, this->m_size*sizeof(Tchar));
        return *this;
    }

    template<class Tchar>
    const Tchar* string_template<Tchar>::c_str() const { return this->m_array; }
    template<class Tchar>
    int string_template<Tchar>::length() const { return this->m_size - 1; }
    template<class Tchar>
    int string_template<Tchar>::size() const { return this->m_size - 1; }
    template<class Tchar>
    int string_template<Tchar>::bytes() const { return (this->m_size-1)*sizeof(Tchar); }
    template<class Tchar>
    Tchar* string_template<Tchar>::end() const { return this->m_array + this->m_size - 1; }
    template<class Tchar>
    Tchar& string_template<Tchar>::back() const {
        if(this->m_size <= 1) return this->front();
        return *(this->m_array + this->m_size - 2);
    }
    template<class Tchar>
    bool string_template<Tchar>::empty() const { return this->m_size <= 1; }


    template<class Tchar>
    int string_template<Tchar>::compare(const Tchar* str) const {
        return detail::strcmp(this->m_array, str);
    }
    template<class Tchar>
    bool string_template<Tchar>::equals(const Tchar* str, int size) const {
        if(str == this->m_array) return true;
        if(size != this->m_size-1) return false;
        for(int i = 0; i < size; ++i)
            if(this->m_array[i] != str[i]) return false;
        return true;
    }
    template<class Tchar>
    bool string_template<Tchar>::equals(const Tchar* str) const {
        return equals(str, static_cast<int>(detail::strlen(str)));
    }
    template<class Tchar>
    bool string_template<Tchar>::equals(const string_template& str) const {
        return equals(str.c_str(), str.size());
    }

    template<class Tchar>
    string_template<Tchar>& string_template<Tchar>::toupper() {
        for(int i = 0; i < this->size(); ++i)
            this->m_array[i] = detail::toupper(this->m_array[i]);
        return *this;
    }

    template<class Tchar>
    string_template<Tchar>& string_template<Tchar>::tolower() {
        for(int i = 0; i < this->size(); ++i)
            this->m_array[i] = detail::tolower(this->m_array[i]);
        return *this;
    }

    template<class Tchar>
    string_template<Tchar>& string_template<Tchar>::pad_start(Tchar ch, int count) {
        int new_size = this->m_size + count;
        while(new_size > this->m_capacity) this->set_capacity(this->m_capacity?this->m_capacity*2:16);
        memmove(this->m_array + count, this->m_array, this->m_size*sizeof(Tchar));
        memset(this->m_array, ch, count*sizeof(Tchar));
        this->m_size = new_size;
        return *this;
    }

    template<class Tchar>
    string_template<Tchar>& string_template<Tchar>::pad_end(Tchar ch, int count) {
        int new_size = this->m_size + count;
        while(new_size > this->m_capacity) this->set_capacity(this->m_capacity?this->m_capacity*2:16);
        memset(this->m_array+this->m_size-1, ch, count*sizeof(Tchar));
        this->m_size = new_size;
        *(this->m_array+this->m_size-1) = '\0';
        return *this;
    }

    template<class Tchar>
    string_template<Tchar>& string_template<Tchar>::pad(Tchar ch, int count) {
        while(this->m_size + count*2 > this->m_capacity) this->set_capacity(this->m_capacity?this->m_capacity*2:16);
        this->pad_start(ch, count);
        this->pad_end(ch, count);
        return *this;
    }


    template<class Tchar>
    string_template<Tchar>& string_template<Tchar>::trim_start(const Tchar* chars) {
        int span = static_cast<int>(detail::strspn(this->m_array, chars));
        this->m_size -= span;
        memmove(this->m_array, this->m_array+span, this->m_size*sizeof(Tchar));
        return *this;
    }

    template<class Tchar>
    string_template<Tchar>& string_template<Tchar>::trim_end(const Tchar* chars) {
        int i = this->m_size - 2; // dont need to check '\0'
        if(i < 0) return *this;
        while(i >= 0) {
            if(!detail::strchr(chars, this->m_array[i])) break;
            --i;
        }
        this->m_size = i+2;
        this->m_array[this->m_size-1] = '\0';
        return *this;
    }

    template<class Tchar>
    string_template<Tchar>& string_template<Tchar>::trim(const Tchar* chars) {
        this->trim_start(chars);
        return this->trim_end(chars);
    }

    template<class Tchar>
    string_template<Tchar>& string_template<Tchar>::remove(Tchar ch) {
        Tchar* fch = this->m_array;
        while((fch = detail::strchr(fch, ch))) {
            memmove(fch, fch+1, (this->m_size - (fch - this->m_array))*sizeof(Tchar));
            --this->m_size;
        }
        return *this;
    }
    template<class Tchar>
    string_template<Tchar>& string_template<Tchar>::remove(Tchar* at, int count) {
        memmove(at, at + count, (this->m_size - (at + count - this->m_array))*sizeof(Tchar));
        this->m_size -= count;
        return *this;
    }
    template<class Tchar>
    string_template<Tchar>& string_template<Tchar>::remove(const Tchar* str) { return this->replace(str, this->nothing); }

    template<class Tchar>
    string_template<Tchar>& string_template<Tchar>::replace(Tchar before, Tchar after) {
        Tchar* fch = this->m_array-1;
        while((fch = detail::strchr(fch+1, before))) *fch = after;
        return *this;
    }
    
    template<class Tchar>
    string_template<Tchar>& string_template<Tchar>::replace(Tchar* at, int count, const Tchar* str) {
        int len = (int)detail::strlen(str);
        int new_size = this->m_size - count + len;
        if(new_size > this->m_capacity){
            auto offset = at - this->m_array;
            do this->set_capacity(this->m_capacity*2);
            while(new_size > this->m_capacity);
            at = this->m_array + offset;
        }
        memmove(at + len, at + count, (this->m_size - (at + count - this->m_array))*sizeof(Tchar));
        memcpy(at, str, len*sizeof(Tchar));
        this->m_size = new_size;
        return *this;
    }

    template<class Tchar>
    string_template<Tchar>& string_template<Tchar>::replace(const Tchar* before, const Tchar* after) {
        if(this->m_size <= 1) return *this; // ??
        int before_len = static_cast<int>(detail::strlen(before));
        if(before_len <= 0) return *this;
        int after_len = static_cast<int>(detail::strlen(after));
        int diff = after_len - before_len;
        int new_size = this->m_size;
        Tchar* fstr = this->m_array-1;
        while((fstr = detail::strstr(fstr+1, before))) new_size += diff;
        while(new_size>this->m_capacity) this->set_capacity(this->m_capacity*2);
        if(this->m_size < new_size) this->m_size = new_size;
        fstr = this->m_array;
        while((fstr = detail::strstr(fstr, before))) {
            memmove(fstr + after_len, fstr + before_len, (this->m_size - (fstr - this->m_array + before_len))*sizeof(Tchar));
            memcpy(fstr, after, after_len);
            fstr += after_len;
        }
        this->m_size = new_size;
        return *this;
    }
    
    template<class Tchar>
    string_template<Tchar>& string_template<Tchar>::replace_next(Tchar before, Tchar after) {
        Tchar* f = detail::strchr(this->m_array, before);
        if(f) *f = after;
        return *this;
    }
    
    template<class Tchar>
    string_template<Tchar>& string_template<Tchar>::replace_next(const Tchar* before, const Tchar* after) {
        if(this->m_size <= 1) return *this; // ??
        int before_len = static_cast<int>(detail::strlen(before));
        if(before_len <= 0) return *this;
        int after_len = static_cast<int>(detail::strlen(after));
        int diff = after_len - before_len;
        int new_size = this->m_size + diff;
        
        while(new_size>this->m_capacity) this->set_capacity(this->m_capacity*2);
        if(this->m_size < new_size) this->m_size = new_size;
        Tchar* fstr = detail::strstr(this->m_array, before);
        memmove(fstr + after_len, fstr + before_len, (this->m_size - (fstr - this->m_array + before_len))*sizeof(Tchar));
        memcpy(fstr, after, after_len);
        this->m_size = new_size;
        return *this;
    }

    template<class Tchar>
    string_template<Tchar>& string_template<Tchar>::append(const Tchar* str, int size) {
        assert(str != this->m_array); // undefined behavior (memcpy)
        if(!size) return *this;
        int new_size = size + this->m_size;
        while(new_size > this->m_capacity) this->set_capacity(this->m_capacity?this->m_capacity*2:16);
        memcpy(this->m_array + this->m_size - 1, str, (size + 1)*sizeof(Tchar));
        this->m_size = new_size;
        return *this;
    }
    template<class Tchar>
    string_template<Tchar>& string_template<Tchar>::append(const Tchar* str) {
        return this->append(str, static_cast<int>(detail::strlen(str)));
    }
    template<class Tchar>
    string_template<Tchar>& string_template<Tchar>::append(const string_template& str) {
        return this->append(str.c_str(), str.size());
    }

    template<class Tchar>
    string_template<Tchar>& string_template<Tchar>::prepend(const Tchar* str, int size) {
        if(!size) return *this;
        int new_size = size + this->m_size;
        while(new_size > this->m_capacity) this->set_capacity(this->m_capacity?this->m_capacity*2:16);
        memmove(this->m_array+size, this->m_array, this->m_size*sizeof(Tchar));
        memcpy(this->m_array, str, size*sizeof(Tchar));
        this->m_size = new_size;
        return *this;
    }
    template<class Tchar>
    string_template<Tchar>& string_template<Tchar>::prepend(const Tchar* str) {
        return this->prepend(str, static_cast<int>(detail::strlen(str)));
    }
    template<class Tchar>
    string_template<Tchar>& string_template<Tchar>::prepend(const string_template& str) {
        return this->prepend(str.c_str(), str.size());
    }

    template<class Tchar>
    const dynarray<string_template<Tchar>> string_template<Tchar>::split(const Tchar* chars) const {
        dynarray<string_template<Tchar>> buf;
        Tchar* first = nullptr;
        for(auto* it = this->begin(); it < this->end(); ++it) {
            if(it == nullptr) break;
            bool split_at = detail::strchr(chars, *it);
            if(!split_at && !first) {
                first = it;
            }
            if(split_at && first) // if it is a char we want to split at
            {
                buf.emplace(first, int(it - first));
                first = nullptr;
            }
        }
        if(first) buf.emplace(first, int(this->end() - first));
        return buf;
    }

    template<class Tchar>
    string_template<Tchar> string_template<Tchar>::toupper() const {
        return string_template(*this).toupper();
    }
    template<class Tchar>
    string_template<Tchar> string_template<Tchar>::tolower() const {
        return string_template(*this).tolower();
    }

    template<class Tchar>
    string_template<Tchar> string_template<Tchar>::pad_start(Tchar ch, int count) const {
        return string_template(*this).pad_start(ch, count);
    }
    template<class Tchar>
    string_template<Tchar> string_template<Tchar>::pad_end(Tchar ch, int count) const {
        return string_template(*this).pad_end(ch, count);
    }
    template<class Tchar>
    string_template<Tchar> string_template<Tchar>::pad(Tchar ch, int count) const {
        return string_template(*this).pad(ch, count);
    }

    template<class Tchar>
    string_template<Tchar> string_template<Tchar>::trim_start(const Tchar* chars) const {
        return string_template(*this).trim_start(chars);
    }
    template<class Tchar>
    string_template<Tchar> string_template<Tchar>::trim_end(const Tchar* chars) const {
        return string_template(*this).trim_end(chars);
    }
    template<class Tchar>
    string_template<Tchar> string_template<Tchar>::trim(const Tchar* chars) const {
        return string_template(*this).trim(chars);
    }

    template<class Tchar>
    string_template<Tchar> string_template<Tchar>::remove(Tchar ch) const {
        return string_template(*this).remove(ch);
    }
    template<class Tchar>
    string_template<Tchar> string_template<Tchar>::remove(Tchar* at, int count) const {
        return string_template(*this).remove(at, count);
    }
    template<class Tchar>
    string_template<Tchar> string_template<Tchar>::remove(const Tchar* str) const {
        return string_template(*this).remove(str);
    }

    template<class Tchar>
    string_template<Tchar> string_template<Tchar>::replace(Tchar before, Tchar after) const {
        return string_template(*this).replace(before, after);
    }
    template<class Tchar>
    string_template<Tchar> string_template<Tchar>::replace(Tchar* at, int count, const Tchar* str) const {
        return string_template(*this).replace(at, count, str);
    }
    template<class Tchar>
    string_template<Tchar> string_template<Tchar>::replace(const Tchar* before, const Tchar* after) const {
        return string_template(*this).replace(before, after);
    }
    
    template<class Tchar>
    string_template<Tchar> string_template<Tchar>::replace_next(Tchar before, Tchar after) const {
        return string_template(*this).replace_next(before, after);
    }
    template<class Tchar>
    string_template<Tchar> string_template<Tchar>::replace_next(const Tchar* before, const Tchar* after) const {
        return string_template(*this).replace_next(before, after);
    }

    template<class Tchar>
    string_template<Tchar> string_template<Tchar>::append(const Tchar* str, int size) const {
        return string_template(*this).append(str, size);
    }
    template<class Tchar>
    string_template<Tchar> string_template<Tchar>::append(const Tchar* str) const {
        return string_template(*this).append(str);
    }
    template<class Tchar>
    string_template<Tchar> string_template<Tchar>::append(const string_template& str) const {
        return string_template(*this).append(str);
    }

    template<class Tchar>
    string_template<Tchar> string_template<Tchar>::prepend(const Tchar* str, int size) const {
        return string_template(*this).prepend(str, size);
    }
    template<class Tchar>
    string_template<Tchar> string_template<Tchar>::prepend(const Tchar* str) const {
        return string_template(*this).prepend(str);
    }
    template<class Tchar>
    string_template<Tchar> string_template<Tchar>::prepend(const string_template& str) const {
        return string_template(*this).prepend(str);
    }

    template class string_template<char>;
    template class string_template<wchar_t>;
    
    sk::string to_string(int v) {
        char buf[30];
        snprintf(buf, 30, "%d", v);
        return sk::string(buf);
    }
    sk::string to_string(short v) {
        char buf[30];
        snprintf(buf, 30, "%hd", v);
        return sk::string(buf);
    }
    sk::string to_string(long v) {
        char buf[30];
        snprintf(buf, 30, "%ld", v);
        return sk::string(buf);
    }
    sk::string to_string(long long v) {
        char buf[30];
        snprintf(buf, 30, "%lld", v);
        return sk::string(buf);
    }
    sk::string to_string(unsigned int v) {
        char buf[30];
        snprintf(buf, 30, "%u", v);
        return sk::string(buf);
    }
    sk::string to_string(unsigned short v) {
        char buf[30];
        snprintf(buf, 30, "%hu", v);
        return sk::string(buf);
    }
    sk::string to_string(unsigned long v) {
        char buf[30];
        snprintf(buf, 30, "%lu", v);
        return sk::string(buf);
    }
    sk::string to_string(unsigned long long v) {
        char buf[30];
        snprintf(buf, 30, "%llu", v);
        return sk::string(buf);
    }
    sk::string to_string(float v) {
        char buf[30];
        snprintf(buf, 30, "%lf", v);
        return sk::string(buf);
    }
    sk::string to_string(double v) {
        char buf[30];
        snprintf(buf, 30, "%lf", v);
        return sk::string(buf);
    }
    sk::string to_string(long double v) {
        char buf[30];
        snprintf(buf, 30, "%Lf", v);
        return sk::string(buf);
    }
    sk::string to_string(char v) {
        char buf[5];
        snprintf(buf, 5, "%c", v);
        return sk::string(buf);
    }
    sk::string to_string(void* v) {
        char buf[30];
        snprintf(buf, 30, "%p", v);
        return sk::string(buf);
    }
    sk::string to_string(const char* value) {
        return value;
    }
    const sk::string& to_string(const sk::string& value) {
        return value;
    }
}
