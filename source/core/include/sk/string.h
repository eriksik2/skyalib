#pragma once

#include "dynarray.h"
#include "array.h"

namespace sk {
    template<class Tchar>
    class string_template : public dynarray<Tchar> {
        static const Tchar* nothing;
        static const Tchar* whitespace;
        using dynarray<Tchar>::dynarray;
    public:
        string_template();
        string_template(int capacity);
        string_template(const Tchar* from);
        string_template(const Tchar* from, int length);

        string_template& operator=(const Tchar* from);

        const Tchar* c_str() const;
        int length() const;
        int size() const;
        int bytes() const;
        Tchar* end() const;
        Tchar& back() const;

        bool empty() const;

        int compare(const Tchar* str) const;

        bool equals(const Tchar* str, int size) const;
        bool equals(const Tchar* str) const;
        bool equals(const string_template& str) const;

        string_template& toupper();
        string_template& tolower();

        string_template& pad_start(Tchar ch, int count = 1);
        string_template& pad_end(Tchar ch, int count = 1);
        string_template& pad(Tchar ch, int count = 1);

        string_template& trim_start(const Tchar* chars = whitespace);
        string_template& trim_end(const Tchar* chars = whitespace);
        string_template& trim(const Tchar* chars = whitespace);

        string_template& remove(Tchar ch);
        string_template& remove(Tchar* at, int count);
        string_template& remove(const Tchar* str);

        string_template& replace(Tchar before, Tchar after);
        string_template& replace(Tchar* at, int count, const Tchar* str);
        string_template& replace(const Tchar* before, const Tchar* after);
        
        string_template& replace_next(Tchar before, Tchar after);
        string_template& replace_next(const Tchar* before, const Tchar* after);

        string_template& append(const Tchar* str, int size);
        string_template& append(const Tchar* str);
        string_template& append(const string_template& str);

        string_template& prepend(const Tchar* str, int size);
        string_template& prepend(const Tchar* str);
        string_template& prepend(const string_template& str);

        const dynarray<string_template> split(const Tchar* chars = whitespace) const;

        string_template toupper() const;
        string_template tolower() const;

        string_template pad_start(Tchar ch, int count = 1) const;
        string_template pad_end(Tchar ch, int count = 1) const;
        string_template pad(Tchar ch, int count = 1) const;

        string_template trim_start(const Tchar* chars = whitespace) const;
        string_template trim_end(const Tchar* chars = whitespace) const;
        string_template trim(const Tchar* chars = whitespace) const;

        string_template remove(Tchar ch) const;
        string_template remove(Tchar* at, int count) const;
        string_template remove(const Tchar* str) const;

        string_template replace(Tchar before, Tchar after) const;
        string_template replace(Tchar* at, int count, const Tchar* str) const;
        string_template replace(const Tchar* before, const Tchar* after) const;
        
        string_template replace_next(Tchar before, Tchar after) const;
        string_template replace_next(const Tchar* before, const Tchar* after) const;

        string_template append(const Tchar* str, int size) const;
        string_template append(const Tchar* str) const;
        string_template append(const string_template& str) const;

        string_template prepend(const Tchar* str, int size) const;
        string_template prepend(const Tchar* str) const;
        string_template prepend(const string_template& str) const;
    };

    using string = string_template<char>;
    using wstring = string_template<wchar_t>;
    
    // to_string for basic types
    sk::string to_string(int v);
    sk::string to_string(short v);
    sk::string to_string(long v);
    sk::string to_string(long long v);
    sk::string to_string(unsigned int v);
    sk::string to_string(unsigned short v);
    sk::string to_string(unsigned long v);
    sk::string to_string(unsigned long long v);
    sk::string to_string(float v);
    sk::string to_string(double v);
    sk::string to_string(long double v);
    sk::string to_string(char v);
    sk::string to_string(void* v);
    sk::string to_string(const char* value);
    const sk::string& to_string(const sk::string& value);

    template<class Tchar>
    bool operator==(const string_template<Tchar>& lhs, const Tchar* rhs) {
        return lhs.equals(rhs);
    }
    template<class Tchar>
    bool operator==(const Tchar* lhs, const string_template<Tchar>& rhs) {
        return rhs.equals(lhs);
    }
    template<class Tchar>
    bool operator==(const string_template<Tchar>& lhs, const string_template<Tchar>& rhs) {
        return lhs.equals(rhs);
    }
    template<class Tchar>
    bool operator!=(const string_template<Tchar>& lhs, const Tchar* rhs) {
        return !lhs.equals(rhs);
    }
    template<class Tchar>
    bool operator!=(const Tchar* lhs, const string_template<Tchar>& rhs) {
        return !rhs.equals(lhs);
    }
    template<class Tchar>
    bool operator!=(const string_template<Tchar>& lhs, const string_template<Tchar>& rhs) {
        return !lhs.equals(rhs);
    }
    template<class Tchar>
    bool operator<(const string_template<Tchar>& lhs, const Tchar* rhs) {
        return lhs.compare(rhs) < 0;
    }
    template<class Tchar>
    bool operator<(const Tchar* lhs, const string_template<Tchar>& rhs) {
        return rhs.compare(lhs) > 0;
    }
    template<class Tchar>
    bool operator<=(const string_template<Tchar>& lhs, const Tchar* rhs) {
        return lhs.compare(rhs) <= 0;
    }
    template<class Tchar>
    bool operator<=(const Tchar* lhs, const string_template<Tchar>& rhs) {
        return rhs.compare(lhs) >= 0;
    }
    template<class Tchar>
    bool operator>(const string_template<Tchar>& lhs, const Tchar* rhs) {
        return lhs.compare(rhs) > 0;
    }
    template<class Tchar>
    bool operator>(const Tchar* lhs, const string_template<Tchar>& rhs) {
        return rhs.compare(lhs) < 0;
    }
    template<class Tchar>
    bool operator>=(const string_template<Tchar>& lhs, const Tchar* rhs) {
        return lhs.compare(rhs) >= 0;
    }
    template<class Tchar>
    bool operator>=(const Tchar* lhs, const string_template<Tchar>& rhs) {
        return rhs.compare(lhs) <= 0;
    }
    template<class Tchar>
    string_template<Tchar> operator+(const string_template<Tchar>& lhs, const Tchar* rhs) {
        return lhs.append(rhs);
    }
    template<class Tchar>
    string_template<Tchar>& operator+(string_template<Tchar>&& lhs, const Tchar* rhs) {
        return lhs.append(rhs);
    }
    template<class Tchar>
    string_template<Tchar> operator+(const Tchar* lhs, const string_template<Tchar>& rhs) {
        return rhs.prepend(lhs);
    }
    template<class Tchar>
    string_template<Tchar>& operator+(const Tchar* lhs, string_template<Tchar>&& rhs) {
        return rhs.prepend(lhs);
    }
    template<class Tchar>
    string_template<Tchar> operator+(const string_template<Tchar>& lhs, const string_template<Tchar>& rhs) {
        return lhs.append(rhs);
    }
    template<class Tchar>
    string_template<Tchar>& operator+(const string_template<Tchar>& lhs, string_template<Tchar>&& rhs) {
        return rhs.prepend(lhs);
    }
    template<class Tchar>
    string_template<Tchar>& operator+=(string_template<Tchar>& lhs, const Tchar* rhs) {
        return lhs.append(rhs);
    }
    template<class Tchar>
    string_template<Tchar>& operator+=(string_template<Tchar>& lhs, const string_template<Tchar>& rhs) {
        return lhs.append(rhs);
    }
}
