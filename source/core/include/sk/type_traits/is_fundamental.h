#pragma once

#include <sk/type_traits/integral_constant.h>
#include <sk/type_traits/is_arithmetic.h>
#include <sk/type_traits/is_void.h>
#include <sk/type_traits/is_pointer.h>

namespace sk {
    namespace impl {
        template<class T>
        struct is_fundamental : sk::bool_constant<
            sk::is_arithmetic<T> ||
            sk::is_void<T>       ||
            sk::is_null_pointer<T>
        > {};
    }
    
    template<class T>
    constexpr bool is_fundamental = impl::is_fundamental<T>::value;
}
