#pragma once

namespace sk {
    namespace impl {
        template<class T>
        struct remove_reference {
            using type = T;
        };
        
        template<class T>
        struct remove_reference<T&> {
            using type = T;
        };
        
        template<class T>
        struct remove_reference<T&&> {
            using type = T;
        };
    }
    
    template<class T> using remove_reference = typename impl::remove_reference<T>::type;
}
