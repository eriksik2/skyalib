#pragma once

#include <sk/type_traits/is_scalar.h>
#include <sk/type_traits/is_reference.h>
#include <sk/type_traits/array_traits.h>
#include <sk/type_traits/void_t.h>
#include <sk/type_traits/declval.h>
#include <sk/type_traits/integral_constant.h>
#include <sk/type_traits/has_feature.h>

namespace sk {
    // is_destructible
    namespace impl {
        template<class T, class = void>
        struct is_destructible_impl : sk::false_type {};
        
        template<class T>
        struct is_destructible_impl<T, sk::void_t<decltype(sk::declval<T>().~T())>> : sk::true_type {};
        
        template<class T>
        struct is_destructible : sk::bool_constant<!sk::is_function<T> &&
            is_destructible_impl<sk::remove_all_extents<T>>::value> {};
        
        template<class T> struct is_destructible<T&> : sk::true_type {};
        template<class T> struct is_destructible<T&&> : sk::true_type {};
        template<class T> struct is_destructible<T[]> : sk::false_type {};
        template<> struct is_destructible<void> : sk::false_type {};
    }

    template<class T>
    constexpr bool is_destructible = impl::is_destructible<T>::value;
    
    // is_trivially_destructible
    namespace impl {
#if __has_feature(has_trivial_destructor) || _GNUC_VER >= 403
        
        template <class T>
        struct is_trivially_destructible
        : sk::bool_constant<sk::is_destructible<T> && __has_trivial_destructor(T)> {};
        
#else
        
        template <class T>
        struct is_trivially_destructible_impl : sk::bool_constant<sk::is_scalar<T> || sk::is_reference<T>> {};
        
        template <class T>
        struct is_trivially_destructible : is_trivially_destructible_impl<sk::remove_all_extents<T>> {};
        
        template <class T>
        struct is_trivially_destructible<T[]> : sk::false_type {};
        
#endif
    }
    
    template<class T>
    constexpr bool is_trivially_destructible = impl::is_trivially_destructible<T>::value;
}
