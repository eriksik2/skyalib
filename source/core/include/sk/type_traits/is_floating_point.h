#pragma once

#include <sk/type_traits/integral_constant.h>
#include <sk/type_traits/remove_cv.h>
#include <sk/type_traits/is_same.h>

namespace sk {
    namespace impl {
        template<class T>
        struct is_floating_point : sk::bool_constant<
            sk::is_same<float,       sk::remove_cv<T>> ||
            sk::is_same<double,      sk::remove_cv<T>> ||
            sk::is_same<long double, sk::remove_cv<T>>
        > {};
    }
    
    template<class T>
    constexpr bool is_floating_point = impl::is_floating_point<T>::value;
    
    //template<class T> constexpr bool is_floating = is_floating_point<T>;
}
