#pragma once

#include <sk/type_traits/is_cv.h>
#include <sk/type_traits/array_traits.h>
#include <sk/type_traits/is_constructible.h>
#include <sk/type_traits/integral_constant.h>
#include <sk/type_traits/has_feature.h>

namespace sk {
    // is_trivially_copyable
    namespace impl {
        template<class T>
        struct is_trivially_copyable
#if _MSC_VER >= 1900
        : sk::bool_constant<__has_trivial_copy(T)> {};
#elif __has_feature(is_trivially_copyable)
        : sk::bool_constant<__is_trivially_copyable(T)> {};
#elif _GNUC_VER >= 501
        : sk::bool_constant<!is_volatile<T> && __is_trivially_copyable(T)> {};
#else
        : is_scalar<remove_all_extents<T>> {};
#endif
    }
    
    template<class T>
    constexpr bool is_trivially_copyable = impl::is_trivially_copyable<T>::value;
    
    // is_trivial
    namespace impl {
        template <class _Tp>
        struct is_trivial
#if !defined(_MSC_VER) && (__has_feature(is_trivial) || _GNUC_VER >= 407)
        : sk::bool_constant<__is_trivial(_Tp)> {};
#else
        : sk::bool_constant<sk::is_trivially_copyable<_Tp>
        && sk::is_trivially_default_constructible<_Tp>> {};
#endif
    }
    
    template<class T>
    constexpr bool is_trivial = impl::is_trivial<T>::value;
    
}
