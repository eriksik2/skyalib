#pragma once

#include <sk/type_traits/integral_constant.h>
#include <sk/type_traits/is_scalar.h>
#include <sk/type_traits/array_traits.h>
#include <sk/type_traits/is_composite.h>

namespace sk {
    namespace impl {
        template<class T>
        struct is_object : sk::bool_constant<
            sk::is_scalar<T>     ||
            sk::is_array<T>      ||
            sk::is_composite<T>
        > {};
    }
    
    template<class T>
    constexpr bool is_object = impl::is_object<T>::value;
}
