#pragma once

namespace sk {
    template<class...>
    using void_t = void;
    
    template<class T1, class...>
    using type_t = T1;
}
