#pragma once

#include <sk/utility/tuple.h>

namespace sk {
    namespace impl {
        
        struct function_traits_base {
            static constexpr bool is_function = true;
            static constexpr bool is_const = false;
            static constexpr bool is_volatile = false;
            static constexpr bool is_lvalue = false;
            static constexpr bool is_rvalue = false;
            static constexpr bool is_noexcept = false;
            static constexpr bool is_variadic = false;
            using return_type = void;
            using args_type = sk::tuple<>;
            //template<sk::size_t s> using arg_type = arg_types;
        };
        
        // primary template
        template<class>
        struct function_traits : function_traits_base {
            static constexpr bool is_function = false;
        };
        
        // specialization for regular functions
        template<class Ret, class... Args>
        struct function_traits<Ret(Args...)> : function_traits_base {
            using return_type = Ret;
            using args_type = sk::tuple<Args...>;
        };
        
        // specialization for function pointer
        template<class Ret, class... Args>
        struct function_traits<Ret(*)(Args...)> : function_traits_base {
            using return_type = Ret;
            using args_type = sk::tuple<Args...>;
        };
        
        // specialization for variadic functions such as std::printf
        template<class Ret, class... Args>
        struct function_traits<Ret(Args...,...)> : function_traits_base {
            static constexpr bool is_variadic = true;
            using return_type = Ret;
            using args_type = sk::tuple<Args...>;
        };
        
        // specialization for function types that have cv-qualifiers
        template<class Ret, class... Args>
        struct function_traits<Ret(Args...) const> : function_traits_base {
            static constexpr bool is_const = true;
            using return_type = Ret;
            using args_type = sk::tuple<Args...>;
        };
        template<class Ret, class... Args>
        struct function_traits<Ret(Args...) volatile> : function_traits_base {
            static constexpr bool is_volatile = true;
            using return_type = Ret;
            using args_type = sk::tuple<Args...>;
        };
        template<class Ret, class... Args>
        struct function_traits<Ret(Args...) const volatile> : function_traits_base {
            static constexpr bool is_const = true;
            static constexpr bool is_volatile = true;
            using return_type = Ret;
            using args_type = sk::tuple<Args...>;
        };
        template<class Ret, class... Args>
        struct function_traits<Ret(Args...,...) const> : function_traits_base {
            static constexpr bool is_const = true;
            static constexpr bool is_variadic = true;
            using return_type = Ret;
            using args_type = sk::tuple<Args...>;
        };
        template<class Ret, class... Args>
        struct function_traits<Ret(Args...,...) volatile> : function_traits_base {
            static constexpr bool is_volatile = true;
            static constexpr bool is_variadic = true;
            using return_type = Ret;
            using args_type = sk::tuple<Args...>;
        };
        template<class Ret, class... Args>
        struct function_traits<Ret(Args...,...) const volatile> : function_traits_base {
            static constexpr bool is_const = true;
            static constexpr bool is_volatile = true;
            static constexpr bool is_variadic = true;
            using return_type = Ret;
            using args_type = sk::tuple<Args...>;
        };
        
        // specialization for function types that have ref-qualifiers
        template<class Ret, class... Args>
        struct function_traits<Ret(Args...) &> : function_traits_base {
            static constexpr bool is_lvalue = true;
            using return_type = Ret;
            using args_type = sk::tuple<Args...>;
        };
        template<class Ret, class... Args>
        struct function_traits<Ret(Args...) const &> : function_traits_base {
            static constexpr bool is_const = true;
            static constexpr bool is_lvalue = true;
            using return_type = Ret;
            using args_type = sk::tuple<Args...>;
        };
        template<class Ret, class... Args>
        struct function_traits<Ret(Args...) volatile &> : function_traits_base {
            static constexpr bool is_volatile = true;
            static constexpr bool is_lvalue = true;
            using return_type = Ret;
            using args_type = sk::tuple<Args...>;
        };
        template<class Ret, class... Args>
        struct function_traits<Ret(Args...) const volatile &> : function_traits_base {
            static constexpr bool is_const = true;
            static constexpr bool is_volatile = true;
            static constexpr bool is_lvalue = true;
            using return_type = Ret;
            using args_type = sk::tuple<Args...>;
        };
        template<class Ret, class... Args>
        struct function_traits<Ret(Args...,...) &> : function_traits_base {
            static constexpr bool is_lvalue = true;
            static constexpr bool is_variadic = true;
            using return_type = Ret;
            using args_type = sk::tuple<Args...>;
        };
        template<class Ret, class... Args>
        struct function_traits<Ret(Args...,...) const &> : function_traits_base {
            static constexpr bool is_const = true;
            static constexpr bool is_lvalue = true;
            static constexpr bool is_variadic = true;
            using return_type = Ret;
            using args_type = sk::tuple<Args...>;
        };
        template<class Ret, class... Args>
        struct function_traits<Ret(Args...,...) volatile &> : function_traits_base {
            static constexpr bool is_volatile = true;
            static constexpr bool is_lvalue = true;
            static constexpr bool is_variadic = true;
            using return_type = Ret;
            using args_type = sk::tuple<Args...>;
        };
        template<class Ret, class... Args>
        struct function_traits<Ret(Args...,...) const volatile &> : function_traits_base {
            static constexpr bool is_const = true;
            static constexpr bool is_volatile = true;
            static constexpr bool is_lvalue = true;
            static constexpr bool is_variadic = true;
            using return_type = Ret;
            using args_type = sk::tuple<Args...>;
        };
        template<class Ret, class... Args>
        struct function_traits<Ret(Args...) &&> : function_traits_base {
            static constexpr bool is_rvalue = true;
            using return_type = Ret;
            using args_type = sk::tuple<Args...>;
        };
        template<class Ret, class... Args>
        struct function_traits<Ret(Args...) const &&> : function_traits_base {
            static constexpr bool is_const = true;
            static constexpr bool is_rvalue = true;
            using return_type = Ret;
            using args_type = sk::tuple<Args...>;
        };
        template<class Ret, class... Args>
        struct function_traits<Ret(Args...) volatile &&> : function_traits_base {
            static constexpr bool is_volatile = true;
            static constexpr bool is_rvalue = true;
            using return_type = Ret;
            using args_type = sk::tuple<Args...>;
        };
        template<class Ret, class... Args>
        struct function_traits<Ret(Args...) const volatile &&> : function_traits_base {
            static constexpr bool is_const = true;
            static constexpr bool is_volatile = true;
            static constexpr bool is_rvalue = true;
            using return_type = Ret;
            using args_type = sk::tuple<Args...>;
        };
        template<class Ret, class... Args>
        struct function_traits<Ret(Args...,...) &&> : function_traits_base {
            static constexpr bool is_rvalue = true;
            static constexpr bool is_variadic = true;
            using return_type = Ret;
            using args_type = sk::tuple<Args...>;
        };
        template<class Ret, class... Args>
        struct function_traits<Ret(Args...,...) const &&> : function_traits_base {
            static constexpr bool is_const = true;
            static constexpr bool is_rvalue = true;
            static constexpr bool is_variadic = true;
            using return_type = Ret;
            using args_type = sk::tuple<Args...>;
        };
        template<class Ret, class... Args>
        struct function_traits<Ret(Args...,...) volatile &&> : function_traits_base {
            static constexpr bool is_volatile = true;
            static constexpr bool is_rvalue = true;
            static constexpr bool is_variadic = true;
            using return_type = Ret;
            using args_type = sk::tuple<Args...>;
        };
        template<class Ret, class... Args>
        struct function_traits<Ret(Args...,...) const volatile &&> : function_traits_base {
            static constexpr bool is_const = true;
            static constexpr bool is_volatile = true;
            static constexpr bool is_rvalue = true;
            static constexpr bool is_variadic = true;
            using return_type = Ret;
            using args_type = sk::tuple<Args...>;
        };
        
        // specializations for noexcept versions of all the above (C++17 and later)
        
        template<class Ret, class... Args>
        struct function_traits<Ret(Args...) noexcept> : function_traits_base {
            static constexpr bool is_noexcept = true;
            using return_type = Ret;
            using args_type = sk::tuple<Args...>;
        };
        template<class Ret, class... Args>
        struct function_traits<Ret(Args...,...) noexcept> : function_traits_base {
            static constexpr bool is_noexcept = true;
            static constexpr bool is_variadic = true;
            using return_type = Ret;
            using args_type = sk::tuple<Args...>;
        };
        template<class Ret, class... Args>
        struct function_traits<Ret(Args...) const noexcept> : function_traits_base {
            static constexpr bool is_const = true;
            static constexpr bool is_noexcept = true;
            using return_type = Ret;
            using args_type = sk::tuple<Args...>;
        };
        template<class Ret, class... Args>
        struct function_traits<Ret(Args...) volatile noexcept> : function_traits_base {
            static constexpr bool is_volatile = true;
            static constexpr bool is_noexcept = true;
            using return_type = Ret;
            using args_type = sk::tuple<Args...>;
        };
        template<class Ret, class... Args>
        struct function_traits<Ret(Args...) const volatile noexcept> : function_traits_base {
            static constexpr bool is_const = true;
            static constexpr bool is_volatile = true;
            static constexpr bool is_noexcept = true;
            using return_type = Ret;
            using args_type = sk::tuple<Args...>;
        };
        template<class Ret, class... Args>
        struct function_traits<Ret(Args...,...) const noexcept> : function_traits_base {
            static constexpr bool is_const = true;
            static constexpr bool is_noexcept = true;
            static constexpr bool is_variadic = true;
            using return_type = Ret;
            using args_type = sk::tuple<Args...>;
        };
        template<class Ret, class... Args>
        struct function_traits<Ret(Args...,...) volatile noexcept> : function_traits_base {
            static constexpr bool is_volatile = true;
            static constexpr bool is_noexcept = true;
            static constexpr bool is_variadic = true;
            using return_type = Ret;
            using args_type = sk::tuple<Args...>;
        };
        template<class Ret, class... Args>
        struct function_traits<Ret(Args...,...) const volatile noexcept> : function_traits_base {
            static constexpr bool is_const = true;
            static constexpr bool is_volatile = true;
            static constexpr bool is_noexcept = true;
            static constexpr bool is_variadic = true;
            using return_type = Ret;
            using args_type = sk::tuple<Args...>;
        };
        template<class Ret, class... Args>
        struct function_traits<Ret(Args...) & noexcept> : function_traits_base {
            static constexpr bool is_lvalue = true;
            static constexpr bool is_noexcept = true;
            using return_type = Ret;
            using args_type = sk::tuple<Args...>;
        };
        template<class Ret, class... Args>
        struct function_traits<Ret(Args...) const & noexcept> : function_traits_base {
            static constexpr bool is_const = true;
            static constexpr bool is_lvalue = true;
            static constexpr bool is_noexcept = true;
            using return_type = Ret;
            using args_type = sk::tuple<Args...>;
        };
        template<class Ret, class... Args>
        struct function_traits<Ret(Args...) volatile & noexcept> : function_traits_base {
            static constexpr bool is_volatile = true;
            static constexpr bool is_lvalue = true;
            static constexpr bool is_noexcept = true;
            using return_type = Ret;
            using args_type = sk::tuple<Args...>;
        };
        template<class Ret, class... Args>
        struct function_traits<Ret(Args...) const volatile & noexcept> : function_traits_base {
            static constexpr bool is_const = true;
            static constexpr bool is_volatile = true;
            static constexpr bool is_lvalue = true;
            static constexpr bool is_noexcept = true;
            using return_type = Ret;
            using args_type = sk::tuple<Args...>;
        };
        template<class Ret, class... Args>
        struct function_traits<Ret(Args...,...) & noexcept> : function_traits_base {
            static constexpr bool is_lvalue = true;
            static constexpr bool is_noexcept = true;
            static constexpr bool is_variadic = true;
            using return_type = Ret;
            using args_type = sk::tuple<Args...>;
        };
        template<class Ret, class... Args>
        struct function_traits<Ret(Args...,...) const & noexcept> : function_traits_base {
            static constexpr bool is_const = true;
            static constexpr bool is_lvalue = true;
            static constexpr bool is_noexcept = true;
            static constexpr bool is_variadic = true;
            using return_type = Ret;
            using args_type = sk::tuple<Args...>;
        };
        template<class Ret, class... Args>
        struct function_traits<Ret(Args...,...) volatile & noexcept> : function_traits_base {
            static constexpr bool is_volatile = true;
            static constexpr bool is_lvalue = true;
            static constexpr bool is_noexcept = true;
            static constexpr bool is_variadic = true;
            using return_type = Ret;
            using args_type = sk::tuple<Args...>;
        };
        template<class Ret, class... Args>
        struct function_traits<Ret(Args...,...) const volatile & noexcept> : function_traits_base {
            static constexpr bool is_const = true;
            static constexpr bool is_volatile = true;
            static constexpr bool is_lvalue = true;
            static constexpr bool is_noexcept = true;
            static constexpr bool is_variadic = true;
            using return_type = Ret;
            using args_type = sk::tuple<Args...>;
        };
        template<class Ret, class... Args>
        struct function_traits<Ret(Args...) && noexcept> : function_traits_base {
            static constexpr bool is_rvalue = true;
            using return_type = Ret;
            using args_type = sk::tuple<Args...>;
        };
        template<class Ret, class... Args>
        struct function_traits<Ret(Args...) const && noexcept> : function_traits_base {
            static constexpr bool is_const = true;
            static constexpr bool is_rvalue = true;
            static constexpr bool is_noexcept = true;
            using return_type = Ret;
            using args_type = sk::tuple<Args...>;
        };
        template<class Ret, class... Args>
        struct function_traits<Ret(Args...) volatile && noexcept> : function_traits_base {
            static constexpr bool is_volatile = true;
            static constexpr bool is_rvalue = true;
            static constexpr bool is_noexcept = true;
            using return_type = Ret;
            using args_type = sk::tuple<Args...>;
        };
        template<class Ret, class... Args>
        struct function_traits<Ret(Args...) const volatile && noexcept> : function_traits_base {
            static constexpr bool is_const = true;
            static constexpr bool is_volatile = true;
            static constexpr bool is_rvalue = true;
            static constexpr bool is_noexcept = true;
            using return_type = Ret;
            using args_type = sk::tuple<Args...>;
        };
        template<class Ret, class... Args>
        struct function_traits<Ret(Args...,...) && noexcept> : function_traits_base {
            static constexpr bool is_rvalue = true;
            static constexpr bool is_noexcept = true;
            static constexpr bool is_variadic = true;
            using return_type = Ret;
            using args_type = sk::tuple<Args...>;
        };
        template<class Ret, class... Args>
        struct function_traits<Ret(Args...,...) const && noexcept> : function_traits_base {
            static constexpr bool is_const = true;
            static constexpr bool is_rvalue = true;
            static constexpr bool is_noexcept = true;
            static constexpr bool is_variadic = true;
            using return_type = Ret;
            using args_type = sk::tuple<Args...>;
        };
        template<class Ret, class... Args>
        struct function_traits<Ret(Args...,...) volatile && noexcept> : function_traits_base {
            static constexpr bool is_volatile = true;
            static constexpr bool is_rvalue = true;
            static constexpr bool is_noexcept = true;
            static constexpr bool is_variadic = true;
            using return_type = Ret;
            using args_type = sk::tuple<Args...>;
        };
        template<class Ret, class... Args>
        struct function_traits<Ret(Args...,...) const volatile && noexcept> : function_traits_base {
            static constexpr bool is_const = true;
            static constexpr bool is_volatile = true;
            static constexpr bool is_rvalue = true;
            static constexpr bool is_noexcept = true;
            static constexpr bool is_variadic = true;
            using return_type = Ret;
            using args_type = sk::tuple<Args...>;
        };
    }
    template<class T>
    using function_traits = impl::function_traits<T>;
    
    template<class T>
    constexpr bool is_function = function_traits<T>::is_function;
    template<class T>
    constexpr bool function_is_const = function_traits<T>::is_const;
    template<class T>
    constexpr bool function_is_volatile = function_traits<T>::is_volatile;
    template<class T>
    constexpr bool function_is_lvalue = function_traits<T>::is_lvalue;
    template<class T>
    constexpr bool function_is_rvalue = function_traits<T>::is_rvalue;
    template<class T>
    constexpr bool function_is_noexcept = function_traits<T>::is_noexcept;
    template<class T>
    constexpr bool function_is_variadic = function_traits<T>::is_variadic;
    
    template<class T>
    using function_return_type = typename function_traits<T>::return_type;
    template<class T>
    using function_args_type = typename function_traits<T>::args_type;
    
}
