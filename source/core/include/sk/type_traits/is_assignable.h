#pragma once

#include <sk/type_traits/is_scalar.h>
#include <sk/type_traits/void_t.h>
#include <sk/type_traits/declval.h>
#include <sk/type_traits/integral_constant.h>
#include <sk/type_traits/has_feature.h>

namespace sk {
    // is_assignable
    namespace impl {
        template<class T, class Targ, class = void>
        struct is_assignable : sk::false_type {};
        
        template<class T, class Targ>
        struct is_assignable<T, Targ, sk::void_t<decltype(sk::declval<T>() = sk::declval<Targ>())>> : sk::true_type {};
        
        template<class T> struct is_assignable<T, void> : sk::false_type {};
        template<class T> struct is_assignable<void, T> : sk::false_type {};
        template<> struct is_assignable<void, void>     : sk::false_type {};
    }

    template<class T, class Targ>
    constexpr bool is_assignable = impl::is_assignable<T, Targ>::value;
    
    template<class T>
    constexpr bool is_copy_assignable = is_assignable<T&, const T&>;
    
    template<class T>
    constexpr bool is_move_assignable = is_assignable<T&, T&&>;
    
    static_assert(is_assignable<int&, int> == true, "");
    static_assert(is_assignable<int&, void> == false, "");
    
    // is_trivially_assignable
    namespace impl {
#if __has_feature(is_trivially_assignable) || _GNUC_VER >= 501
        template <class T, class Targ>
        struct is_trivially_assignable : sk::bool_constant<__is_trivially_assignable(T, Targ)> {};
#else
        template <class, class>
        struct is_trivially_assignable : sk::false_type {};
        
        template<class T>
        struct is_trivially_assignable<T&, T> : sk::bool_constant<sk::is_scalar<T>> {};
        
        template<class T>
        struct is_trivially_assignable<T&, T&> : sk::bool_constant<sk::is_scalar<T>> {};
        
        template<class T>
        struct is_trivially_assignable<T&, const T&> : sk::bool_constant<sk::is_scalar<T>> {};
        
        template<class T>
        struct is_trivially_assignable<T&, T&&> : sk::bool_constant<sk::is_scalar<T>> {};
#endif
    }
    
    template<class T, class Targ>
    constexpr bool is_trivially_assignable = impl::is_trivially_assignable<T, Targ>::value;
    
    template<class T>
    constexpr bool is_trivially_copy_assignable = is_trivially_assignable<T&, const T&>;
    
    template<class T>
    constexpr bool is_trivially_move_assignable = is_trivially_assignable<T&, T&&>;
}
