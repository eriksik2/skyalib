#pragma once

#include <sk/type_traits/is_object.h>
#include <sk/type_traits/is_reference.h>
#include <sk/type_traits/void_t.h>
#include <sk/type_traits/declval.h>
#include <sk/type_traits/integral_constant.h>
#include <sk/type_traits/has_feature.h>

namespace sk {
    // is_constructible
    namespace impl {
        template<class T, class, class...Targs>
        struct is_constructible : sk::false_type {};
        
        template<class T, class...Targs>
        struct is_constructible<T, sk::void_t<decltype(T(sk::declval<Targs>()...))>, Targs...> : sk::bool_constant<sk::is_object<T> || sk::is_reference<T>> {};
    }

    template<class T, class...Targs>
    constexpr bool is_constructible = impl::is_constructible<T, void, Targs...>::value;
    
    template<class T>
    constexpr bool is_default_constructible = is_constructible<T>;
    
    template<class T>
    constexpr bool is_copy_constructible = is_constructible<T, const T&>;
    
    template<class T>
    constexpr bool is_move_constructible = is_constructible<T, T&&>;
    
    static_assert(is_constructible<int> == true, "");
    static_assert(is_constructible<int, int> == true, "");
    static_assert(is_constructible<int, int, int> == false, "");
    static_assert(is_constructible<int, void> == false, "");
    
    // is_trivially_constructible
    namespace impl {
#if __has_feature(is_trivially_constructible) || _GNUC_VER >= 501
        template <class T, class...Targs>
        struct is_trivially_constructible : sk::bool_constant<__is_trivially_constructible(T, Targs...)> {};
#else
        template <class, class...>
        struct is_trivially_constructible : sk::false_type {};
        
        template <class T>
        struct is_trivially_constructible<T>
#if (_MSC_VER >= 1900) || __has_feature(has_trivial_constructor) || _GNUC_VER >= 403
        : sk::bool_constant<__has_trivial_constructor(T)> {};
#else
        : sk::bool_constant<sk::is_scalar<T>> {};
#endif
        template <class T>
        struct is_trivially_constructible<T, T&&> : sk::bool_constant<sk::is_scalar<T>> {};
        
        template <class T>
        struct is_trivially_constructible<T, const T&> : sk::bool_constant<sk::is_scalar<T>> {};
        
        template <class T>
        struct is_trivially_constructible<T, T&> : sk::bool_constant<sk::is_scalar<T>> {};
#endif
    }
    
    template<class T, class...Targs>
    constexpr bool is_trivially_constructible = impl::is_trivially_constructible<T, Targs...>::value;
    
    template<class T>
    constexpr bool is_trivially_default_constructible = is_trivially_constructible<T>;
    
    template<class T>
    constexpr bool is_trivially_copy_constructible = is_trivially_constructible<T, const T&>;
    
    template<class T>
    constexpr bool is_trivially_move_constructible = is_trivially_constructible<T, T&&>;
}
