#pragma once

namespace sk {
    namespace impl {
        template<class T> struct remove_const          { using type = T; };
        template<class T> struct remove_const<const T> { using type = T; };
        
        template<class T> struct remove_volatile             { using type = T; };
        template<class T> struct remove_volatile<volatile T> { using type = T; };
        
        template<class T>
        struct remove_cv {
            using type = typename remove_volatile<typename remove_const<T>::type>::type;
        };
    }

    template<class T> using remove_const = typename impl::remove_const<T>::type;
    template<class T> using remove_volatile = typename impl::remove_volatile<T>::type;
    template<class T> using remove_cv = typename impl::remove_cv<T>::type;
}
