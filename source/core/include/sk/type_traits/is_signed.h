#pragma once

#include <sk/type_traits/integral_constant.h>
#include <sk/type_traits/conditional.h>

namespace sk {
    namespace impl {
        template<class T> struct is_signed : sk::conditional<(T(-1) < T(0)), sk::true_type, sk::false_type> {};
        template<class T> struct is_unsigned : sk::conditional<(T(-1) < T(0)), sk::false_type, sk::true_type> {};
    }
    
    template<class T>
    constexpr bool is_signed = impl::is_signed<T>::value;
    template<class T>
    constexpr bool is_unsigned = impl::is_unsigned<T>::value;
    
    static_assert(is_signed<int> == true, "");
    static_assert(is_signed<unsigned int> == false, "");
}
