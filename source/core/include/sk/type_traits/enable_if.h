#pragma once

namespace sk {
    namespace impl {
        template<bool b, class T = int>
        struct enable_if {};
        
        template<class T>
        struct enable_if<true, T> { using type = T; };
    }
    
    template<bool b, class T = int>
    using enable_if = typename impl::enable_if<b, T>::type;
}
