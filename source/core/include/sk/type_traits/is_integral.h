#pragma once

#include <sk/type_traits/integral_constant.h>
#include <sk/type_traits/remove_cv.h>
#include <sk/type_traits/is_same.h>

namespace sk {
    namespace impl {
        template<class T>
        struct is_integral_impl : sk::bool_constant<
            sk::is_same<bool,      T> ||
            sk::is_same<char,      T> ||
            sk::is_same<char16_t,  T> ||
            sk::is_same<char32_t,  T> ||
            sk::is_same<wchar_t,   T> ||
            sk::is_same<short,     T> ||
            sk::is_same<int,       T> ||
            sk::is_same<long,      T> ||
            sk::is_same<long long, T> ||
            sk::is_same<signed char,        T> ||
            sk::is_same<unsigned char,      T> ||
            sk::is_same<unsigned short,     T> ||
            sk::is_same<unsigned int,       T> ||
            sk::is_same<unsigned long,      T> ||
            sk::is_same<unsigned long long, T>
        > {};
        
        template<class T>
        using is_integral = is_integral_impl<sk::remove_cv<T>>;
    }
    
    template<class T>
    constexpr bool is_integral = impl::is_integral<T>::value;
}
