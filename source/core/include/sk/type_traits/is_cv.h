#pragma once

#include <sk/type_traits/integral_constant.h>

namespace sk {
    namespace impl {
        template<class T> struct is_const          : sk::false_type {};
        template<class T> struct is_const<const T> : sk::true_type  {};
        
        template<class T> struct is_volatile             : sk::false_type {};
        template<class T> struct is_volatile<volatile T> : sk::true_type  {};
    }

    template<class T> constexpr bool is_const = impl::is_const<T>::value;
    template<class T> constexpr bool is_volatile = impl::is_volatile<T>::value;
}
