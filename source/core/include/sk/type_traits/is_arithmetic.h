#pragma once

#include <sk/type_traits/integral_constant.h>
#include <sk/type_traits/is_integral.h>
#include <sk/type_traits/is_floating_point.h>

namespace sk {
    namespace impl {
        template<class T>
        struct is_arithmetic : sk::bool_constant<
            sk::is_integral<T> || sk::is_floating_point<T>
        > {};
    }
    
    template<class T>
    constexpr bool is_arithmetic = impl::is_arithmetic<T>::value;
}
