#pragma once

#include <sk/type_traits/integral_constant.h>

namespace sk {
    namespace impl {
        template<class T> struct is_reference     : sk::false_type {};
        template<class T> struct is_reference<T&> : sk::true_type  {};
        template<class T> struct is_reference<T&&> : sk::true_type  {};
        
        template<class T> struct is_lvalue_reference     : sk::false_type {};
        template<class T> struct is_lvalue_reference<T&> : sk::true_type  {};
        
        template<class T> struct is_rvalue_reference      : sk::false_type {};
        template<class T> struct is_rvalue_reference<T&&> : sk::true_type  {};
    }
    
    template<class T>
    constexpr bool is_reference = impl::is_reference<T>::value;
    
    static_assert(is_reference<int> == false, "");
    static_assert(is_reference<void> == false, "");
    static_assert(is_reference<bool&> == true, "");
    static_assert(is_reference<int&&> == true, "");
    static_assert(is_reference<const int&> == true, "");
    static_assert(is_reference<const bool&&> == true, "");
    
    template<class T>
    constexpr bool is_lvalue_reference = impl::is_lvalue_reference<T>::value;
    
    static_assert(is_lvalue_reference<int> == false, "");
    static_assert(is_lvalue_reference<void> == false, "");
    static_assert(is_lvalue_reference<bool&> == true, "");
    static_assert(is_lvalue_reference<int&&> == false, "");
    static_assert(is_lvalue_reference<const int&> == true, "");
    static_assert(is_lvalue_reference<const bool&&> == false, "");
    
    template<class T>
    constexpr bool is_rvalue_reference = impl::is_rvalue_reference<T>::value;
    
    static_assert(is_rvalue_reference<int> == false, "");
    static_assert(is_rvalue_reference<void> == false, "");
    static_assert(is_rvalue_reference<bool&> == false, "");
    static_assert(is_rvalue_reference<int&&> == true, "");
    static_assert(is_rvalue_reference<const int&> == false, "");
    static_assert(is_rvalue_reference<const bool&&> == true, "");
}
