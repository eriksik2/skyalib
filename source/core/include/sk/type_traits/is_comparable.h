#pragma once

#include <sk/type_traits/void_t.h>
#include <sk/type_traits/declval.h>
#include <sk/type_traits/integral_constant.h>

namespace sk {
    // is_equality_comparable
    namespace impl {
        template<class T1, class T2, class = void> struct is_equality_comparable : sk::false_type {};
        
        template<class T1, class T2>
        struct is_equality_comparable<T1, T2, sk::void_t<decltype(sk::declval<T1>() == sk::declval<T2>())>> : sk::true_type {};
    }
    template<class T1, class T2>
    constexpr bool is_equality_comparable = impl::is_equality_comparable<T1, T2>::value;
    
    // is_inequality_comparable
    namespace impl {
        template<class T1, class T2, class = void> struct is_inequality_comparable : sk::false_type {};
        
        template<class T1, class T2>
        struct is_inequality_comparable<T1, T2, sk::void_t<decltype(sk::declval<T1>() != sk::declval<T2>())>> : sk::true_type {};
    }
    template<class T1, class T2>
    constexpr bool is_inequality_comparable = impl::is_inequality_comparable<T1, T2>::value;
    
    // is_greater_than_comparable
    namespace impl {
        template<class T1, class T2, class = void> struct is_greater_than_comparable : sk::false_type {};
        
        template<class T1, class T2>
        struct is_greater_than_comparable<T1, T2, sk::void_t<decltype(sk::declval<T1>() > sk::declval<T2>())>> : sk::true_type {};
    }
    template<class T1, class T2>
    constexpr bool is_greater_than_comparable = impl::is_greater_than_comparable<T1, T2>::value;
    
    // is_less_than_comparable
    namespace impl {
        template<class T1, class T2, class = void> struct is_less_than_comparable : sk::false_type {};
        
        template<class T1, class T2>
        struct is_less_than_comparable<T1, T2, sk::void_t<decltype(sk::declval<T1>() < sk::declval<T2>())>> : sk::true_type {};
    }
    template<class T1, class T2>
    constexpr bool is_less_than_comparable = impl::is_less_than_comparable<T1, T2>::value;
    
    // is_greater_equal_comparable
    namespace impl {
        template<class T1, class T2, class = void> struct is_greater_equal_comparable : sk::false_type {};
        
        template<class T1, class T2>
        struct is_greater_equal_comparable<T1, T2, sk::void_t<decltype(sk::declval<T1>() >= sk::declval<T2>())>> : sk::true_type {};
    }
    template<class T1, class T2>
    constexpr bool is_greater_equal_comparable = impl::is_greater_equal_comparable<T1, T2>::value;
    
    // is_less_equal_comparable
    namespace impl {
        template<class T1, class T2, class = void> struct is_less_equal_comparable : sk::false_type {};
        
        template<class T1, class T2>
        struct is_less_equal_comparable<T1, T2, sk::void_t<decltype(sk::declval<T1>() <= sk::declval<T2>())>> : sk::true_type {};
    }
    template<class T1, class T2>
    constexpr bool is_less_equal_comparable = impl::is_less_equal_comparable<T1, T2>::value;
}
