#pragma once

#include <sk/type_traits/integral_constant.h>
#include <sk/type_traits/remove_cv.h>

namespace sk {
    namespace impl {
        template<class T> struct is_void_helper       : sk::false_type {};
        template<>        struct is_void_helper<void> : sk::true_type  {};
        template<class T> struct is_void : is_void_helper<sk::remove_cv<T>> {};
    }
    
    template<class T>
    constexpr bool is_void = impl::is_void<T>::value;
    
    static_assert(is_void<int> == false, "");
    static_assert(is_void<const bool*> == false, "");
    static_assert(is_void<void> == true, "");
    static_assert(is_void<const void> == true, "");
    static_assert(is_void<void*> == false, "");
}
