#pragma once

#include <sk/type_traits/integral_constant.h>
#include <sk/type_traits/is_void.h>
#include <sk/type_traits/is_integral.h>
#include <sk/type_traits/is_floating_point.h>
#include <sk/type_traits/array_traits.h>
#include <sk/type_traits/is_pointer.h>
#include <sk/type_traits/is_reference.h>
#include <sk/type_traits/is_composite.h>
#include <sk/type_traits/function_traits.h>

namespace sk {
    namespace impl {
        template<class T>
        struct is_enum : sk::bool_constant<
            !sk::is_void<T>           &&
            !sk::is_integral<T>       &&
            !sk::is_floating_point<T> &&
            !sk::is_array<T>          &&
            !sk::is_pointer<T>        &&
            !sk::is_reference<T>      &&
            !sk::is_member_pointer<T> &&
            !sk::is_composite<T>      &&
            !sk::is_function<T>
        > {};
    }
    
    template<class T>
    constexpr bool is_enum = impl::is_enum<T>::value;
}
