#pragma once

namespace sk {
    template<class T>
    T&& declval();
}
