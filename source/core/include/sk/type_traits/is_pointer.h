#pragma once

#include <sk/type_traits/integral_constant.h>
#include <sk/type_traits/remove_cv.h>
#include <sk/type_traits/function_traits.h>
#include <sk/type_traits/is_same.h>
#include <sk/utility/types.h>

namespace sk {
    namespace impl {
        template<class T> struct is_pointer_helper     : sk::false_type {};
        template<class T> struct is_pointer_helper<T*> : sk::true_type  {};
        template<class T> struct is_pointer : is_pointer_helper<sk::remove_cv<T>> {};
        
        template<class T> struct is_member_pointer_helper                      : sk::false_type {};
        template<class T1, class T2> struct is_member_pointer_helper<T1 T2::*> : sk::true_type {};
        template<class T> struct is_member_pointer : is_member_pointer_helper<sk::remove_cv<T>> {};
        
        template<class T> struct is_member_function_pointer_helper : sk::false_type {};
        template<class T1, class T2>
        struct is_member_function_pointer_helper<T1 T2::*> : sk::bool_constant<sk::is_function<T1>> {};
        template<class T> struct is_member_function_pointer
            : is_member_function_pointer_helper<sk::remove_cv<T>> {};
        
        template<class T>
        struct is_member_object_pointer : sk::bool_constant<
            is_member_pointer<T>::value &&
            !is_member_function_pointer<T>::value
        > {};
        
        template<class T> struct is_null_pointer : sk::impl::is_same<sk::nullptr_t, sk::remove_cv<T>> {};
    }
    
    template<class T>
    constexpr bool is_pointer = impl::is_pointer<T>::value;
    
    template<class T>
    constexpr bool is_member_pointer = impl::is_member_pointer<T>::value;
    
    template<class T>
    constexpr bool is_member_function_pointer = impl::is_member_function_pointer<T>::value;
    
    template<class T>
    constexpr bool is_member_object_pointer = impl::is_member_object_pointer<T>::value;
    
    template<class T>
    constexpr bool is_null_pointer = impl::is_null_pointer<T>::value;
    
    static_assert(is_pointer<int> == false, "");
    static_assert(is_pointer<void> == false, "");
    static_assert(is_pointer<void*> == true, "");
    static_assert(is_pointer<const bool*> == true, "");
}
