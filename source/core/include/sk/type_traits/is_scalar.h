#pragma once

#include <sk/type_traits/integral_constant.h>
#include <sk/type_traits/is_arithmetic.h>
#include <sk/type_traits/is_enum.h>
#include <sk/type_traits/is_pointer.h>

namespace sk {
    namespace impl {
        template<class T>
        struct is_scalar : sk::bool_constant<
            sk::is_arithmetic<T>     ||
            sk::is_enum<T>           ||
            sk::is_pointer<T>        ||
            sk::is_member_pointer<T> ||
            sk::is_null_pointer<T>
        > {};
    }
    
    template<class T>
    constexpr bool is_scalar = impl::is_scalar<T>::value;
}
