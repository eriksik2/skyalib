#pragma once

#include <sk/type_traits/declval.h>
#include <sk/type_traits/array_traits.h>
#include <sk/type_traits/function_traits.h>
#include <sk/type_traits/is_void.h>
#include <sk/type_traits/integral_constant.h>

namespace sk {
    // is_convertible
    namespace is_convertible_impl {
        template<class T> void convert_func(T);
        
        template<class Tfrom, class Tto, class = void>
        struct is_convertible_impl : sk::false_type {};
        
        template<class Tfrom, class Tto>
        struct is_convertible_impl<Tfrom, Tto, decltype(convert_func<Tto>(sk::declval<Tfrom>()))> : sk::true_type {};
        
        template<class T, bool = sk::is_array<T>, bool = sk::is_function<T>, bool = sk::is_void<T>>
        constexpr int arr_func_void = 0;
        template<class T> constexpr int arr_func_void<T, true, false, false> = 1;
        template<class T> constexpr int arr_func_void<T, false, true, false> = 2;
        template<class T> constexpr int arr_func_void<T, false, false, true> = 3;
        
        template<class Tfrom, class Tto, int = arr_func_void<Tfrom>, int = arr_func_void<Tto>>
        struct is_convertible : is_convertible_impl<Tfrom, Tto> {};
        
        template<class Tfrom, class Tto> struct is_convertible<Tfrom, Tto, 0, 1> : sk::false_type {};
        template<class Tfrom, class Tto> struct is_convertible<Tfrom, Tto, 1, 1> : sk::false_type {};
        template<class Tfrom, class Tto> struct is_convertible<Tfrom, Tto, 2, 1> : sk::false_type {};
        template<class Tfrom, class Tto> struct is_convertible<Tfrom, Tto, 3, 1> : sk::false_type {};
        
        template<class Tfrom, class Tto> struct is_convertible<Tfrom, Tto, 0, 2> : sk::false_type {};
        template<class Tfrom, class Tto> struct is_convertible<Tfrom, Tto, 1, 2> : sk::false_type {};
        template<class Tfrom, class Tto> struct is_convertible<Tfrom, Tto, 2, 2> : sk::false_type {};
        template<class Tfrom, class Tto> struct is_convertible<Tfrom, Tto, 3, 2> : sk::false_type {};
        
        template<class Tfrom, class Tto> struct is_convertible<Tfrom, Tto, 0, 3> : sk::false_type {};
        template<class Tfrom, class Tto> struct is_convertible<Tfrom, Tto, 1, 3> : sk::false_type {};
        template<class Tfrom, class Tto> struct is_convertible<Tfrom, Tto, 2, 3> : sk::false_type {};
        template<class Tfrom, class Tto> struct is_convertible<Tfrom, Tto, 3, 3> : sk::true_type {};
    }
    
    template<class Tfrom, class Tto>
    constexpr bool is_convertible = is_convertible_impl::is_convertible<Tfrom, Tto>::value;
}
