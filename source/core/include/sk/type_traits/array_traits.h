#pragma once

#include <sk/type_traits/integral_constant.h>
#include <sk/utility/types.h>

namespace sk {
    namespace impl {
        template<class T>
        struct is_array : sk::false_type {};
        
        template<class T>
        struct is_array<T[]> : sk::true_type {};
        
        template<class T, sk::size_t s>
        struct is_array<T[s]> : sk::true_type {};
    }
    
    template<class T>
    constexpr bool is_array = impl::is_array<T>::value;
    
    namespace impl {
        template<class T>
        struct remove_extent {
            using type = T;
        };
        
        template<class T>
        struct remove_extent<T[]> {
            using type = T;
        };
        
        template<class T, sk::size_t N>
        struct remove_extent<T[N]> {
            using type = T;
        };
        
        template<class T>
        struct remove_all_extents {
            using type = T;
        };
        
        template<class T>
        struct remove_all_extents<T[]> {
            using type = typename remove_all_extents<T>::type;
        };
        
        template<class T, sk::size_t N>
        struct remove_all_extents<T[N]> {
            using type = typename remove_all_extents<T>::type;
        };
    }
    
    template<class T>
    using remove_extent = typename impl::remove_extent<T>::type;
    
    template<class T>
    using remove_all_extents = typename impl::remove_all_extents<T>::type;
}
