#pragma once

#include <sk/type_traits/integral_constant.h>

namespace sk {
    namespace impl {
        
        template<class...T>
        struct is_same : sk::false_type {};
        
        template<class T, class...Trest>
        struct is_same<T, T, Trest...> : is_same<T, Trest...> {};
        
        template<class T>
        struct is_same<T> : sk::true_type {};
    }
    
    template<class...T>
    constexpr bool is_same = impl::is_same<T...>::value;
}
