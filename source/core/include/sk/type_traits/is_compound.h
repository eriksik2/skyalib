#pragma once

#include <sk/type_traits/integral_constant.h>
#include <sk/type_traits/is_fundamental.h>

namespace sk {
    namespace impl {
        template<class T>
        struct is_compound : sk::bool_constant<
            !sk::is_fundamental<T>
        > {};
    }
    
    template<class T>
    constexpr bool is_compound = impl::is_compound<T>::value;
}
