#pragma once

#include <sk/type_traits/integral_constant.h>
#include <sk/type_traits/has_feature.h>

namespace sk {
    namespace impl {
#if (_MSC_VER >= 1900) || __has_feature(is_union) || (_GNUC_VER >= 403)
        template <class T>
        struct is_union : sk::bool_constant<__is_union(T)> {};
#else
        template <class> struct is_union : sk::false_type {};
#endif
        
#if (_MSC_VER >= 1900) || __has_feature(is_class) || (_GNUC_VER >= 403)
        template <class T>
        struct is_class : sk::bool_constant<__is_class(T)> {};
#else
        template <class T> sk::true_type  is_class_test(int T::*);
        template <class T> sk::false_type is_class_test(...);
        
        template <class T>
        struct is_class : sk::bool_constant<decltype(is_class_test<T>(0))::value> {};
#endif
        
        template<class T>
        struct is_composite : sk::bool_constant<is_class<T>::value || is_union<T>::value> {};
    }
    
    template<class T>
    constexpr bool is_union = impl::is_union<T>::value;
    
    template<class T>
    constexpr bool is_class = impl::is_class<T>::value;
    
    template<class T>
    constexpr bool is_composite = impl::is_composite<T>::value;
}
