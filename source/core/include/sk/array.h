#pragma once

#include <sk/utility/forward.h>
#include <sk/utility/move.h>
#include <sk/array_view.h>

#include <assert.h>
#include <string.h>
#include <type_traits>
#include <utility>

/*
 array<T>

 Simple array that can change size at runtime.
 Every change in size reallocates.
 Every object in the array is always initialized.
*/

namespace sk {

    template<class Tobj>
    class array {
    protected:
        static const bool trivial_copy = std::is_trivially_copyable<Tobj>::value;
        static const bool trivial_del = std::is_trivially_destructible<Tobj>::value;

        int   m_size = 0;
        Tobj* m_array = nullptr;
    public:
        static array adopt(Tobj*& arr, int size);

        array() {}
        template<class...Tfwd>
        array(int size, Tfwd...ctor_args) { set(size, sk::forward<Tfwd>(ctor_args)...); }
        ~array() { clear(); }

        array(const array& from);
        array(array&& from);

        array& operator=(const array& from);
        array& operator=(array&& from);

        // set size of sarray, using ctor_args to construct any new objects when increasing size
        template<class...Tfwd>
        void set(int new_size, Tfwd...ctor_args);

        // deallocate and set size to 0
        void clear();

        int capacity() const { return m_size; }
        int size() const { return m_size; }
        int bytes() const { return m_size*sizeof(Tobj); }
        Tobj* begin() const { return m_array; }
        Tobj* end() const { return m_array + m_size; }
        Tobj& front() const { return *m_array; }
        Tobj& back() const { return *(m_array + m_size - 1); }

        Tobj* at(int index) const;
        Tobj& operator[](int index) const;
    };

    template<class Tobj>
    using array2d = array<array<Tobj>>;

    template<class Tobj>
    using array3d = array<array2d<Tobj>>;


    template<class Tobj>
    array<Tobj> array<Tobj>::adopt(Tobj*& arr, int size) {
        array<Tobj> ret;
        ret.m_array = arr;
        arr = nullptr;
        ret.m_size = size;
        return sk::move(ret);
    }

    template<class Tobj>
    array<Tobj>::array(const array& from) {
        set(from.m_size);
        for(int i = 0; i < m_size; ++i)
            m_array[i] = from.m_array[i];
    }

    template<class Tobj>
    array<Tobj>::array(array&& from) : m_size(from.m_size), m_array(from.m_array) {
        from.m_array = nullptr;
    }

    template<class Tobj>
    array<Tobj>& array<Tobj>::operator=(const array& from) {
        if(m_array==from.m_array) return *this;
        if(from.m_size!=m_size)
        {
            if(m_array) clear();
            set(from.m_size);
        }
        for(int i = 0; i < m_size; ++i)
            m_array[i] = from.m_array[i];
        return *this;
    }

    template<class Tobj>
    array<Tobj>& array<Tobj>::operator=(array&& from) {
        if(m_array==from.m_array) return *this;
        if(m_array) clear();
        m_array = from.m_array;
        m_size = from.m_size;
        from.m_array = nullptr;
        return *this;
    }

    template<class Tobj>
    template<class...Tfwd>
    void array<Tobj>::set(int new_size, Tfwd...construct_args) {
        if(new_size==m_size) return;
        if(new_size <= 0)
        {
            clear();
            return;
        }

        Tobj* new_array = (Tobj*)(::operator new(new_size*sizeof(Tobj)));

        if(m_array)
        {
            int min_size = new_size < m_size ? new_size : m_size;

            if(trivial_copy) memcpy(new_array, m_array, min_size*sizeof(Tobj));
            if(!trivial_copy || !trivial_del)
                for(int i = 0; i < min_size; ++i)
                {
                    if(!trivial_copy) new (new_array + i) Tobj(std::move(m_array[i]));
                    if(!trivial_del) m_array[i].~Tobj();
                }
            if(!trivial_del) for(int i = min_size; i < m_size; ++i)
                m_array[i].~Tobj();

            ::operator delete(m_array);
            m_array = nullptr;
        }
        else assert(m_size == 0);

        for(int i = m_size; i < new_size; ++i)
            new (new_array + i) Tobj(std::forward<Tfwd>(construct_args)...);

        m_array = new_array;
        m_size = new_size;
    }

    template<class Tobj>
    void array<Tobj>::clear() {
        if(m_array)
        {
            if(!trivial_del)
                for(int i = 0; i < m_size; ++i) m_array[i].~Tobj();
            ::operator delete(m_array);
            m_array = nullptr;
        }
        m_size = 0;
    }

    template<class Tobj>
    Tobj* array<Tobj>::at(int index) const {
        if(index < 0 || index >= m_size) return nullptr;
        return &m_array[index];
    }

    template<class Tobj>
    Tobj& array<Tobj>::operator[](int index) const {
        assert(index >= 0 || index < size());
        return m_array[index];
    }
}
