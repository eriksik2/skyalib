#pragma once

#include <assert.h>
#include <sk/utility/index_sequence.h>

/*
 statarray<int, T>

 RAII wrapper for c array.
 Doesn't allocate memory.
 Size has to be known at compile time.
 Every object in the array is always initialized.
*/

namespace sk {
    
    template<int TSize, class Tobj>
    class statarray {
    protected:
        Tobj m_array[TSize];
        
        template<sk::size_t...Irest> // default ctor
        constexpr statarray(sk::index_sequence<Irest...>) : m_array{ (Irest, Tobj())... } {}
        
        template<int Asize, sk::size_t...I, sk::size_t...Irest> // value ctor (c array)
        constexpr statarray(const Tobj(&arr)[Asize], sk::index_sequence<I...>, sk::index_sequence<Irest...>) : m_array{ arr[I]..., (Irest, Tobj())... } {}
        
        template<int Asize, sk::size_t...I, sk::size_t...Irest> // copy ctor
        constexpr statarray(const statarray<Asize, Tobj>& arr, sk::index_sequence<I...>, sk::index_sequence<Irest...>) : m_array{ arr[I]..., (Irest, Tobj())... } {}
    public:
        
        constexpr statarray() : statarray(sk::make_index_sequence<TSize>{}) {} // default ctor
        
        template<int Asize, int Rsize = (Asize > TSize) ? TSize : Asize> // value ctor (c array)
        constexpr statarray(const Tobj(&arr)[Asize]) : statarray(arr, sk::make_index_sequence<Rsize>{}, sk::make_index_sequence<TSize - Rsize>{}) {}
        
        template<int Asize, int Rsize = (Asize > TSize) ? TSize : Asize> // copy ctor
        constexpr statarray(const statarray<Asize, Tobj>& arr) : statarray(arr, sk::make_index_sequence<Rsize>{}, sk::make_index_sequence<TSize - Rsize>{}) {}
        
        template<class...TObjvar, sk::enable_if<sizeof...(TObjvar) == TSize> = 0>
        constexpr statarray(TObjvar...list) : m_array{ list... } {}

        constexpr static int capacity() { return TSize; }
        constexpr static int size() { return TSize; }
        constexpr static int bytes() { return TSize*sizeof(Tobj); }
        
        constexpr Tobj* begin() { return m_array; }
        constexpr Tobj* end() { return m_array + TSize; }
        constexpr const Tobj* cbegin() const { return m_array; }
        constexpr const Tobj* cend() const { return m_array + TSize; }
        
        constexpr Tobj& front() const { return *m_array; }
        constexpr Tobj& back() const { return *(m_array + TSize - 1); }
        
        constexpr Tobj* at(int index);
        constexpr Tobj& operator[](int index);
        constexpr const Tobj* at(int index) const;
        constexpr const Tobj& operator[](int index) const;
    };
    
    template<int TSize, class Tobj>
    constexpr Tobj* statarray<TSize, Tobj>::at(int index) {
        if(index < 0 || index >= TSize) return nullptr;
        return &m_array[index];
    }
    
    template<int TSize, class Tobj>
    constexpr Tobj& statarray<TSize, Tobj>::operator[](int index) {
        assert(index >= 0 || index < TSize);
        return m_array[index];
    }
    
    template<int TSize, class Tobj>
    constexpr const Tobj* statarray<TSize, Tobj>::at(int index) const {
        if(index < 0 || index >= TSize) return nullptr;
        return &m_array[index];
    }
    
    template<int TSize, class Tobj>
    constexpr const Tobj& statarray<TSize, Tobj>::operator[](int index) const {
        assert(index >= 0 || index < TSize);
        return m_array[index];
    }
    
    template<int TSizeX, int TSizeY, class Tobj>
    using statarray2d = statarray<TSizeX, statarray<TSizeY, Tobj>>;
    
    template<int TSizeX, int TSizeY, int TSizeZ, class Tobj>
    using statarray3d = statarray<TSizeX, statarray2d<TSizeY, TSizeZ, Tobj>>;
}
