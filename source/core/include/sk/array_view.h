#pragma once


#include <sk/assert.h> // sk_assert
#include <sk/type_traits/enable_if.h>
#include <sk/type_traits/is_same.h>
#include <sk/type_traits/remove_cv.h>
#include <sk/type_traits/remove_reference.h>

#include <sk/type_traits/void_t.h>
#include <sk/type_traits/declval.h>

#include <string.h> // strchr
#include <wchar.h>  // wcschr

/*
 array_view<T>

 non-owning const array type
*/

// !!!!! WARNING !!!!!
// not const correct. c++ doesn't have const constructors

namespace sk {
    namespace impl {
        template<class Tobj>
        class array_view {
        protected:
            Tobj* m_begin;
            Tobj* m_end;
        public:
            // default constructor
            constexpr array_view() : m_begin(nullptr), m_end(nullptr) {}
            
            // copy constructor
            constexpr array_view(const array_view<sk::remove_const<Tobj>>& copy) : m_begin(copy.begin()), m_end(copy.end()) {}
            
            // generic constructor
            template<class Tarr, class = sk::type_t<sk::enable_if<!sk::is_same<sk::remove_reference<Tarr>, array_view<const Tobj>>>, decltype(sk::remove_reference<Tarr>(sk::declval<array_view<const Tobj>>()))>>
            constexpr array_view(Tarr&& arr) : m_begin(arr.begin()), m_end(arr.end()) {}
            
            // begin/end constructor
            constexpr array_view(Tobj* begin, Tobj* end) : m_begin(begin), m_end(end) {}
            
            // c array constructor
            template<int N>
            constexpr array_view(Tobj(&begin)[N]) : m_begin(begin), m_end(begin + N) {}
            
            // c string constructorwe
            template<class T = Tobj>
            constexpr array_view(Tobj* c_str, sk::enable_if<sk::is_same<T, char>> = 0) : m_begin(c_str), m_end(strchr(c_str, '\0')) {}
            
            // wide c string constructor
            template<class T = Tobj>
            constexpr array_view(Tobj* c_str, sk::enable_if<sk::is_same<T, wchar_t>> = 0) : m_begin(c_str), m_end(wcschr(c_str, '\0')) {}
            
            // converts to false if empty, true otherwise
            constexpr explicit operator bool() const { return m_begin != m_end; }
            
            // convert to const
            //constexpr operator array_view<const Tobj>() const { return array_view<const Tobj>(m_begin, m_end);}
            
            constexpr int size() const { return int(m_end - m_begin); }
            constexpr int bytes() const { return size()*sizeof(Tobj); }
            
            constexpr Tobj* begin() const { return m_begin; }
            constexpr Tobj* end()   const { return m_end; }
            
            constexpr Tobj& front() const { sk_assert(m_begin != nullptr); return *m_begin; }
            constexpr Tobj& back() const { sk_assert(m_end > m_begin); return *(m_end - 1); }
            
            // element access with bounds checking
            constexpr Tobj* at(int index) const;
            
            // element access without bounds checking
            constexpr Tobj& operator[](int index) const;
        };
        
        template<class Tobj>
        constexpr Tobj* array_view<Tobj>::at(int index) const {
            if(index < 0 || index >= size()) return nullptr;
            return &m_begin[index];
        }
        
        template<class Tobj>
        constexpr Tobj& array_view<Tobj>::operator[](int index) const {
            sk_assert(index >= 0 || index < size(), "index is out of bounds");
            return m_begin[index];
        }
    }
    template<class Tobj>
    using array_view = impl::array_view<Tobj>;
    
    using string_view = array_view<char>;
    using const_string_view = array_view<const char>;
    using wstring_view = array_view<wchar_t>;
    using const_wstring_view = array_view<const wchar_t>;
}
