#pragma once

#include <sk/utility/forward.h>
#include <sk/utility/move.h>
#include <sk/utility/swap.h>
#include <sk/type_traits/enable_if.h>
/*
 
*/

namespace sk {
    
    template<class Tobj>
    struct tree_element {
        Tobj m_value;
        tree_element* m_parent = nullptr;
        tree_element* m_left = nullptr;
        tree_element* m_right = nullptr;
        
        template<class _Tobj, sk::enable_if<sk::is_same<_Tobj, Tobj>> = 0>
        tree_element(_Tobj&& val) : m_value(sk::forward<_Tobj>(val)) {}
        
        tree_element(tree_element&& move)
            : m_value(sk::move(move.m_value))
            , m_parent(move.m_parent)
            , m_left(move.m_left)
            , m_right(move.m_right) {
            move.m_left = move.m_right = nullptr;
        }
        
        tree_element& operator=(tree_element&& move) {
            m_value = sk::move(move.m_value);
            m_parent = move.m_parent;
            sk::swap(m_left, move.m_left);
            sk::swap(m_right, move.m_right);
            return *this;
        }
        
        ~tree_element() {
            delete m_left;
            delete m_right;
        }
    }
    
}
