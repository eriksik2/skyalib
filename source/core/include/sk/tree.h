#pragma once

#include <sk/utility/forward.h>
#include <sk/type_traits/enable_if.h>
#include <sk/type_traits/is_same.h>
#include <sk/type_traits/is_comparable.h>

#include <stdio.h>

namespace sk { namespace tree {
    
    template<class Tnode, class Tvalue, sk::enable_if<sk::is_same<Tvalue, decltype(Tnode::m_value)>> = 0>
    Tnode* insert(Tnode*& tree, Tvalue&& value){
        Tnode** new_node = tree ? nullptr : &tree;
        Tnode* this_node = tree;
        while(!new_node){
            if(value <= this_node->m_value) {
                if(this_node->m_left) this_node = this_node->m_left;
                else new_node = &this_node->m_left;
            }else{
                if(this_node->m_right) this_node = this_node->m_right;
                else new_node = &this_node->m_right;
            }
        }
        *new_node = new Tnode(sk::forward<Tvalue>(value));
        return *new_node;
    }
    
    template<class Tnode>
    void remove(Tnode** parent_edge){
        if(!parent_edge || !(*parent_edge)) return;
        Tnode* this_node = *parent_edge;
        bool left = this_node->m_left, right = this_node->m_right;
        if(!left && !right){
            *parent_edge = nullptr;
        }else if(left && !right){
            *parent_edge = this_node->m_left;
            this_node->m_left = nullptr;
        }else if(!left && right){
            *parent_edge = this_node->m_right;
            this_node->m_right = nullptr;
        }else{
            Tnode** right_min_edge = &this_node->m_right;
            while((*right_min_edge)->m_left) right_min_edge = &(*right_min_edge)->m_left;
            Tnode* right_min = *right_min_edge;
            if(right_min->m_right) *right_min_edge = right_min->m_right;
            else *right_min_edge = nullptr;
            right_min->m_left = this_node->m_left;
            right_min->m_right = this_node->m_right;
            this_node->m_left = nullptr;
            this_node->m_right = nullptr;
            *parent_edge = right_min;
        }
        delete this_node;
    }
    
    template<class Tnode, class Tvalue, sk::enable_if<sk::is_same<Tvalue, decltype(Tnode::m_value)>> = 0>
    bool remove(Tnode*& tree, Tvalue&& value){
        if(!tree) return false;
        Tnode** parent_edge = &tree;
        Tnode* this_node;
        while(1){
            this_node = (*parent_edge);
            if(value == this_node->m_value){
                remove(parent_edge);
                return true;
            }
            if(value < this_node->m_value){
                if(!this_node->m_left) return false;
                parent_edge = &this_node->m_left;
            }else{
                if(!this_node->m_right) return false;
                parent_edge = &this_node->m_right;
            }
        }
    }
    
    template<class Tnode, class Tvalue, sk::enable_if<
        sk::is_equality_comparable<Tvalue, decltype(Tnode::m_value)> &&
        sk::is_less_than_comparable<Tvalue, decltype(Tnode::m_value)>> = 0>
    Tvalue* find(const Tnode* tree, Tvalue&& value){
        if(!tree) return nullptr;
        if(value == tree->m_value) return &tree->m_value;
        if(value < tree->m_value) return sk::tree::find(tree->m_left, value);
        return sk::tree::find(tree->m_right, value);
    }
    
    template<class Tnode, bool call = true>
    void print(const Tnode* tree){
        if(!tree) return;
        sk::tree::print<Tnode, false>(tree->m_left);
        printf("%d ", tree->m_value);
        sk::tree::print<Tnode, false>(tree->m_right);
        if(call) printf("\n");
    }
    
}}
