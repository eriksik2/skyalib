#pragma once

// only want assert when debugging
#if defined(DEBUG)
    #include <stdlib.h> // abort
    #include <stdio.h> // printf
    #include <string.h> // strlen

    // define assert macro
    #if defined(sk_assert)
        #undef sk_assert
    #endif
    #define sk_assert(COND, ...) ((COND) ? (void)0 : (printf("%s:%d: in function '%s()':\n\tASSERT FAILED: %s\n%s%scalling abort()...\n", __FILE__, __LINE__, __func__, #COND, strlen((__VA_ARGS__"")) ? "\tASSERT REASON: \"" : "", strlen((__VA_ARGS__"")) ? (__VA_ARGS__"\"\n") : ""), abort()))
#else
    // define assert as 0
    #if defined(sk_assert)
        #undef sk_assert
    #endif
    #define sk_assert(...) ((void)0)
#endif
