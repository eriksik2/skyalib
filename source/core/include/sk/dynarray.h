#pragma once

#include <sk/assert.h> // sk_assert
#include <sk/utility.h> // sk::move, sk::forward
#include <sk/type_traits.h> // sk::is_trivially_copyable, sk::is_trivially_destructible
#include <sk/array_view.h>

#include <string.h>    // memcpy

#include <new>         // operator new

/*
 dynarray<T>

 probably similar to std::vector
*/

namespace sk {

    template<class Tobj>
    class dynarray {
    protected:
        // TODO use size_t instead of int for these?
        int   m_capacity = 0;
        int   m_size = 0;
        Tobj* m_array = nullptr;
    public:
        // default constructor
        dynarray() {}
        
        // list constructor
        template<class...Tobjlist, sk::enable_if<(sk::is_convertible<Tobjlist, Tobj> && ...)> = 0>
        dynarray(Tobjlist&&...args) {
            set_capacity(sizeof...(args));
            push(sk::forward<Tobjlist>(args)...);
        }
        
        // sk::array_view constructor
        dynarray(const sk::array_view<const Tobj>& array) {
            set_capacity(array.size());
            if(sk::is_trivially_copyable<Tobj>)
                memcpy(m_array, array.begin(), array.bytes());
            else for(int i = 0; i < m_size; ++i) new(m_array + i) Tobj(array[i]);
            m_size = array.size();
        }
        
        // reserving constructor
        explicit dynarray(int capacity) { set_capacity(capacity); }
        
        // destructor
        ~dynarray() { clear_capacity(); }
        
        // copy constructor
        dynarray(const dynarray& other);
        // move constructor
        dynarray(dynarray&& other);
        
        // copy assignment
        dynarray& operator=(const dynarray& other);
        // move assignment
        dynarray& operator=(dynarray&& other);

        int capacity() const { return m_capacity; }
        int size() const { return m_size; }
        int bytes() const { return m_size*sizeof(Tobj); }
        Tobj* begin() const { return m_array; }
        Tobj* end() const { return m_array + m_size; }
        Tobj& front() const { return *m_array; }
        Tobj& back() const { return *(m_array + m_size - 1); }
        
        // increase capacity of dynarray to 'capacity'
        void reserve(int capacity);
        
        // remove all elements from dynarray
        void clear();
        
        // construct new elements with move/copy constructor at end of dynarray
        template<class...Tobjlist>
        void push(Tobjlist&&...objects);
        
        // remove element from end of dynarray
        void pop();
        
        // construct new element with 'args' at end of dynarray
        template<class...Targs>
        void emplace(Targs&&...args);
        
        // remove element at location 'at'
        void remove(Tobj* at);
        
        // remove element at location 'at', but keep order of remaining elements
        void remove_sorted(Tobj* at);

        // element access with bounds checking
        Tobj* at(int index) const;
        
        // element access without bounds checking
        Tobj& operator[](int index) const;
    protected:
        
        // set capacity to an exact value, this is the main function for changing the size of the allocated storage
        void set_capacity(int capacity);
        
        // deallocate and reset dynarray to it's default state
        void clear_capacity();

        // call Tobj constructor with 'args' at dynarray::end(), then increment m_size
        template<class...Targ>
        Tobj* construct_back(Targ&&...args);
        
        // decrement m_size, then call Tobj destructor at dynarray::end()
        void destruct_back();

        // used to expand parameter packs
        template<class...T>
        void dummy(T&&...) const {}
    };

    template<class Tobj>
    dynarray<Tobj>::dynarray(const dynarray & other) {
        set_capacity(other.m_capacity);
        if(sk::is_trivially_copyable<Tobj>) memcpy(m_array, other.m_array, other.m_size*sizeof(Tobj));
        else for(int i = 0; i < other.m_size; ++i) new(m_array + i) Tobj(other.m_array[i]);
        m_size = other.m_size;
    }

    template<class Tobj>
    dynarray<Tobj>::dynarray(dynarray && other)
        : m_capacity(other.m_capacity)
        , m_size(other.m_size)
        , m_array(other.m_array) {
        other.m_array = nullptr;
        other.m_size = 0;
        other.m_capacity = 0;
    }

    template<class Tobj>
    dynarray<Tobj>& dynarray<Tobj>::operator=(const dynarray & other) {
        set_capacity(other.m_capacity);
        if(sk::is_trivially_copyable<Tobj>) memcpy(m_array, other.m_array, other.m_size*sizeof(Tobj));
        else {
            for(int i = 0; i < m_size; ++i) m_array[i] = other.m_array[i];
            for(int i = m_size; i < other.m_size; ++i) new(m_array + i) Tobj(other.m_array[i]);
        }
        m_size = other.m_size;
        return *this;
    }

    template<class Tobj>
    dynarray<Tobj>& dynarray<Tobj>::operator=(dynarray && other) {
        if(m_array==other.m_array) return *this;
        if(m_array) clear_capacity();
        m_array = other.m_array;
        m_size = other.m_size;
        m_capacity = other.m_capacity;
        other.m_array = nullptr;
        other.m_size = 0;
        other.m_capacity = 0;
        return *this;
    }

    template<class Tobj>
    void dynarray<Tobj>::reserve(int capacity) {
        if(capacity <= m_capacity) return;
        set_capacity(capacity);
    }

    template<class Tobj>
    void dynarray<Tobj>::clear() {
        if(!sk::is_trivially_destructible<Tobj>)
            for(int i = 0; i < m_size; i++) m_array[i].~Tobj();
        m_size = 0;
    }

    template<class Tobj>
    template<class...Tobjlist>
    void dynarray<Tobj>::push(Tobjlist&&...objects) {
        while(m_size + sizeof...(Tobjlist) > m_capacity) set_capacity(m_capacity?m_capacity*2:16);

        auto tuple = sk::tuple<Tobjlist...>(sk::forward<Tobjlist>(objects)...);
        for(auto item : tuple) {
            construct_back<decltype(item)>(sk::forward<decltype(item)>(item));
        }
    }

    template<class Tobj>
    void dynarray<Tobj>::pop() {
        sk_assert(m_size > 0, "cannot remove element from empty dynarray"); // dynarray has to have at least one element to be able to remove one element
        destruct_back();
    }

    template<class Tobj>
    template<class...Targs>
    void dynarray<Tobj>::emplace(Targs&&...args) {
        if(m_size >= m_capacity) set_capacity(m_capacity?m_capacity*2:16);
        construct_back(sk::forward<Targs>(args)...);
    }

    template<class Tobj>
    void dynarray<Tobj>::remove(Tobj * at) {
        sk_assert(at >= begin() && at < end(), "cannot remove element outside of dynarray"); // 'at' has to be a pointer to one of the elements in this dynarray
        if(at != &back()) *at = sk::move(back());
        destruct_back();
    }
    template<class Tobj>
    void dynarray<Tobj>::remove_sorted(Tobj * at) {
        sk_assert(at >= begin() && at < end(), "cannot remove element outside of dynarray"); // 'at' has to be a pointer to one of the elements in this dynarray
        if(sk::is_trivially_copyable<Tobj>)
            memcpy(at, at+1, (end()-(at+1))*sizeof(Tobj));
        else {
            auto* back = end()-1;
            for(auto* it = at; it < back; ++it) {
                if(!sk::is_trivially_destructible<Tobj>) it->~Tobj();
                new(it) Tobj(std::move(*(it+1)));
            }
            if(!sk::is_trivially_destructible<Tobj>) back->~Tobj();
        }
        --m_size;
    }

    template<class Tobj>
    Tobj* dynarray<Tobj>::at(int index) const {
        if(index < 0 || index >= m_size) return nullptr;
        return &m_array[index];
    }
    template<class Tobj>
    Tobj& dynarray<Tobj>::operator[](int index) const {
        sk_assert(index >= 0 || index < m_size, "index is out of bounds");
        return m_array[index];
    }

    template<class Tobj>
    void dynarray<Tobj>::set_capacity(int capacity) {
        if(capacity == m_capacity) return;
        if(capacity <= 0) {
            clear_capacity();
            return;
        }
        Tobj* new_array = (Tobj*)malloc(capacity*sizeof(Tobj));//Tobj* new_array = (Tobj*)(::operator new(capacity*sizeof(Tobj)));

        if(m_array) {
            int min_size = capacity < m_size ? capacity : m_size;

            if(sk::is_trivially_copyable<Tobj>) memcpy(new_array, m_array, min_size*sizeof(Tobj));
            if(!sk::is_trivially_copyable<Tobj> || !sk::is_trivially_destructible<Tobj>)
                for(int i = 0; i < min_size; ++i) {
                    if(!sk::is_trivially_copyable<Tobj>) new(new_array + i) Tobj(sk::move(m_array[i]));
                    if(!sk::is_trivially_destructible<Tobj>) m_array[i].~Tobj();
                }
            if(!sk::is_trivially_destructible<Tobj>) for(int i = min_size; i < m_size; ++i)
                m_array[i].~Tobj();

            if(capacity < m_size) m_size = capacity;
            
            free(m_array);//::operator delete(m_array);
            m_array = nullptr;
        }
        else sk_assert(m_size == 0 && m_capacity == 0, "if this gets triggered theres a bug in sk::dynarray");

        m_array = new_array;
        m_capacity = capacity;
    }

    template<class Tobj>
    void dynarray<Tobj>::clear_capacity() {
        if(m_array) {
            clear();
            free(m_array);//::operator delete(m_array);
            m_array = nullptr;
            m_capacity = 0;
        }
    }

    template<class Tobj>
    template<class...Targ>
    Tobj* dynarray<Tobj>::construct_back(Targ&&...args) {
        Tobj* obj = new(end()) Tobj(sk::forward<Targ>(args)...);
        ++m_size;
        return obj;
    }
    template<class Tobj>
    void dynarray<Tobj>::destruct_back() {
        --m_size;
        if(!sk::is_trivially_destructible<Tobj>) end()->~Tobj();
    }
}
