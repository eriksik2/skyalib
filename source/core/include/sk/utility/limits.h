#pragma once

#include <sk/type_traits/enable_if.h>
#include <sk/type_traits/is_signed.h>

namespace sk {
    
    // pow
    namespace impl {
        template<int base, int exp, class T, bool err_on_overflow = true, class = int>
        struct pow {
            constexpr static T value = pow<base, exp-1, T>::value * base;
        };
        template<class T, int base, int exp, bool err_on_overflow>
        struct pow<base, exp, T, err_on_overflow, sk::enable_if<(exp<=0)>> {
            constexpr static T value = 1;
        };
        template<class T, int base, int exp, bool err_on_overflow>
        struct pow<base, exp, T, err_on_overflow, sk::enable_if<(exp>=sizeof(T)*8 - 1 && base>=2)>> {
            static_assert(!err_on_overflow, "data type is not big enough to hold value");
            constexpr static T value = pow<2, sizeof(T)*8 - 2, T>::value + (pow<2, sizeof(T)*8 - 2, T>::value - 1);
        };
    }
    template<int base, int exp, class T = long long, bool err_on_overflow = true>
    constexpr static T pow = impl::pow<base, exp, T, err_on_overflow>::value;
    // pow
    
    
    namespace impl {
        template<class T, bool is_signed = sk::is_signed<T>> struct limits {};
        
        template<class T>
        struct limits<T, true> {
            constexpr static bool is_signed = true;
            constexpr static T max = sk::pow<2, sizeof(T)*8, T, false>;
            constexpr static T min = -sk::pow<2, sizeof(T)*8, T, false>-1;
        };
        
        template<class T>
        struct limits<T, false> {
            constexpr static bool is_signed = false;
            constexpr static T min = T(0);
            constexpr static T max = T(-1);
        };
    }
    template<class T>
    using limits = impl::limits<T>;
}


#include <climits>
static_assert(sk::limits<char>::min == CHAR_MIN, "");
static_assert(sk::limits<char>::max == CHAR_MAX, "");
static_assert(sk::limits<unsigned char>::min == 0, "");
static_assert(sk::limits<unsigned char>::max == UCHAR_MAX, "");
static_assert(sk::limits<unsigned short>::min == 0, "");
static_assert(sk::limits<unsigned short>::max == USHRT_MAX, "");
static_assert(sk::limits<unsigned int>::min == 0, "");
static_assert(sk::limits<unsigned int>::max == UINT_MAX, "");
static_assert(sk::limits<unsigned long>::min == 0, "");
static_assert(sk::limits<unsigned long>::max == ULONG_MAX, "");
static_assert(sk::limits<unsigned long long>::min == 0, "");
static_assert(sk::limits<unsigned long long>::max == ULLONG_MAX, "");
static_assert(sk::limits<signed char>::min == SCHAR_MIN, "");
static_assert(sk::limits<signed char>::max == SCHAR_MAX, "");
static_assert(sk::limits<short>::min == SHRT_MIN, "");
static_assert(sk::limits<short>::max == SHRT_MAX, "");
static_assert(sk::limits<int>::min == INT_MIN, "");
static_assert(sk::limits<int>::max == INT_MAX, "");
static_assert(sk::limits<long>::min == LONG_MIN, "");
static_assert(sk::limits<long>::max == LONG_MAX, "");
static_assert(sk::limits<long long>::min == LLONG_MIN, "");
static_assert(sk::limits<long long>::max == LLONG_MAX, "");
