#pragma once

#include <sk/utility/move.h>

namespace sk {
    template<class T1, class T2>
    void swap(T1&& a, T2&& b){
        auto tmp = a;
        a = b;
        b = sk::move(tmp);
    }
}
