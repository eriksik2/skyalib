#pragma once


#include <sk/utility/move.h>
#include <sk/utility/forward.h>
#include <sk/utility/types.h>
#include <sk/utility/index_sequence.h>
#include <sk/type_traits/enable_if.h>
#include <sk/type_traits/is_same.h>
#include <sk/statarray.h>

namespace sk {
    namespace impl {
        // tuple_end_type is used to be able to range iterate over tuple with all the same types.
        class tuple_end_type {};

        // tuple_begin_type is used to be able to range iterate over tuple with all the same types.
        template<sk::size_t N, class T>
        class tuple_begin_type {
            sk::statarray<N, T> data;
            sk::size_t index = 0;
        public:

            tuple_begin_type(sk::statarray<N, T> data) : data(data) {}

            tuple_begin_type<N, T>& operator++() {
                this->index++;
                return *this;
            }

            bool operator==(tuple_end_type) {
                return this->index == N;
            }

            T const& operator*() {
                return this->data[(int)this->index];
            }
        };
    }

    template<sk::size_t I, class T>
    class tuple_element {
        T m_value;
    public:
        static constexpr sk::size_t index = I;
        using type = T;
        
        tuple_element() = default;
        tuple_element(const tuple_element&) = default;
        tuple_element(tuple_element&&) = default;
        tuple_element& operator=(const tuple_element&) = default;
        tuple_element& operator=(tuple_element&&) = default;

        template<typename Tt>
        explicit tuple_element(Tt&& fwd) : m_value(sk::forward<Tt>(fwd)) {}

        T const& get() { return m_value; }
    };
    
    template<class S, class...T> class tuple_impl;
    
    template<sk::size_t...I, class...T>
    class tuple_impl<index_sequence<I...>, T...> : public tuple_element<I, T>... {
    public:
        tuple_impl() = default;
        tuple_impl(const tuple_impl&) = default;
        tuple_impl(tuple_impl&&) = default;
        tuple_impl& operator=(const tuple_impl&) = default;
        tuple_impl& operator=(tuple_impl&&) = default;
        
        template<sk::size_t N1, sk::size_t N2, class T1, class...Tr>
        struct at_impl {
            using type = typename at_impl<N1, N2+1, Tr...>::type;
        };
        template<sk::size_t N, class T1, class...Tr>
        struct at_impl<N, N, T1, Tr...> {
            using type = T1;
        };
        
        template<sk::size_t N>
        using at = typename at_impl<N, 0, T...>::type;
        
        constexpr static sk::size_t size = sizeof...(T);
        
        template<class...Te> tuple_impl(Te&&...elements)
        : tuple_element<I, T>(sk::forward<Te>(elements))... {}

        template<sk::enable_if<sk::is_same<T...>> = 0>
        sk::statarray<sizeof...(T), at<0>> to_array() {
            sk::statarray<sizeof...(T), at<0>> statarray;
            dummy(set_at<I>(statarray)...);
            return statarray;
        }

        template<sk::enable_if<sk::is_same<T...>> = 0>
        impl::tuple_begin_type<size, at<0>> begin() {
            return impl::tuple_begin_type<size, at<0>>(to_array());
        }

        template<sk::enable_if<sk::is_same<T...>> = 0>
        impl::tuple_end_type end() {
            return impl::tuple_end_type();
        }

        template<sk::size_t Idx>
        at<Idx>const & get() {
            return ((tuple_element<Idx, at<Idx>>*)this)->get();
        }

    private:
        template<sk::size_t N>
        int set_at(sk::statarray<sizeof...(T), at<0>>& array) {
            array[N] = this->get<N>();
            return 0;
        }

        template<class...T>
        void dummy(T&&...) const {}
    };
    
    template<class...T>
    class tuple : public tuple_impl<make_index_sequence<sizeof...(T)>, sk::remove_reference<T>...> {
    public:
        tuple() = default;
        tuple(const tuple&) = default;
        tuple(tuple&&) = default;
        tuple& operator=(const tuple&) = default;
        tuple& operator=(tuple&&) = default;
        
        template<class...Te> tuple(Te&&...elements)
        : tuple_impl<make_index_sequence<sizeof...(T)>, sk::remove_reference<T>...>(sk::forward<Te>(elements)...) {}


        template<sk::size_t N1, sk::size_t N2, class T1, class...Tr>
        struct at_impl {
            using type = typename at_impl<N1, N2+1, Tr...>::type;
        };
        template<sk::size_t N, class T1, class...Tr>
        struct at_impl<N, N, T1, Tr...> {
            using type = T1;
        };
        template<sk::size_t N>
        using at_type = typename at_impl<N, 0, T...>::type;
    };
    
    
}
