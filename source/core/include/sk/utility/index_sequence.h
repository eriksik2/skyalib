#pragma once

#include <sk/utility/types.h>
#include <sk/utility/type_concat.h>

namespace sk {

    template<sk::size_t...I>
    struct index_sequence {
        using type = index_sequence<I...>;
    };

    namespace impl {
        template<sk::size_t N> struct make_index_sequence
            : sk::type_concat<typename make_index_sequence<N-1>::type, index_sequence<N-1>> {};
        template<> struct make_index_sequence<1> : index_sequence<0> {};
        template<> struct make_index_sequence<0> : index_sequence<> {};
    }

    template<sk::size_t N>
    using make_index_sequence = typename impl::make_index_sequence<N>::type;
}
