#pragma once

#include <sk/type_traits/remove_reference.h>
#include <sk/type_traits/is_reference.h>

namespace sk {
    template<class T>
    constexpr T&& forward(sk::remove_reference<T>& t) noexcept
    {
        return static_cast<T&&>(t);
    }
    
    template<class T>
    constexpr T&& forward(sk::remove_reference<T>&& t) noexcept
    {
        static_assert(!sk::is_lvalue_reference<T>, "Can not forward an rvalue as an lvalue.");
        return static_cast<T&&>(t);
    }
}
