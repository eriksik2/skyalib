#pragma once

/* type_concat<class, class>
 *      Passing two types of the same template
 *      gives one type of that template with all
 *      the two types parameters concatenated.
 */

#include <sk/utility/types.h>

namespace sk {
    namespace impl {
        template<class T1, class T2> struct type_concat;
        
        template<template<class...>class T, class...T1, class...T2>
        struct type_concat<T<T1...>, T<T2...>> {
            using type = T<T1..., T2...>;
        };
        
        template<class Tv, template<Tv...> class T, Tv...T1, Tv...T2>
        struct type_concat<T<T1...>, T<T2...>> {
            using type = T<T1..., T2...>;
        };
    }
    template<class T1, class T2>
    using type_concat = typename impl::type_concat<T1, T2>::type;
    /*template<class T1, class T2> struct type_concat;

    template<template<class...>class T, class...T1, class...T2>
    struct type_concat<T<T1...>, T<T2...>> : T<T1..., T2...> {};

#if defined(_MSC_VER)
    template<template<sk::size_t...> class T, sk::size_t...T1, sk::size_t...T2>
    struct type_concat<T<T1...>, T<T2...>> : T<T1..., T2...> {};

    template<template<int...> class T, int...T1, int...T2>
    struct type_concat<T<T1...>, T<T2...>> : T<T1..., T2...> {};
#else
    template<class Tv, template<Tv...> class T, Tv...T1, Tv...T2>
    struct type_concat<T<T1...>, T<T2...>> : T<T1..., T2...> {};
#endif*/
}
