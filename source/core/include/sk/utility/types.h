#pragma once

#include <stdint.h>
namespace sk {
    extern "C" {
#include <stdint.h>
    }
    using size_t = decltype(sizeof(int));
    using nullptr_t = decltype(nullptr);
}
