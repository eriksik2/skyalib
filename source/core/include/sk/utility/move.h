#pragma once

#include <sk/type_traits/remove_reference.h>

namespace sk {
    template<class T>
    constexpr sk::remove_reference<T>&& move(T&& arg) noexcept {
        return static_cast<sk::remove_reference<T>&&>(arg);
    }
}
