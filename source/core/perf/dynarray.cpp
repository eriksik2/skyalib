#include "picobench/picobench.hpp"

#include <sk/dynarray.h>
#include <vector>

namespace sk {
    template<class T>
    using vector = dynarray<T>;
}

BENCHMARK(vector, default_ctor, skyalib) {
    for(auto _ : state) {
        sk::vector<int> test;
    }
}
BENCHMARK(vector, default_ctor, standard) {
    for(auto _ : state) {
        std::vector<int> test;
    }
}


BENCHMARK(vector, reserving_ctor, skyalib) {
    for(auto _ : state) {
        sk::vector<int> test(512);
    }
}
BENCHMARK(vector, reserving_ctor, standard) {
    for(auto _ : state) {
        std::vector<int> test(512);
    }
}


BENCHMARK(vector, copy_ctor, skyalib) {
    sk::vector<int> test(128);
    for(int i = 0; i < 128; ++i) test.push(i);
    for(auto _ : state) {
        sk::vector<int> test2 = test;
    }
}
BENCHMARK(vector, copy_ctor, standard) {
    std::vector<int> test(128);
    for(int i = 0; i < 128; ++i) test.push_back(i);
    for(auto _ : state) {
        std::vector<int> test2 = test;
    }
}


BENCHMARK(vector, size, skyalib) {
    sk::vector<int> test(128);
    for(int i = 0; i < 128; ++i) test.push(i);
    for(auto _ : state) {
        int t = test.size();
    }
}
BENCHMARK(vector, size, standard) {
    std::vector<int> test(128);
    for(int i = 0; i < 128; ++i) test.push_back(i);
    for(auto _ : state) {
        size_t t = test.size();
    }
}


BENCHMARK(vector, loop, skyalib) {
    sk::vector<int> test(1024);
    for(int i = 0; i < 1024; ++i) test.push(i);
    for(auto _ : state) {
        for(auto&& l : test);
    }
}
BENCHMARK(vector, loop, standard) {
    std::vector<int> test(1024);
    for(int i = 0; i < 1024; ++i) test.push_back(i);
    for(auto _ : state) {
        for(auto&& l : test);
    }
}

BENCHMARK(vector, push, skyalib) {
    sk::vector<int> test;
    for(auto _ : state) {
        test.push(4);
    }
}
BENCHMARK(vector, push, reserve_skyalib) {
    sk::vector<int> test(state.iterations());
    for(auto _ : state) {
        test.push(4);
    }
}
BENCHMARK(vector, push, standard) {
    std::vector<int> test;
    for(auto _ : state) {
        test.push_back(4);
    }
}
BENCHMARK(vector, push, reserve_standard) {
    std::vector<int> test(state.iterations());
    for(auto _ : state) {
        test.push_back(4);
    }
}

BENCHMARK(vector, get_element, skyalib) {
    sk::vector<int> test(128);
    for(int i = 0; i < 128; ++i) test.push(i);
    for(auto _ : state) {
        int i = test[83];
    }
}

BENCHMARK(vector, get_element, standard) {
    std::vector<int> test(128);
    for(int i = 0; i < 128; ++i) test.push_back(i);
    for(auto _ : state) {
        int i = test[83];
    }
}

BENCHMARK(vector, clear, skyalib) {
    sk::vector<int> test(1024);
    for(int i = 0; i < 1024; ++i) test.push(i);
    for(auto _ : state) {
        test.clear();
    }
}

BENCHMARK(vector, clear, standard) {
    std::vector<int> test(1024);
    for(int i = 0; i < 1024; ++i) test.push_back(i);
    for(auto _ : state) {
        test.clear();
    }
}