#define PICOBENCH_IMPLEMENT_WITH_MAIN
#include "picobench/picobench.hpp"


#include <sk/utility/move.h>
#include <sk/dynarray.h>
#include <sk/string.h>
#include <string>

BENCHMARK(string, default_ctor, skyalib) {
    for(auto _ : state) {
        sk::string test;
    }
}
BENCHMARK(string, default_ctor, standard) {
    for(auto _ : state) {
        std::string test;
    }
}


BENCHMARK(string, c_str_ctor, skyalib) {
    for(auto _ : state) {
        sk::string test = "test string test string test string test string";
    }
}
BENCHMARK(string, c_str_ctor, standard) {
    for(auto _ : state) {
        std::string test = "test string test string test string test string";
    }
}

BENCHMARK(string, copy_ctor, skyalib) {
    sk::string from = "test string test string test string test string";
    for(auto _ : state) {
        sk::string test = from;
    }
}
BENCHMARK(string, copy_ctor, standard) {
    std::string from = "test string test string test string test string";
    for(auto _ : state) {
        std::string test = from;
    }
}

BENCHMARK(string, move_ctor, skyalib) {
    sk::dynarray<sk::string> from(state.iterations());
    for(int i = 0; i < state.iterations(); ++i) from.emplace("test string test string test string test string");
    int i = -1;
    for(auto _ : state) {
        sk::string test = sk::move(from[++i]);
    }
}
BENCHMARK(string, move_ctor, standard) {
    sk::dynarray<std::string> from(state.iterations());
    for(int i = 0; i < state.iterations(); ++i) from.emplace("test string test string test string test string");
    int i = -1;
    for(auto _ : state) {
        std::string test = sk::move(from[++i]);
    }
}


BENCHMARK(string, c_str, skyalib) {
    sk::string test = "test string test string test string test string";
    for(auto _ : state) {
        const char* t = test.c_str();
    }
}
BENCHMARK(string, c_str, standard) {
    std::string test = "test string test string test string test string";
    for(auto _ : state) {
        const char* t = test.c_str();
    }
}


BENCHMARK(string, size, skyalib) {
    sk::string test = "test string test string test string test string";
    for(auto _ : state) {
        int t = test.size();
    }
}
BENCHMARK(string, size, standard) {
    std::string test = "test string test string test string test string";
    for(auto _ : state) {
        int t = test.size();
    }
}


BENCHMARK(string, equals, skyalib) {
    sk::string test1 = "test string test string test string test string 1";
    sk::string test2 = "test string test string test string test string 2";
    for(auto _ : state) {
        bool t = test1 == test2;
    }
}
BENCHMARK(string, equals, standard) {
    std::string test1 = "test string test string test string test string 1";
    std::string test2 = "test string test string test string test string 2";
    for(auto _ : state) {
        bool t = test1 == test2;
    }
}


BENCHMARK(string, compare, skyalib) {
    sk::string test = "test string test string test string test string";
    for(auto _ : state) {
        int t = test.compare("test string test string test string testx string");
    }
}
BENCHMARK(string, compare, standard) {
    std::string test = "test string test string test string test string";
    for(auto _ : state) {
        int t = test.compare("test string test string test string testx string");
    }
}


BENCHMARK(string, append, skyalib) {
    sk::string test1 = "test string test string test string test string 1";
    sk::string test2 = "test string test string test string test string 2";
    for(auto _ : state) {
        test1.append(test2);
    }
}
BENCHMARK(string, append, standard) {
    std::string test1 = "test string test string test string test string 1";
    std::string test2 = "test string test string test string test string 2";
    for(auto _ : state) {
        test1.append(test2);
    }
}