#include "gtest/gtest.h"

#include <sk/type_traits.h>


TEST(type_traits, is_same) {

    static_assert(sk::is_same<int>, "");
    static_assert(sk::is_same<int, int>, "");
    static_assert(sk::is_same<int, int, int>, "");
    static_assert(sk::is_same<int, int, int, int, int>, "");
    
    static_assert(!sk::is_same<bool, bool, int, int, int>, "");
    static_assert(!sk::is_same<int, int, int, int, bool>, "");
    static_assert(!sk::is_same<int, int, bool, int, int>, "");
}

TEST(type_traits, is_comparable) {
    struct is_comparable_test1 { bool operator==(int); };
    struct is_comparable_test2 { bool operator==(int&); };
    struct is_comparable_test3 { bool operator==(int&&); };
    struct is_comparable_test4 { bool operator==(const int&); };
    
    static_assert(sk::is_equality_comparable<is_comparable_test1, int> == true, "");
    static_assert(sk::is_equality_comparable<is_comparable_test1, int&> == true, "");
    static_assert(sk::is_equality_comparable<is_comparable_test1, int&&> == true, "");
    static_assert(sk::is_equality_comparable<is_comparable_test1, const int> == true, "");
    
    static_assert(sk::is_equality_comparable<is_comparable_test2, int> == false, "");
    static_assert(sk::is_equality_comparable<is_comparable_test2, int&> == true, "");
    static_assert(sk::is_equality_comparable<is_comparable_test2, int&&> == false, "");
    static_assert(sk::is_equality_comparable<is_comparable_test2, const int> == false, "");
    
    static_assert(sk::is_equality_comparable<is_comparable_test3, int> == true, "");
    static_assert(sk::is_equality_comparable<is_comparable_test3, int&> == false, "");
    static_assert(sk::is_equality_comparable<is_comparable_test3, int&&> == true, "");
    static_assert(sk::is_equality_comparable<is_comparable_test3, const int> == false, "");
    
    static_assert(sk::is_equality_comparable<is_comparable_test4, int> == true, "");
    static_assert(sk::is_equality_comparable<is_comparable_test4, int&> == true, "");
    static_assert(sk::is_equality_comparable<is_comparable_test4, int&&> == true, "");
    static_assert(sk::is_equality_comparable<is_comparable_test4, const int> == true, "");
    
    static_assert(sk::is_equality_comparable<int, int>, "");
    static_assert(sk::is_equality_comparable<int, char>, "");
    static_assert(sk::is_equality_comparable<int, bool>, "");
    static_assert(sk::is_equality_comparable<char, bool>, "");
    static_assert(sk::is_equality_comparable<int, const char*> == false, "");
}

