#include "gtest/gtest.h"

#include <sk/tree.h>


struct my_tree {
    int m_value;
    my_tree* m_left = nullptr;
    my_tree* m_right = nullptr;
    
    my_tree(int value = 0) : m_value(value) {}
    
    ~my_tree(){
        delete m_left;
        delete m_right;
    }
};

TEST(tree, insert) {
    my_tree* tt = nullptr;
    
    sk::tree::insert(tt, 5);
    sk::tree::insert(tt, 5);
    sk::tree::insert(tt, 2);
    sk::tree::insert(tt, 1);
    sk::tree::insert(tt, 3);
    sk::tree::insert(tt, 3);
    sk::tree::insert(tt, 10);
    sk::tree::insert(tt, 9);
    sk::tree::insert(tt, 7);
    sk::tree::insert(tt, 7);
    
    EXPECT_EQ(tt->m_value, 5);
        EXPECT_EQ(tt->m_right->m_value, 10);
            EXPECT_EQ(tt->m_right->m_right, nullptr);
            EXPECT_EQ(tt->m_right->m_left->m_value, 9);
                EXPECT_EQ(tt->m_right->m_left->m_right, nullptr);
                EXPECT_EQ(tt->m_right->m_left->m_left->m_value, 7);
                    EXPECT_EQ(tt->m_right->m_left->m_left->m_right, nullptr);
                    EXPECT_EQ(tt->m_right->m_left->m_left->m_left->m_value, 7);
                        EXPECT_EQ(tt->m_right->m_left->m_left->m_left->m_right, nullptr);
                        EXPECT_EQ(tt->m_right->m_left->m_left->m_left->m_left, nullptr);
        EXPECT_EQ(tt->m_left->m_value, 5);
            EXPECT_EQ(tt->m_left->m_left->m_value, 2);
                EXPECT_EQ(tt->m_left->m_left->m_right->m_value, 3);
                    EXPECT_EQ(tt->m_left->m_left->m_right->m_right, nullptr);
                    EXPECT_EQ(tt->m_left->m_left->m_right->m_left->m_value, 3);
                        EXPECT_EQ(tt->m_left->m_left->m_right->m_left->m_right, nullptr);
                        EXPECT_EQ(tt->m_left->m_left->m_right->m_left->m_left, nullptr);
                EXPECT_EQ(tt->m_left->m_left->m_left->m_value, 1);
                    EXPECT_EQ(tt->m_left->m_left->m_left->m_right, nullptr);
                    EXPECT_EQ(tt->m_left->m_left->m_left->m_left, nullptr);
    
    sk::tree::print(tt);
}

TEST(tree, remove) {
    my_tree* t1 = nullptr;
    
    EXPECT_EQ(sk::tree::remove(t1, 10), false);
    
    sk::tree::insert(t1, 10);
    EXPECT_NE(t1, nullptr);
    
    EXPECT_EQ(sk::tree::remove(t1, 10), true);
    EXPECT_EQ(t1, nullptr);
    
    sk::tree::insert(t1, 0);
    sk::tree::insert(t1, 10);
    sk::tree::insert(t1, 10);
    sk::tree::insert(t1, 10);
    sk::tree::remove(t1, 10);
    sk::tree::insert(t1, 9);
    sk::tree::insert(t1, 3);
    sk::tree::insert(t1, 8);
    sk::tree::insert(t1, 11);
    sk::tree::insert(t1, 10);
    sk::tree::insert(t1, 4);
    sk::tree::insert(t1, 5);
    EXPECT_EQ(sk::tree::remove(t1, 10), true);
    EXPECT_EQ(sk::tree::remove(t1, 10), true);
    EXPECT_EQ(sk::tree::remove(t1, 10), true);
    EXPECT_EQ(sk::tree::remove(t1, 10), false);
    sk::tree::remove(t1, 0);
    sk::tree::remove(t1, 8);
    sk::tree::remove(t1, 11);
    sk::tree::print(t1);
}

TEST(tree, find) {
    my_tree* t1 = nullptr;
}


