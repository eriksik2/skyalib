#include "gtest/gtest.h"

#include <sk/dynarray.h>
#include <sk/array_view.h>

TEST(array_view, def_ctor) {
    sk::array_view<int> arr;

    EXPECT_EQ(arr.size(), 0);
    EXPECT_EQ(arr.begin(), arr.end());
    EXPECT_EQ(arr.begin(), nullptr);
    EXPECT_EQ(bool(arr), false);
}
 
TEST(array_view, conversion) {
    sk::dynarray<int> a;
    a.push(1, 2, 1, 3);
    sk::array_view<int> arr;
    sk::array_view<const int> arr2 = arr;
    
    EXPECT_EQ(bool(arr), false);
    
    arr = a;
    arr2 = a;
    
    EXPECT_EQ(arr.size(), 4);
    EXPECT_NE(arr.begin(), arr.end());
    EXPECT_NE(arr.begin(), nullptr);
    EXPECT_EQ(arr[0], 1);
    EXPECT_EQ(arr.back(), 3);
    EXPECT_EQ(bool(arr), true);
    
    a = sk::dynarray<int>(arr);
    a = arr2;
}

TEST(array_view, generic_ctor) {
    sk::dynarray<int> a;
    a.push(1, 2, 1, 3);
    sk::array_view<int> arr(a);
    
    EXPECT_EQ(arr.size(), 4);
    EXPECT_NE(arr.begin(), arr.end());
    EXPECT_NE(arr.begin(), nullptr);
    EXPECT_EQ(arr[0], 1);
    EXPECT_EQ(arr.back(), 3);
    EXPECT_EQ(bool(arr), true);
}

TEST(array_view, begin_end_ctor) {
    int a[] = {2, 3, 1, 2, 3, 1, 2, 3, 1};
    sk::array_view<int> arr(a+1, a+4);
    
    EXPECT_EQ(arr.size(), 3);
    EXPECT_NE(arr.begin(), arr.end());
    EXPECT_NE(arr.begin(), nullptr);
    EXPECT_EQ(arr[0], 3);
    EXPECT_EQ(arr.back(), 2);
    EXPECT_EQ(bool(arr), true);
}

TEST(array_view, c_array_ctor) {
    int a[] = {2, 3, 1, 2, 3, 1, 2, 3, 1};
    sk::array_view<int> arr = a;
    
    EXPECT_EQ(arr.size(), 9);
    EXPECT_NE(arr.begin(), arr.end());
    EXPECT_NE(arr.begin(), nullptr);
    EXPECT_EQ(arr[0], 2);
    EXPECT_EQ(arr.back(), 1);
    EXPECT_EQ(bool(arr), true);
}

TEST(array_view, copy_assign) {
    int a[] = {2, 3, 1};
    int b[] = {6, 7, 8, 9, 10};
    
    sk::array_view<int> arr = a;
    EXPECT_EQ(arr.size(), 3);
    
    arr = b;
    EXPECT_EQ(arr.size(), 5);
}


void increment(sk::array_view<int> arr) {
    for(auto& i : arr) ++i;
}
TEST(array_view, function_call) {
    sk::dynarray<int> a;
    a.push(1, 2, 1, 3);
    int b[] = {6, 7, 8, 0, 10};
    
    increment(a);
    EXPECT_EQ(a[0], 2);
    EXPECT_EQ(a[1], 3);
    EXPECT_EQ(a[2], 2);
    EXPECT_EQ(a[3], 4);
    
    increment(b);
    EXPECT_EQ(b[0], 7);
    EXPECT_EQ(b[1], 8);
    EXPECT_EQ(b[2], 9);
    EXPECT_EQ(b[3], 1);
    EXPECT_EQ(b[4], 11);
}

