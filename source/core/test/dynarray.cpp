#include "gtest/gtest.h"

#include <vector>
#include <sk/array.h>
#include <sk/dynarray.h>


TEST(dynarray, def_ctor) {
    sk::dynarray<int> arr;

    EXPECT_EQ(arr.size(), 0);
    EXPECT_EQ(arr.capacity(), 0);
    EXPECT_EQ(arr.begin(), arr.end());
    EXPECT_EQ(arr.begin(), nullptr);
}

TEST(dynarray, list_ctor) {
    sk::dynarray<int> arr = {1, 3, 1, 2};
    
    EXPECT_EQ(arr.size(), 4);
    EXPECT_EQ(arr[1], 3);
}

TEST(dynarray, array_view_ctor) {
    sk::dynarray<int> a;
    a.push(5, 2, 2, 2);
    
    sk::array_view<int> b = a;
    
    sk::dynarray<int> arr(b);
    
    EXPECT_EQ(arr.size(), 4);
}

TEST(dynarray, reserving_ctor) {
    sk::dynarray<int> arr(10);
    
    EXPECT_EQ(arr.size(), 0);
    EXPECT_EQ(arr.capacity(), 10);
    EXPECT_EQ(arr.begin(), arr.end());
    EXPECT_NE(arr.begin(), nullptr);
}
