#include "gtest/gtest.h"

#include "sk/string.h"

#include <string.h>


#include <iostream>
template<class Tchar>
::std::ostream& operator<<(::std::ostream& os, const sk::string_template<Tchar>& lhs) {
    return os << lhs.pad('"', 1).c_str();
}

TEST(string, charpointer) {
    sk::string str = "aaabbb_cde";

    EXPECT_EQ(strlen(str.c_str()), str.length());
    EXPECT_EQ(strcmp(str.c_str(), "aaabbb_cde"), 0);

    sk::wstring wstr = L"wide: aaabbb_cde";

    EXPECT_EQ(wcslen(wstr.c_str()), wstr.length());
    EXPECT_EQ(wcscmp(wstr.c_str(), L"wide: aaabbb_cde"), 0);

    str = "changes...";

    EXPECT_EQ(strlen("changes..."), str.length());
    EXPECT_EQ(strcmp(str.c_str(), "changes..."), 0);

    const wchar_t* wcharptr = L"still wide... ";
    wstr = wcharptr;

    EXPECT_EQ(wcslen(wcharptr), wcslen(wstr.c_str()));
    EXPECT_EQ(wcscmp(wstr.c_str(), wcharptr), 0);
}

TEST(string, size) {
    sk::string str = "<zxc(/)%&0324h haksjd g78132";
    sk::wstring wstr = L"asdfjh3214g5 34 gyu:::_Zasdf";

    EXPECT_EQ(str.front(), '<');
    EXPECT_EQ(wstr.front(), 'a');

    EXPECT_EQ(str.back(), '2');
    EXPECT_EQ(wstr.back(), 'f');

    EXPECT_EQ(*str.begin(), '<');
    EXPECT_EQ(*wstr.begin(), 'a');

    EXPECT_EQ(*str.end(), '\0');
    EXPECT_EQ(*wstr.end(), '\0');
}

TEST(string, empty) {
    sk::string str = "";
    sk::wstring wstr = L"";

    EXPECT_EQ(str.front(), '\0');
    EXPECT_EQ(wstr.front(), '\0');

    EXPECT_EQ(str.back(), '\0');
    EXPECT_EQ(wstr.back(), '\0');

    EXPECT_EQ(*str.begin(), '\0');
    EXPECT_EQ(*wstr.begin(), '\0');

    EXPECT_EQ(*str.end(), '\0');
    EXPECT_EQ(*wstr.end(), '\0');

    EXPECT_EQ(str.length(), 0);
    EXPECT_EQ(wstr.length(), 0);
    EXPECT_EQ(str.size(), 0);
    EXPECT_EQ(wstr.size(), 0);

    EXPECT_EQ(str.empty(), true);
    EXPECT_EQ(wstr.empty(), true);

    str.append(".");
    wstr = L"zzz";

    EXPECT_EQ(str.empty(), false);
    EXPECT_EQ(wstr.empty(), false);

    EXPECT_EQ(str.length(), 1);
    EXPECT_EQ(wstr.length(), 3);
}

TEST(string, compare) {
    sk::string str1 = "around";
    sk::string str2 = "basic";
    sk::string str3 = "caller";
    sk::string str4 = "caller";
    const char* str5 = "depth";


    EXPECT_EQ(str1.compare(str1.c_str()), strcmp(str1.c_str(), str1.c_str()));
    EXPECT_EQ(str1.compare(str2.c_str()), strcmp(str1.c_str(), str2.c_str()));
    EXPECT_EQ(str1.compare(str3.c_str()), strcmp(str1.c_str(), str3.c_str()));
    EXPECT_EQ(str1.compare(str4.c_str()), strcmp(str1.c_str(), str4.c_str()));
    EXPECT_EQ(str1.compare(str5), strcmp(str1.c_str(), str5));

    EXPECT_EQ(str2.compare(str1.c_str()), strcmp(str2.c_str(), str1.c_str()));
    EXPECT_EQ(str2.compare(str2.c_str()), strcmp(str2.c_str(), str2.c_str()));
    EXPECT_EQ(str2.compare(str3.c_str()), strcmp(str2.c_str(), str3.c_str()));
    EXPECT_EQ(str2.compare(str4.c_str()), strcmp(str2.c_str(), str4.c_str()));
    EXPECT_EQ(str2.compare(str5), strcmp(str2.c_str(), str5));

    EXPECT_EQ(str3.compare(str1.c_str()), strcmp(str3.c_str(), str1.c_str()));
    EXPECT_EQ(str3.compare(str2.c_str()), strcmp(str3.c_str(), str2.c_str()));
    EXPECT_EQ(str3.compare(str3.c_str()), strcmp(str3.c_str(), str3.c_str()));
    EXPECT_EQ(str3.compare(str4.c_str()), strcmp(str3.c_str(), str4.c_str()));
    EXPECT_EQ(str3.compare(str5), strcmp(str3.c_str(), str5));

    EXPECT_EQ(str4.compare(str1.c_str()), strcmp(str4.c_str(), str1.c_str()));
    EXPECT_EQ(str4.compare(str2.c_str()), strcmp(str4.c_str(), str2.c_str()));
    EXPECT_EQ(str4.compare(str3.c_str()), strcmp(str4.c_str(), str3.c_str()));
    EXPECT_EQ(str4.compare(str4.c_str()), strcmp(str4.c_str(), str4.c_str()));
    EXPECT_EQ(str4.compare(str5), strcmp(str4.c_str(), str5));

    EXPECT_EQ(str4.equals(str3), true);
    EXPECT_EQ(str4.equals(str4), true);
    EXPECT_EQ(str3.equals(str3), true);
    EXPECT_EQ(str3.equals(str4), true);

    EXPECT_EQ(str1.equals(str3), false);
    EXPECT_EQ(str2.equals(str4), false);

    EXPECT_EQ(str1.equals("around"), true);
    EXPECT_EQ(str1.equals("around "), false);
}

TEST(string, casing) {
    sk::string str1 = "Erik Sik: <CASE>";

    str1.toupper();

    EXPECT_EQ(strcmp(str1.c_str(), "ERIK SIK: <CASE>"), 0);
    EXPECT_NE(strcmp(str1.c_str(), "erik sik: <case>"), 0);

    str1.tolower();

    EXPECT_EQ(strcmp(str1.c_str(), "erik sik: <case>"), 0);
    EXPECT_NE(strcmp(str1.c_str(), "ERIK SIK: <CASE>"), 0);

    str1.toupper();

    EXPECT_EQ(strcmp(str1.c_str(), "ERIK SIK: <CASE>"), 0);
    EXPECT_NE(strcmp(str1.c_str(), "erik sik: <case>"), 0);
}

TEST(string, pad_start) {
    sk::string str = "Erik Sik";

    int length_before = str.length();

    str.pad_start('_', 3);

    EXPECT_EQ(str.length(), length_before + 3);

    str.pad_start('k', 1);

    EXPECT_EQ(str.equals("k___Erik Sik"), true);
}

TEST(string, const_pad_start) {
    const sk::string str1 = "Erik Sik";

    int length_before = str1.length();

    sk::string str2 = str1.pad_start('z', 5);

    EXPECT_EQ(str1.length(), length_before);
    EXPECT_EQ(str2.length(), length_before + 5);

    str2.pad_start(' ', 2).pad_start('|', 3);

    EXPECT_EQ(strcmp(str1.c_str(), "Erik Sik"), 0);
    EXPECT_EQ(strcmp(str2.c_str(), "|||  zzzzzErik Sik"), 0);
}

TEST(string, pad_end) {
    sk::string str1 = "aAlan";

    int length_before = str1.length();

    str1.pad_end(')', 299);

    EXPECT_EQ(str1.length(), length_before + 299);
    EXPECT_EQ(str1[200], ')');
}

TEST(string, pad) {
    sk::string str1 = "aAlan";

    int length_before = str1.length();

    str1.pad('|', 2);

    EXPECT_EQ(str1.length(), length_before + 2*2);
    EXPECT_EQ(str1.equals("||aAlan||"), true);
}

TEST(string, trim_start) {
    const sk::string str = " \n  aAlan ";

    EXPECT_EQ(str.trim_start(), "aAlan ");
    EXPECT_EQ(str.trim_start("aAl "), "\n  aAlan ");
    EXPECT_EQ(str.trim_start(" \n"), "aAlan ");
    EXPECT_EQ(str.trim_start(" \na"), "Alan ");
    EXPECT_EQ(str.trim_start("\na"), " \n  aAlan ");

    EXPECT_EQ(sk::string(".|").trim_start("."), "|");
    EXPECT_EQ(sk::string("|.").trim_start("."), "|.");
    EXPECT_EQ(sk::string("..").trim_start("."), "");
    EXPECT_EQ(sk::string(".").trim_start("."), "");
    EXPECT_EQ(sk::string("").trim_start(), "");
    EXPECT_EQ(sk::string("   ").trim_start(), "");
}

TEST(string, trim_end) {
    const sk::string str = " \n  aAlan ";

    EXPECT_EQ(str.trim_end(), " \n  aAlan");
    EXPECT_EQ(str.trim_end("n"), " \n  aAlan ");
    EXPECT_EQ(str.trim_end("n "), " \n  aAla");
    EXPECT_EQ(str.trim_end("n la"), " \n  aA");
    EXPECT_EQ(str.trim_end("nla"), " \n  aAlan ");
    EXPECT_EQ(str.trim_end(" nlaA"), " \n");
    EXPECT_EQ(str.trim_end(" nlaA\n"), "");

    EXPECT_EQ(sk::string(".|").trim_end("."), ".|");
    EXPECT_EQ(sk::string("|.").trim_end("."), "|");
    EXPECT_EQ(sk::string("..").trim_end("."), "");
    EXPECT_EQ(sk::string(".").trim_end("."), "");
    EXPECT_EQ(sk::string("").trim_end(), "");
    EXPECT_EQ(sk::string("   ").trim_end(), "");
}

TEST(string, trim) {
    const sk::string str = " \n  aAlan ";

    EXPECT_EQ(str.trim(), "aAlan");
    EXPECT_EQ(str.trim(" "), "\n  aAlan");
    EXPECT_EQ(str.trim(" \n"), "aAlan");
    EXPECT_EQ(str.trim(" na"), "\n  aAl");
    EXPECT_EQ(str.trim(" naAl"), "\n");

    EXPECT_EQ(sk::string(".|").trim("|"), ".");
    EXPECT_EQ(sk::string("|.").trim("|"), ".");
    EXPECT_EQ(sk::string(".|").trim("."), "|");
    EXPECT_EQ(sk::string("|.").trim("."), "|");
    EXPECT_EQ(sk::string("..").trim("."), "");
    EXPECT_EQ(sk::string(".").trim("."), "");
    EXPECT_EQ(sk::string("").trim(), "");
    EXPECT_EQ(sk::string("   ").trim(), "");
}

TEST(string, remove) {
    const sk::string str = "(Apples, Bananas, Cars)";

    EXPECT_EQ(str.remove('a'), "(Apples, Bnns, Crs)");
    EXPECT_EQ(str.remove(' '), "(Apples,Bananas,Cars)");
    EXPECT_EQ(str.remove(','), "(Apples Bananas Cars)");
    EXPECT_EQ(str.remove('('), "Apples, Bananas, Cars)");
    EXPECT_EQ(str.remove(')'), "(Apples, Bananas, Cars");
    EXPECT_EQ(str.remove(')').remove('('), "Apples, Bananas, Cars");
    EXPECT_EQ(str.remove('|'), "(Apples, Bananas, Cars)");

    EXPECT_EQ(str.remove("a"), "(Apples, Bnns, Crs)");
    EXPECT_EQ(str.remove(", "), "(ApplesBananasCars)");
    EXPECT_EQ(str.remove(", Bananas"), "(Apples, Cars)");
    EXPECT_EQ(str.remove("(App"), "les, Bananas, Cars)");
    EXPECT_EQ(str.remove("s, Cars)"), "(Apples, Banana");
    EXPECT_EQ(str.remove("s, Cars)") + ")", "(Apples, Banana)");

    EXPECT_EQ(sk::string("Erik Sik").remove("ik"), "Er S");
    EXPECT_EQ(sk::string("Bananas, another aannan").remove("an"), "Bas, other an");

    sk::string rem = "an";
    char* chrs = rem.begin();

    EXPECT_EQ(sk::string("anvan").remove(chrs), "v");
    EXPECT_EQ(sk::string("zanvan|").remove(chrs), "zv|");
    EXPECT_EQ(sk::string("anan").remove(chrs), "");
    EXPECT_EQ(sk::string("an").remove(chrs), "");
}

TEST(string, replace) {
    const sk::string str = "(Apples, Bananas, Cars)";

    EXPECT_EQ(str.replace('a', ' '), "(Apples, B n n s, C rs)");
    EXPECT_EQ(str.replace(' ', 'z'), "(Apples,zBananas,zCars)");
    EXPECT_EQ(str.replace(',', '|'), "(Apples| Bananas| Cars)");
    EXPECT_EQ(str.replace(')', 'E').replace('(', 'B'), "BApples, Bananas, CarsE");
    EXPECT_EQ(str.replace('|', 'z'), "(Apples, Bananas, Cars)");

    EXPECT_EQ(str.replace(", ", "|"), "(Apples|Bananas|Cars)");
    EXPECT_EQ(str.replace("Bananas", "Oranges"), "(Apples, Oranges, Cars)");
    EXPECT_EQ(str.replace("Cars", "Arguments"), "(Apples, Bananas, Arguments)");
    EXPECT_EQ(str.replace(", ", "Arguments"), "(ApplesArgumentsBananasArgumentsCars)");
    EXPECT_EQ(str.replace("(Apples, Bananas, Cars)", ""), "");
    EXPECT_EQ(str.replace("(Apples, Bananas, Cars)", "."), ".");
    EXPECT_EQ(str.replace("(Apples, Bananas, Cars)", "(Apples, Bananas, Cars) "), "(Apples, Bananas, Cars) ");
    EXPECT_EQ(str.replace("Apples, Bananas, Cars)", "(Apples, Bananas, Cars) "), "((Apples, Bananas, Cars) ");
}

TEST(string, append_prepend) {
    const sk::string str = "Erik";

    EXPECT_EQ(str.append(" Sik"), "Erik Sik");
    EXPECT_EQ(str.append(""), "Erik");
    EXPECT_EQ(str.append(str), "ErikErik");
    EXPECT_EQ(str.append(str.append("||")), "ErikErik||");
    EXPECT_EQ(str.append(str).append("||"), "ErikErik||");

    EXPECT_EQ(str.prepend(" Sik"), " SikErik");
    EXPECT_EQ(str.prepend(""), "Erik");
    EXPECT_EQ(str.prepend(str), "ErikErik");
    EXPECT_EQ(str.prepend(str.prepend("||")), "||ErikErik");
    EXPECT_EQ(str.prepend(str).prepend("||"), "||ErikErik");

    EXPECT_EQ(str.prepend(str.append("||")), "Erik||Erik");
    EXPECT_EQ(str.prepend("zZz").append("||"), "zZzErik||");

    sk::string str2 = "";

    EXPECT_EQ(str2.length(), 0);
    EXPECT_EQ(str2.append("x"), "x");
    EXPECT_EQ(str2.length(), 1);
    EXPECT_EQ(str2, "x");
}

TEST(string, operators) {

    sk::string str = "Me";

    EXPECT_EQ("["+str+"]", "[Me]");
    EXPECT_EQ(str+" "+"me"+" you", "Me me you");

    EXPECT_NE("["+str+"]", "[Me] ");
    EXPECT_NE(str+" "+"me"+" you", "MeD me you");

    str += "d";

    EXPECT_EQ(("["+str+"]") + str, "[Med]Med");
    EXPECT_EQ(str+" "+"me"+" you", "Med me you");

    EXPECT_NE("["+str+"]", "[Me]");
    EXPECT_NE(str+" "+"me"+" you", "Me me you");
}

TEST(string, split) {

    const sk::string str1 = "f Lorem Ipsum        is simply  the     printing  . Lorem    I    has been the    .";
    const sk::string str2 = "     Lorem Ip\n \nsum    \n\n\n\n    is sim\np  the  \n   pri\n\ng and . Lem    I    h he  .   ";

    EXPECT_EQ(str1.split().size(), 14);
    EXPECT_EQ(str2.split().size(), 16);
    EXPECT_EQ(sk::string("").split().size(), 0);
    EXPECT_EQ(sk::string(" ").split().size(), 0);
    EXPECT_EQ(sk::string("        ").split().size(), 0);
    EXPECT_EQ(sk::string("\n      \n  ").split().size(), 0);
    EXPECT_EQ(sk::string("f  xx").split().size(), 2);
    EXPECT_EQ(sk::string("f  x").split().size(), 2);
    EXPECT_EQ(sk::string("ff  xx").split().size(), 2);
    EXPECT_EQ(sk::string(" f  x").split().size(), 2);
    EXPECT_EQ(sk::string("f  x ").split().size(), 2);
    EXPECT_EQ(sk::string("f  xf ").split().size(), 2);
    EXPECT_EQ(sk::string(" xf \n x").split().size(), 2);
    EXPECT_EQ(sk::string(" \nxf \n x").split().size(), 2);
    EXPECT_EQ(sk::string(" \nxf \n x\n").split().size(), 2);
    EXPECT_EQ(sk::string(" \nxf \n \nx\n").split().size(), 2);
    EXPECT_EQ(sk::string("\n \nxf \n \nx\n").split().size(), 2);
}
