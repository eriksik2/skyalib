#include "sk/ini/parser.h"
#include "sk/ini/loader.h"

#include "stdarg.h"
#include "string.h"
#include "stdlib.h"

int __ini_parser::fpeekc(FILE* file) {
    int c = fgetc(file);
    ungetc(c, file);
    if(c == '\0') return EOF;
    return c;
}

thread_local int parser_line;
thread_local sk::ini* root_ini;
thread_local int assign;
thread_local int scope;

void __ini_parser::error(FILE* file, PARSER_ERR code, const char* user_msg) {
    const char* error_msg = "Unknown error";
    switch(code)
    {
        case UNEXPECTED_SYMBOL:   error_msg = "Unexpected symbol"; break;
        default: break;
    }
    printf("Error while parsing ini file: %i: ", parser_line);
    printf("%s: %s (C%i)\n", error_msg, user_msg, (int)code);
    parse_string(file, 0);
}


void __ini_parser::parse(FILE* file, sk::ini* iniobj) {
    if(!file || !iniobj) return;
    parser_line = 1;
    root_ini = iniobj;
    assign = 0;
    scope = 0;
    char c;
    while((c = fpeekc(file)) != EOF)
    {
        if(c == symbol[DELIMITER])
        {
            fgetc(file);
            ++parser_line;
            continue;
        }
        whitespace(file);
        if(parse_scope(file, iniobj)) continue;
        if(parse_assignment(file, iniobj)) continue;
        if(parse_comment(file)) continue;
        fgetc(file);
    }
}

bool __ini_parser::parse_comment(FILE* file, sk::string* buf) {
    int peek = fpeekc(file);
    if(peek != symbol[COMMENT1] &&
        peek != symbol[COMMENT2]) return false;
    fgetc(file);
    char c;
    while((c = fpeekc(file)) != EOF)
    {
        if(c == symbol[DELIMITER])
        {
            fgetc(file);
            return true;
        }
        char ch[2] = { 0, 0 };
        ch[0] = fgetc(file);
        if(buf) buf->append(ch);
    }
    return true;
}
void __ini_parser::whitespace(FILE* file, sk::string* buf) {
    char c;
    while((c = fpeekc(file)) != EOF)
    {
        if(c == ' ' || c == '\t')
        {
            char ch[2] = { 0, 0 };
            ch[0] = fgetc(file);
            if(buf) buf->append(ch);
            continue;
        }
        return;
    }
}

bool __ini_parser::parse_ident(FILE* file, sk::string* name) {
    bool entry = true;
    char c;
    while((c = fpeekc(file)) != EOF)
    {
        if(c==symbol[SECTION_OPEN] ||
            c==symbol[SECTION_CLOSE] ||
            c==symbol[ASSIGNMENT1] ||
            c==symbol[ASSIGNMENT2] ||
            c==symbol[DELIMITER] ||
            c==symbol[COMMENT1] ||
            c==symbol[COMMENT2] ||
            c==symbol[SCOPE1] ||
            c==symbol[SCOPE2] ||
            c==' '||
            c=='\t')
        {
            return !entry;
        }
        entry = false;
        char ch[2] = { 0, 0 };
        ch[0] = c;
        if(name) name->append(ch);
        fgetc(file);
    }
    return !entry;
}

bool __ini_parser::parse_string(FILE* file, sk::string* string) {
    bool entry = true;
    char c;
    while((c = fpeekc(file)) != EOF)
    {
        if(c==symbol[DELIMITER]) return !entry;
        entry = false;
        char ch[2] = { 0, 0 };
        ch[0] = c;
        if(string) string->append(ch);
        fgetc(file);
    }
    return !entry;
}

bool __ini_parser::parse_scope_token(FILE* file) {
    int c = fpeekc(file);
    switch(c)
    {
        case symbol[SCOPE1]:
        case symbol[SCOPE2]:
            if(!scope) scope = c;
            if(c == scope)
            {
                fgetc(file);
                return true;
            }
            error(file, UNEXPECTED_SYMBOL, "Cannot use '.' and '/' in the same file");
        default: return false;
    }
}
bool __ini_parser::parse_assign_token(FILE* file) {
    int c = fpeekc(file);
    switch(c)
    {
        case symbol[ASSIGNMENT1]:
        case symbol[ASSIGNMENT2]:
            if(!assign) assign = c;
            if(c == assign)
            {
                fgetc(file);
                return true;
            }
            error(file, UNEXPECTED_SYMBOL, "Cannot use '=' and ':' in the same file");
        default: return false;
    }
}

bool __ini_parser::parse_selector(FILE* file, sk::ini*& iniobj) {
    sk::string name;
    bool entry = true;
    bool scope_last = false;
    while(parse_ident(file, &name))
    {
        scope_last = false;
        iniobj = &(*iniobj)[name.c_str()];
        name = "";
        whitespace(file);
        scope_last = parse_scope_token(file);
        entry = false;
    }
    if(scope_last)
    {
        error(file, UNEXPECTED_SYMBOL, "Expected an identifier after scope token");
    }
    return !entry;
}

bool __ini_parser::parse_scope(FILE* file, sk::ini*& iniobj) {
    if(fpeekc(file) != symbol[SECTION_OPEN]) return false;
    iniobj = root_ini; // scope always relative to root
    fgetc(file);
    whitespace(file);
    if(!parse_selector(file, iniobj))
    {
        error(file, UNEXPECTED_SYMBOL, "Could be that the scope selector is empty");
        return true;
    }
    whitespace(file);
    if(fpeekc(file) != symbol[SECTION_CLOSE])
    {
        error(file, UNEXPECTED_SYMBOL, "Expected ']'");
        return true;
    }
    fgetc(file);
    return true;
}

bool __ini_parser::parse_assignment(FILE* file, sk::ini* iniobj) {
    if(!parse_selector(file, iniobj)) return false;

    whitespace(file);
    if(!parse_assign_token(file))
    {
        error(file, UNEXPECTED_SYMBOL, "Expected '=' or ':'");
        return true;
    }
    whitespace(file);
    sk::string value;
    if(!parse_string(file, &value))
    {
        //warning(no value in assignment, ini);
    }
    (*iniobj) = value.c_str();
    return true;
}
