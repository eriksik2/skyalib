#include "sk/ini/loader.h"
#include "sk/ini/parser.h"

#include <stdio.h>

using namespace sk;

ini ini::read(const char * path) {
    ini ret{sk::string{path}.split("/\\").back().split(".").front().c_str()};
    
    FILE* file = fopen(path, "r");
    if(!file) return ret;
    
    __ini_parser::parse(file, &ret);
    
    if(fclose(file) != 0){
        //error
    }
    return ret;
}

void ini::write(const ini& inifile, const char * path) {
    // if path is a dir, use "path/{inifile.name()}.ini"
}

ini::ini(const char * name)
    : m_name(name) {}

ini::~ini() {
    if(m_children) delete m_children;
}

ini::ini(const ini & f)
    : m_name(f.m_name)
    , m_value(f.m_value) {
    if(!m_children) m_children = new sk::dynarray<ini>();
    *m_children = *f.m_children;
}

ini::ini(ini && f)
    : m_name(static_cast<sk::string&&>(f.m_name))
    , m_value(static_cast<sk::string&&>(f.m_value)) {
    auto* tmp = m_children;
    m_children = f.m_children;
    f.m_children = tmp;
}

ini & ini::operator=(const ini & f) {
    m_name = f.m_name;
    m_value = f.m_value;
    if(!m_children) m_children = new sk::dynarray<ini>();
    *m_children = *f.m_children;
    return *this;
}

ini & ini::operator=(ini && f) {
    m_name = static_cast<sk::string&&>(f.m_name);
    m_value = static_cast<sk::string&&>(f.m_value);
    auto* tmp = m_children;
    m_children = f.m_children;
    f.m_children = tmp;
    return *this;
}

ini& ini::operator=(const char * value) {
    this->m_value = value;
    return *this;
}

ini& ini::operator[](const char* n) {
    const sk::string index = n;
    const auto names = index.split(".");
    if(!names.size()) return *this;

    auto* next = this;
    for(auto&& name : names)
    {
        auto*& children = next->m_children;
        if(!children) children = new sk::dynarray<ini>();
        next = nullptr;
        for(auto&& child : *children)
            if(child.name() == name)
            {
                next = &child;
                break;
            }
        if(!next)
        {
            children->emplace(name.c_str());
            next = &children->back();
        }
    }
    return *next;
}

bool ini::has_value() const {
    return m_value.length() > 0;
}
bool ini::has_children() const {
    return m_children && m_children->size() > 0;
}
const sk::string & ini::name() const {
    return m_name;
}
ini::operator bool() const {
    return has_value() || has_children();
}
