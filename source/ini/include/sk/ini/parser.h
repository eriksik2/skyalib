#pragma once

#include "sk/string.h"

#include "stdio.h"

namespace sk {
    struct ini;
}

namespace __ini_parser {
    constexpr const int symbol[9] = {
        ';',
        '#',
        '[',
        ']',
        '=',
        ':',
        '\n',
        '.',
        '/'
    };
    enum SYMBOL {
        COMMENT1 = 0,
        COMMENT2 = 1,
        SECTION_OPEN = 2,
        SECTION_CLOSE = 3,
        ASSIGNMENT1 = 4,
        ASSIGNMENT2 = 5,
        DELIMITER = 6,
        SCOPE1 = 7,
        SCOPE2 = 8
    };
    enum PARSER_ERR {
        UNEXPECTED_SYMBOL
    };

    int fpeekc(FILE* file);

    void error(FILE* file, PARSER_ERR, const char* msg);

    void parse(FILE* file, sk::ini* iniobj);

    void whitespace(FILE* file, sk::string* buf = 0);
    bool parse_comment(FILE* file, sk::string* buf = 0);
    bool parse_ident(FILE* file, sk::string* name);
    bool parse_string(FILE* file, sk::string* string);

    bool parse_scope_token(FILE* file);
    bool parse_assign_token(FILE* file);

    bool parse_selector(FILE* file, sk::ini*& iniobj);
    bool parse_scope(FILE* file, sk::ini*& iniobj);
    bool parse_assignment(FILE* file, sk::ini* iniobj);
}
