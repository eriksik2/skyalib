#pragma once

#include "sk/string.h"
#include "sk/dynarray.h"

#include <stdlib.h>

namespace sk {
    struct ini {

        static ini read(const char* path);
        static void write(const ini& file, const char* path);

        explicit ini(const char* name);
        ~ini();

        ini(const ini&);
        ini(ini&&);
        ini& operator=(const ini&);
        ini& operator=(ini&&);
        
        ini& operator=(const char* value);
        ini& operator[](const char* name);

        template<class Tv = const sk::string&>
        Tv value() const;
        
        // assign my value as Tv to assignee
        template<class Tv, class Tas>
        bool assign_value(Tas& assignee) const;
        
        template<class Tv>
        bool assign_value(Tv& assignee) const;
        
        bool has_value() const;
        bool has_children() const;
        const sk::string& name() const;
        
        // has_value || has_children
        operator bool() const;
    private:
        sk::string m_name;
        sk::string m_value;
        sk::dynarray<ini>* m_children = nullptr;
    };
}

template<class Tv, class Tas>
bool sk::ini::assign_value(Tas& assignee) const {
    if(!has_value()) return false;
    assignee = value<Tv>();
    return true;
}
template<class Tv>
bool sk::ini::assign_value(Tv& assignee) const {
    if(!has_value()) return false;
    assignee = value<Tv>();
    return true;
}


template<>
inline const char* sk::ini::value<const char*>() const {
    return m_value.c_str();
}
template<>
inline sk::string sk::ini::value<sk::string>() const {
    return m_value;
}
template<>
inline const sk::string& sk::ini::value<const sk::string&>() const {
    return m_value;
}

template<>
inline int sk::ini::value<int>() const {
    return int(strtol(m_value.c_str(), 0, 10));
}

template<>
inline long sk::ini::value<long>() const {
    return strtol(m_value.c_str(), 0, 10);
}

template<>
inline long long sk::ini::value<long long>() const {
    return strtoll(m_value.c_str(), 0, 10);
}

template<>
inline float sk::ini::value<float>() const {
    return strtof(m_value.c_str(), 0);
}

template<>
inline double sk::ini::value<double>() const {
    return strtod(m_value.c_str(), 0);
}

template<>
inline long double sk::ini::value<long double>() const {
    return strtold(m_value.c_str(), 0);
}

template<>
inline bool sk::ini::value<bool>() const {
    auto str = m_value.trim().tolower();
    if(str == "true"
    || str == "yes"
    || str == "on") return true;
    if(str == "false"
    || str == "no"
    || str == "off") return false;
    return bool(strtol(m_value.c_str(), 0, 10));
}

template<>
inline char sk::ini::value<char>() const {
    return m_value.trim_start()[0];
}
