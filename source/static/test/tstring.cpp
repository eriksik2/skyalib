#include "gtest/gtest.h"

#include <sk/type_traits/is_same.h>
#include <sk/static/tstring.h>

#include <stdio.h>


TEST(tstring, is_same) {
    using test_string1 = TSTRING("test tstring");
    using test_string2 = TSTRING("test tstring");
    
    using test_string3 = TSTRING("test  tstring");
    using test_string4 = TSTRING("test tstrinf");
    using test_string5 = TSTRING("xest tstring");
    using test_string6 = TSTRING("xest tstring");
    
    static_assert(sk::is_same<test_string1, test_string2> == true, "");
    static_assert(sk::is_same<test_string3, test_string3> == true, "");
    static_assert(sk::is_same<test_string1, test_string3> == false, "");
    static_assert(sk::is_same<test_string1, test_string4> == false, "");
    static_assert(sk::is_same<test_string1, test_string5> == false, "");
    static_assert(sk::is_same<test_string4, test_string5> == false, "");
    static_assert(sk::is_same<test_string5, test_string6> == true, "");
}
