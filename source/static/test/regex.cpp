#include "gtest/gtest.h"

#include <sk/static.h>


TEST(regex, groups) {
    
    using reg1 = sk_regex(^eri(k)$);
    
    static_assert(reg1::is_match("erik") == true, "");
    static_assert(reg1::is_match("eric") == false, "");
    static_assert(reg1::is_match("eri") == false, "");
    static_assert(reg1::is_match("erik ") == false, "");
    static_assert(reg1::is_match("erikk") == false, "");
    static_assert(reg1::is_match("k") == false, "");
    
    using reg2 = sk_regex(^(eri)k$);
    
    static_assert(reg2::is_match("erik") == true, "");
    static_assert(reg2::is_match("eric") == false, "");
    static_assert(reg2::is_match("eri") == false, "");
    static_assert(reg2::is_match("erik ") == false, "");
    static_assert(reg2::is_match("erikk") == false, "");
    static_assert(reg2::is_match("k") == false, "");
    
    using reg3 = sk_regex(^(e)(r)(i)(k)$);
    
    static_assert(reg3::is_match("erik") == true, "");
    static_assert(reg3::is_match("eric") == false, "");
    static_assert(reg3::is_match("eri") == false, "");
    static_assert(reg3::is_match("erik ") == false, "");
    static_assert(reg3::is_match("erikk") == false, "");
    static_assert(reg3::is_match("k") == false, "");
    
    using reg4 = sk_regex(^(((e)r)()((()))(i))(()(k))$);
    
    static_assert(reg4::is_match("erik") == true, "");
    static_assert(reg4::is_match("eric") == false, "");
    static_assert(reg4::is_match("eri") == false, "");
    static_assert(reg4::is_match("erik ") == false, "");
    static_assert(reg4::is_match("erikk") == false, "");
    static_assert(reg4::is_match("k") == false, "");
    
    static_assert(sk::is_same<reg1, reg2, reg3, reg4>, "");
}

TEST(regex, quantifiers) {
    
    using reg1 = sk_regex(^e+$);
    
    static_assert(reg1::is_match("") == false, "");
    static_assert(reg1::is_match("x") == false, "");
    static_assert(reg1::is_match(" ") == false, "");
    static_assert(reg1::is_match(" e") == false, "");
    static_assert(reg1::is_match("e ") == false, "");
    static_assert(reg1::is_match("eeeeeeeeeeeeeeeee eeeeee") == false, "");
    static_assert(reg1::is_match("e") == true, "");
    static_assert(reg1::is_match("ee") == true, "");
    static_assert(reg1::is_match("eeeeeeeeeeeeeeeeeeeeeee") == true, "");
    
    using reg2 = sk_regex(^e*$);
    
    static_assert(reg2::is_match("") == true, "");
    static_assert(reg2::is_match("x") == false, "");
    static_assert(reg2::is_match(" ") == false, "");
    static_assert(reg2::is_match(" e") == false, "");
    static_assert(reg2::is_match("e ") == false, "");
    static_assert(reg2::is_match("eeeeeeeeeeeeeeeee eeeeee") == false, "");
    static_assert(reg2::is_match("e") == true, "");
    static_assert(reg2::is_match("ee") == true, "");
    static_assert(reg2::is_match("eeeeeeeeeeeeeeeeeeeeeee") == true, "");
    
    using reg3 = sk_regex(^e?$);
    
    static_assert(reg3::is_match("") == true, "");
    static_assert(reg3::is_match("x") == false, "");
    static_assert(reg3::is_match(" ") == false, "");
    static_assert(reg3::is_match(" e") == false, "");
    static_assert(reg3::is_match("e ") == false, "");
    static_assert(reg3::is_match("e") == true, "");
    static_assert(reg3::is_match("ee") == false, "");
    
    using reg4 = sk_regex(^xe?$);
    
    static_assert(reg4::is_match("x") == true, "");
    static_assert(reg4::is_match("xe") == true, "");
    static_assert(reg4::is_match("e") == false, "");
    static_assert(reg4::is_match("xx") == false, "");
    static_assert(reg4::is_match("ee") == false, "");
    static_assert(reg4::is_match("") == false, "");
    
    using reg5 = sk_regex(^(xe)?$);
    
    static_assert(reg5::is_match("") == true, "");
    static_assert(reg5::is_match("xe") == true, "");
    static_assert(reg5::is_match("x") == false, "");
    static_assert(reg5::is_match("e") == false, "");
    static_assert(reg5::is_match("xx") == false, "");
    static_assert(reg5::is_match("ee") == false, "");
}

TEST(regex, or_) {
    
    using reg1 = sk_regex(^a|b$);
    
    static_assert(reg1::is_match("") == false, "");
    static_assert(reg1::is_match("x") == false, "");
    static_assert(reg1::is_match(" a") == false, "");
    static_assert(reg1::is_match("ab") == false, "");
    static_assert(reg1::is_match("a") == true, "");
    static_assert(reg1::is_match("b") == true, "");
    static_assert(reg1::is_match("aa") == false, "");
    static_assert(reg1::is_match("bb") == false, "");
    
    using reg2 = sk_regex(^ea|b$);
    
    static_assert(reg2::is_match("") == false, "");
    static_assert(reg2::is_match("x") == false, "");
    static_assert(reg2::is_match(" a") == false, "");
    static_assert(reg2::is_match("eab") == false, "");
    static_assert(reg2::is_match("ea") == true, "");
    static_assert(reg2::is_match("b") == true, "");
    static_assert(reg2::is_match("eb") == false, "");
    static_assert(reg2::is_match("aa") == false, "");
    
    using reg3 = sk_regex(^ea|b+$);
    
    static_assert(reg3::is_match("") == false, "");
    static_assert(reg3::is_match("x") == false, "");
    static_assert(reg3::is_match(" a") == false, "");
    static_assert(reg3::is_match("eab") == false, "");
    static_assert(reg3::is_match("ea") == true, "");
    static_assert(reg3::is_match("b") == true, "");
    static_assert(reg3::is_match("bbbbb") == true, "");
    static_assert(reg3::is_match("aaaaa") == false, "");
    static_assert(reg3::is_match("eaeaea") == false, "");
    static_assert(reg3::is_match("eb") == false, "");
    static_assert(reg3::is_match("aa") == false, "");
}

TEST(regex, char_group) {
    using reg1 = sk_regex(^[]$);
    
    static_assert(reg1::is_match("") == true, "");
    static_assert(reg1::is_match(" ") == false, "");
    static_assert(reg1::is_match("  ") == false, "");
    static_assert(reg1::is_match("x") == false, "");
    
    using reg2 = sk_regex(^[a]$);
    
    static_assert(reg2::is_match("a") == true, "");
    static_assert(reg2::is_match("b") == false, "");
    static_assert(reg2::is_match("") == false, "");
    static_assert(reg2::is_match("ab") == false, "");
    static_assert(reg2::is_match("a ") == false, "");
    static_assert(reg2::is_match("aa") == false, "");
    
    using reg3 = sk_regex(^[ab]$);
    
    static_assert(reg3::is_match("a") == true, "");
    static_assert(reg3::is_match("b") == true, "");
    static_assert(reg3::is_match("") == false, "");
    static_assert(reg3::is_match("x") == false, "");
    static_assert(reg3::is_match("ab") == false, "");
    static_assert(reg3::is_match("a ") == false, "");
    static_assert(reg3::is_match("aa") == false, "");
    
    using reg4 = sk_regex(^[ab]+$);
    
    static_assert(reg4::is_match("a") == true, "");
    static_assert(reg4::is_match("b") == true, "");
    static_assert(reg4::is_match("aa") == true, "");
    static_assert(reg4::is_match("ab") == true, "");
    static_assert(reg4::is_match("baabbabbab") == true, "");
    static_assert(reg4::is_match("baabbabxbab") == false, "");
    static_assert(reg4::is_match("") == false, "");
    static_assert(reg4::is_match("x") == false, "");
    static_assert(reg4::is_match("a ") == false, "");
    
    using reg5 = sk_regex(^[^ab]c$);
    
    static_assert(reg5::is_match("xc") == true, "");
    static_assert(reg5::is_match(" c") == true, "");
    static_assert(reg5::is_match("ac") == false, "");
    static_assert(reg5::is_match("c") == false, "");
    static_assert(reg5::is_match("b") == false, "");
    static_assert(reg5::is_match("") == false, "");
    static_assert(reg5::is_match("ab") == false, "");
    static_assert(reg5::is_match("abc") == false, "");
    static_assert(reg5::is_match("a ") == false, "");
    static_assert(reg5::is_match("aa") == false, "");
    
    using reg6 = sk_regex(^[0-3x]$);
    
    static_assert(reg6::apply<TSTRING("0")>::is_match == true, "");
    static_assert(reg6::is_match("1") == true, "");
    static_assert(reg6::is_match("2") == true, "");
    static_assert(reg6::is_match("3") == true, "");
    static_assert(reg6::is_match("x") == true, "");
    static_assert(reg6::is_match("4") == false, "");
    static_assert(reg6::is_match("01") == false, "");
    
    using reg7 = sk_regex(^[^0-3]$);
    
    static_assert(reg7::is_match("0") == false, "");
    static_assert(reg7::is_match("1") == false, "");
    static_assert(reg7::is_match("2") == false, "");
    static_assert(reg7::is_match("3") == false, "");
    static_assert(reg7::is_match("4") == true, "");
    static_assert(reg7::is_match("01") == false, "");
}
                 
TEST(regex, tstring) {
    using str1 = TSTRING("erik sik");
    using reg1 = sk_regex([a-zA-Z]+);
    
    static_assert(reg1::apply<str1>::is_match == true, "");
    static_assert(reg1::apply<str1>::length == 4, "");
    static_assert(sk::is_same<reg1::apply<str1>::match, TSTRING("erik")>, "");
    static_assert(sk::is_same<reg1::apply<str1>::next, TSTRING(" sik")>, "");
}

TEST(regex, other) {
    using FLOAT = sk_regex(^([1-9][0-9]*|0*).[0-9]+$);
    
    static_assert(FLOAT::is_match("06.0") == false, "");
    static_assert(FLOAT::is_match("02") == false, "");
    static_assert(FLOAT::is_match("4") == false, "");
    static_assert(FLOAT::is_match("6.0") == true, "");
    static_assert(FLOAT::is_match(".801") == true, "");
    static_assert(FLOAT::is_match("42051.16") == true, "");
    static_assert(FLOAT::is_match("000.000") == true, "");
    
    using email = sk_regex(^[a-zA-Z0-9\\-_.]+@[a-zA-Z0-9.]+$);
    
    static_assert(email::is_match("grettireirikur@gmail.com"), "");
    static_assert(email::is_match("john.smith66@mailnow.io"), "");
    static_assert(email::is_match("spam-55@edu.qbert.org"), "");
    
    static_assert(email::is_match("john.smith66@mail-now.io") == false, "");
    static_assert(email::is_match("jöhn.smith66@mailnow.io") == false, "");
    static_assert(email::is_match("john.smith66mailnow.io") == false, "");
}
