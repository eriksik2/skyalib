#pragma once

// constexpr string manipulation

namespace sk {
    constexpr size_t static_strlen(const char* str, size_t n = 0) {
        if(str[n] == 0) return n;
        return static_strlen(str, n+1);
    }
    
    constexpr int static_strcmp(const char* lhs, const char* rhs, size_t n = 0) {
        if(lhs[n] != rhs[n]) return lhs[n] < rhs[n] ? -1 : 1;
        if(lhs[n] == 0 || rhs[n] == 0) return 0;
        return static_strcmp(lhs, rhs, n+1);
    }
    
    constexpr int static_strncmp(const char* lhs, const char* rhs, size_t count, size_t n = 0) {
        if(lhs[n] != rhs[n]) return lhs[n] < rhs[n] ? -1 : 1;
        if(lhs[n] == 0 || rhs[n] == 0 || n >= count-1) return 0;
        return static_strncmp(lhs, rhs, count, n+1);
    }
    
    constexpr const char* static_strchr(const char* str, int ch, size_t n = 0) {
        if(str[n] == ch) return str + n;
        if(str[n] == 0) return (const char*)0;
        return static_strchr(str, ch, n+1);
    }
    
    namespace impl {
        constexpr const char* static_strrchr_impl(const char* str, int ch, size_t n) {
            if(str[n] == ch) return str + n;
            if(n == 0) return (const char*)0;
            return static_strrchr_impl(str, ch, n-1);
        }
    }
    constexpr const char* static_strrchr(const char* str, int ch) {
        return impl::static_strrchr_impl(str, ch, static_strlen(str));
    }
}
