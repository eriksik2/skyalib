#pragma once

#include <sk/static/tstring.h>
#include <sk/utility/index_sequence.h>
#include <sk/type_traits/enable_if.h>
#include <sk/type_traits/conditional.h>
#include <sk/type_traits/is_same.h>

namespace sk {
    namespace static_regex_impl {
        
        template<class T, class...Tr> struct reduce {
            using type = T;
        };
        
        template<template<class...> class TT, class Tf, class...Tr1, class...Tr2>
        struct reduce<TT<Tf, Tr1...>, Tr2...> { // shift
            using type = typename reduce<TT<Tr1...>, Tr2..., Tf>::type;
        };
        
        template<template<class...> class TT, class...Tfr, class...Tr1, class...Tr2>
        struct reduce<TT<TT<Tfr...>, Tr1...>, Tr2...> { // reduce
            using type = typename reduce<TT<typename reduce<Tfr>::type..., Tr1...>, Tr2...>::type;
        };
        
        template<template<class> class TT, class Tf>
        struct reduce<TT<Tf>> { // done/shift single
            using type = TT<Tf>;
        };
        
        template<template<class> class TT, class Tf>
        struct reduce<TT<TT<Tf>>> { // reduce single
            using type = typename reduce<TT<typename reduce<Tf>::type>>::type;
        };
        
        template<template<class...> class TT, class...Tr>
        struct reduce<TT<>, Tr...> { // done
            using type = TT<Tr...>;
        };
        
        template<class T> struct common {
            using type = typename reduce<T>::type;
            using ctype = common<type>;
            
            struct match_result {
                const char* match = nullptr;
                int size = 0;
            };
            
            static constexpr match_result match(const char* start) {
                match_result res;
                const char* end = start;
                while(!type::type::check_internal(end = start))
                    if(*(++start) == '\0') return res;
                res.match = start;
                res.size = end - start;
                return res;
            };
            
            // checks if str matches expression
            static constexpr bool is_match(const char* str) { return type::type::check_internal(str); }
            template<char...ch> static constexpr bool is_match() {
                const char data[] = { ch..., '\0' };
                return is_match(data);
            }
            
            // checks if whole str matches expression
            static constexpr bool is_match_strict(const char* str) { return type::type::check_internal(str) && *str == '\0'; }
            
            // get length of match
            static constexpr int length(const char* str) {
                const char* start = str;
                type::type::check_internal(str);
                return (int)(str - start);
            }
            template<char...ch> static constexpr int length() {
                const char data[] = { ch..., '\0' };
                return length(data);
            }
            
            // return pointer to next char in str after match
            static constexpr const char* next(const char* str) { type::type::check_internal(str); return str; }
            /*template<int Len, char here, char...ch> static constexpr auto next() {
                if constexpr(Len != 0) return next<Len-1, ch...>();
                else return sk::tstring<here, ch...>{};
            }*/
            
            template<class Tstr> struct apply {};
            template<char...ch> struct apply<tstring<ch...>> {
                constexpr static const bool is_match = common::is_match<ch...>();
                constexpr static const int length = common::length<ch...>();
                using match = sk::copy_tstring<sk::tstring<ch...>, length>;
                
                //using next = decltype(common::next<length, ch...>());
                using next = sk::substr_tstring<tstring<ch...>, length>;
            };
        };
        
        template<class...T> // union type
        struct union_impl : common<union_impl<T...>>::ctype {
            static constexpr bool check_internal(const char*& str){
                const char* strcp = str;
                auto ret = (T::type::check_internal(strcp = str) || ...);
                if(ret) str = strcp;
                return ret;
            }
        };
        template<> struct union_impl<> : common<union_impl<>>::ctype {
            static constexpr bool check_internal(const char*&){ return true; }
        };
        
        template<class...T> // sequence type
        struct sequence_impl : common<sequence_impl<T...>>::ctype {
            static constexpr bool check_internal(const char*& str){
                //if(*str == 0) return false;
                return (T::type::check_internal(str) && ...);
            }
        };
        template<> struct sequence_impl<> : common<sequence_impl<>>::ctype {
            static constexpr bool check_internal(const char*&){ return true; }
        };
        
        template<class T> // any quantifier type
        struct any_impl : common<any_impl<T>>::ctype {
            static constexpr bool check_internal(const char*& str){
                while(/* *str != 0 && */T::type::check_internal(str));
                return true;
            }
        };
        
        template<char ch>
        struct ct_impl : common<ct_impl<ch>>::ctype  {
            static constexpr bool check_internal(const char*& str){
                if(*str != ch) return false;
                if(*str != '\0') ++str;
                return true;
            }
        };
        
        template<class...T> struct negative_union_impl : common<negative_union_impl<T...>>::ctype {};
        template<char...ch> // inverted union type
        struct negative_union_impl<ct_impl<ch>...> : common<negative_union_impl<ct_impl<ch>...>>::ctype {
            static constexpr bool check_internal(const char*& str){
                if(*str == 0 || ((*str == ch) || ...)) return false;
                ++str;
                return true;
            }
        };
        template<> struct negative_union_impl<> : common<negative_union_impl<>>::ctype {
            static constexpr bool check_internal(const char*&){ return true; }
        };
        
        
        template<class T> struct common<sequence_impl<T>> : common<T>::ctype {};
        template<class T> struct common<union_impl<T>> : common<T>::ctype {};
        template<class T> struct common<negative_union_impl<T>> : common<T>::ctype {};
        
        // --- aliases
        
        template<class...T>
        using union_ = typename union_impl<T...>::type;
        
        template<class...T>
        using negative_union_ = typename negative_union_impl<T...>::type;
        
        template<class...T>
        using sequence = typename sequence_impl<T...>::type;
        
        template<class T>
        using any = typename any_impl<T>::type;
        
        template<char ch>
        using ct = typename ct_impl<ch>::type;

        // --- make_regex
        
        template<class Tstr, class Tlast = sequence<>, class...Tres>
        struct make_regex {};
        
        template<class Tlast, class...Tres>
        struct make_regex<sk::tstring<>, Tlast, Tres...> {
            using type = sequence<Tres..., Tlast>;
            using next = sk::tstring<>;
        };
        template<class...Tres>
        struct make_regex<sk::tstring<>, sequence<>, Tres...> {
            using type = sequence<Tres...>;
            using next = sk::tstring<>;
        };
        
        template<char here, char...rest, class Tlast, class...Tres>
        struct make_regex<sk::tstring<here, rest...>, Tlast, Tres...> {
            using make_type = make_regex<sk::tstring<rest...>, ct<here>, Tres..., Tlast>;
            using type = typename make_type::type;
            using next = typename make_type::next;
        };
        
        template<class Tlast, class...Tres>
        struct make_regex<sk::tstring<'$'>, Tlast, Tres...> { // end of string
            using type = sequence<Tres..., Tlast, ct<'\0'>>;
            using next = sk::tstring<'\0'>;
        };
        
        template<char...rest>
        struct make_regex<sk::tstring<'^', rest...>, sequence<>> { // start of string
            using make_type = make_regex<sk::tstring<rest...>, sequence<>>;
            using type = typename make_type::type;
            using next = typename make_type::next;
        };
        
        template<char here, char...rest, class Tlast, class...Tres>
        struct make_regex<sk::tstring<'\\', here, rest...>, Tlast, Tres...> { // escape
            using make_type = make_regex<sk::tstring<rest...>, ct<here>, Tres..., Tlast>;
            using type = typename make_type::type;
            using next = typename make_type::next;
        };
        
        template<char...rest, class Tlast, class...Tres>
        struct make_regex<sk::tstring<'\\', 'n', rest...>, Tlast, Tres...> { // newline
            using make_type = make_regex<sk::tstring<rest...>, ct<'\n'>, Tres..., Tlast>;
            using type = typename make_type::type;
            using next = typename make_type::next;
        };
        
        template<char...rest, class Tlast, class...Tres>
        struct make_regex<sk::tstring<'"', rest...>, Tlast, Tres...> { // quote
            using make_type = make_regex<sk::tstring<rest...>, Tlast, Tres...>;
            using type = typename make_type::type;
            using next = typename make_type::next;
        };
        
        template<char...rest, class Tlast, class...Tres>
        struct make_regex<sk::tstring<'|', rest...>, Tlast, Tres...> { // or
            using make_type = make_regex<sk::tstring<rest...>>;
            using make_type2 = make_regex<typename make_type::next, union_<sequence<Tres..., Tlast>, typename make_type::type>>;
            using next = typename make_type2::next;
            using type = typename make_type2::type;
        };
        
        template<char...rest, class Tlast, class...Tres>
        struct make_regex<sk::tstring<'(', rest...>, Tlast, Tres...> { // open group
            using make_type = make_regex<sk::tstring<rest...>>;
            using make_type2 = make_regex<typename make_type::next, sequence<Tres..., Tlast, typename make_type::type>>;
            using next = typename make_type2::next;
            using type = typename make_type2::type;
        };
        template<char...rest, class Tlast, class...Tres>
        struct make_regex<sk::tstring<')', rest...>, Tlast, Tres...> { // close group
            using type = sequence<Tres..., Tlast>;
            using next = sk::tstring<rest...>;
        };
        
        template<char...rest, class Tlast, class...Tres>
        struct make_regex<sk::tstring<'?', rest...>, Tlast, Tres...> { // one or zero
            using make_type = make_regex<sk::tstring<rest...>, union_<Tlast, sequence<>>, Tres...>;
            using type = typename make_type::type;
            using next = typename make_type::next;
        };
        
        template<char...rest, class Tlast, class...Tres>
        struct make_regex<sk::tstring<'+', rest...>, Tlast, Tres...> { // one or more
            using make_type = make_regex<sk::tstring<rest...>, sequence<Tlast, any<Tlast>>, Tres...>;
            using type = typename make_type::type;
            using next = typename make_type::next;
        };
        
        template<char...rest, class Tlast, class...Tres>
        struct make_regex<sk::tstring<'*', rest...>, Tlast, Tres...> { // any
            using make_type = make_regex<sk::tstring<rest...>, any<Tlast>, Tres...>;
            using type = typename make_type::type;
            using next = typename make_type::next;
        };
        
        template<bool negative, class Tstr, class Tlast = sk::conditional<negative, negative_union_<>, union_<>>, class...Tres>
        struct make_regex_union {};
        
        template<bool negative, char here, char...rest, class Tlast, class...Tres>
        struct make_regex_union<negative, sk::tstring<here, rest...>, Tlast, Tres...> {
            using make_type = make_regex_union<negative, sk::tstring<rest...>, ct<here>, Tres..., Tlast>;
            using type = typename make_type::type;
            using next = typename make_type::next;
        };
        
        template<bool negative, char here, char...rest, class Tlast, class...Tres>
        struct make_regex_union<negative, sk::tstring<'\\', here, rest...>, Tlast, Tres...> { // escape
            using make_type = make_regex_union<negative, sk::tstring<rest...>, ct<here>, Tres..., Tlast>;
            using type = typename make_type::type;
            using next = typename make_type::next;
        };
        
        template<bool negative, char...rest, class Tlast, class...Tres>
        struct make_regex_union<negative, sk::tstring<'\\', 'n', rest...>, Tlast, Tres...> { // newline
            using make_type = make_regex_union<negative, sk::tstring<rest...>, ct<'\n'>, Tres..., Tlast>;
            using type = typename make_type::type;
            using next = typename make_type::next;
        };

        template<bool negative, char unrelatedto, char...rest, class Tlast, class...Tres>
        struct make_regex_union<negative, sk::tstring<'\\', '-', unrelatedto, rest...>, Tlast, Tres...> { // Escaped hyphen
            using make_type = make_regex_union<negative, sk::tstring<unrelatedto, rest...>, ct<'-'>, Tres..., Tlast>;
            using type = typename make_type::type;
            using next = typename make_type::next;
        };
        
        template<char from, sk::enable_if<from != '\\', bool> negative, char to, char...rest, class Tlast, class...Tres>
        struct make_regex_union<negative, sk::tstring<from, '-', to, rest...>, Tlast, Tres...> { // range
            static_assert(from < to, "Invalid character range");
            using make_type = make_regex_union<negative, sk::tstring<from+1, '-', to, rest...>, ct<from>, Tres..., Tlast>;
            using type = typename make_type::type;
            using next = typename make_type::next;
        };
        
        template<char from, sk::enable_if<from != '\\', bool> negative, char...rest, class Tlast, class...Tres>
        struct make_regex_union<negative, sk::tstring<from, '-', from, rest...>, Tlast, Tres...> { // range end
            using make_type = make_regex_union<negative, sk::tstring<rest...>, ct<from>, Tres..., Tlast>;
            using type = typename make_type::type;
            using next = typename make_type::next;
        };
        
        template<bool negative, char...rest, class Tlast, class...Tres>
        struct make_regex_union<negative, sk::tstring<']', rest...>, Tlast, Tres...> { // end union
            using make_type = make_regex<sk::tstring<rest...>, sk::conditional<negative, negative_union_<Tres..., Tlast>, union_<Tres..., Tlast>>>;
            using type = sk::conditional<negative, negative_union_<Tres..., Tlast>, union_<Tres..., Tlast>>;
            using next = sk::tstring<rest...>;
        };
        
        template<char...rest, class Tlast, class...Tres>
        struct make_regex<sk::tstring<'[', rest...>, Tlast, Tres...> { // initiate union
            using make_type = make_regex_union<false, sk::tstring<rest...>>;
            using make_type2 = make_regex<typename make_type::next, typename make_type::type, Tres..., Tlast>;
            using next = typename make_type2::next;
            using type = typename make_type2::type;
        };
        
        template<char...rest, class Tlast, class...Tres>
        struct make_regex<sk::tstring<'[', '^', rest...>, Tlast, Tres...> { // initiate negative union
            using make_type = make_regex_union<true, sk::tstring<rest...>>;
            using make_type2 = make_regex<typename make_type::next, typename make_type::type, Tres..., Tlast>;
            using next = typename make_type2::next;
            using type = typename make_type2::type;
        };
    }
    
    template<class Tstr>
    using make_regex = typename static_regex_impl::make_regex<Tstr>::type;
}
#define sk_regex(text) ::sk::make_regex<TSTRING(#text)>
