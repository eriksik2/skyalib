#pragma once


#include <sk/static/string.h>
#include <sk/utility/types.h>

namespace sk {
    template<char...c>
    struct tstring {
        constexpr static const char c_str[] = { c..., '\0' };
        constexpr static sk::size_t size = sizeof...(c);
        
        constexpr static const char* cbegin() { return c_str; }
        constexpr static const char* cend() { return c_str + size; }
    };
    
    
    namespace impl { // concat_tstring
        template<class T1, class T2> struct concat_tstring;
        template<char...c1, char...c2>
        struct concat_tstring<tstring<c1...>, tstring<c2...>> {
            using type = tstring<c1..., c2...>;
        };
    }
    template<class T1, class T2> using concat_tstring = typename impl::concat_tstring<T1, T2>::type;
    
    
    namespace impl { // transform_tstring
        template<class T, char(*func)(char)> struct transform_tstring;
        template<char(*func)(char), char...c>
        struct transform_tstring<tstring<c...>, func>{
            using type = tstring<(func)(c)...>;
        };
    }
    template<class T, char(*func)(char)> using transform_tstring = typename impl::transform_tstring<T, func>::type;
    
    
    namespace impl { // make_tstring
        template<const char* (*Tclass)(), int N, char...c>
        struct make_tstring {
            using type = typename make_tstring<Tclass, N-1, (Tclass)()[N-1], c...>::type;
        };
        template<const char* (*Tclass)(), char...c>
        struct make_tstring<Tclass, 0, c...> {
            using type = sk::tstring<c...>;
        };
    }
    template<const char* (*Tclass)(), int Slen = static_strlen((Tclass)())> using make_tstring = typename impl::make_tstring<Tclass, Slen>::type;
    
    
    namespace impl { // copy_tstring
        template<class Tstr, int N, char...c> struct copy_tstring {};
        template<char next, char...strc, int N, char...c>
        struct copy_tstring<sk::tstring<next, strc...>, N, c...> {
            using type = typename copy_tstring<sk::tstring<strc...>, N-1, c..., next>::type;
        };
        template<char...strc, char...c>
        struct copy_tstring<sk::tstring<strc...>, 0, c...> {
            using type = sk::tstring<c...>;
        };
        template<char next, char...strc, char...c>
        struct copy_tstring<sk::tstring<next, strc...>, 0, c...> {
            using type = sk::tstring<c...>;
        };
    }
    template<class Tstr, int Slen = Tstr::size> using copy_tstring = typename impl::copy_tstring<Tstr, Slen>::type;
    
    
    namespace impl { // substr_tstring
        template<class Tstr, int start = 0, int end = Tstr::size> struct substr_tstring;
        template<char here, char...rest, int i, int end>
        struct substr_tstring<sk::tstring<here, rest...>, i, end> {
            using type = typename substr_tstring<sk::tstring<rest...>, i-1, end-1>::type;
        };
        template<char here, char...rest, int end>
        struct substr_tstring<sk::tstring<here, rest...>, 0, end> {
            using type = sk::copy_tstring<sk::tstring<here, rest...>, end>;
        };
        template<int end>
        struct substr_tstring<sk::tstring<>, 0, end> {
            using type = sk::tstring<>;
        };
    }
    template<class Tstr, int start = 0, int end = Tstr::size> using substr_tstring = typename impl::substr_tstring<Tstr, start, end>::type;
}

#define TSTRING(str) ::sk::make_tstring<static_cast<const char*(*)()>([]{ return str; })>
