#pragma once
#include "BaseObject.h"

#include "sk/array.h"
#include "sk/string.h"

namespace sk {

    class ShaderObject;

    struct ProgramInput {
        GLint size;
        GLenum type;
        sk::string name;
        GLint location;
    };

    class ProgramObject : public BaseObject {
        friend class Renderer;
        using BaseObject::BaseObject;

        GLint link_status = 0; // GL_FALSE

        array<ProgramInput> attributes;
        array<ProgramInput> uniforms;

        GLint GenericGetLocation(const array<ProgramInput>& list, const GLchar* name);
        void DeleteInfo();
    public:
        ~ProgramObject();

        ProgramObject& operator=(ProgramObject&& f);
        ProgramObject(ProgramObject&& f);

        void AttachShader(const ShaderObject& shader);
        void DetachShader(const ShaderObject& shader);

        GLint GetUniformLocation(const GLchar* name);
        GLint GetAttributeLocation(const GLchar* name);

        template<class T>
        bool SetUniform(int location, T&& value, bool transpose = false) {
            return SetUniform(location, (const void*)&value, transpose);
        }
        template<class T>
        bool SetUniform(const char* name, T&& value, bool transpose = false) {
            return SetUniform(name, (const void*)&value, transpose);
        }

        bool SetUniform(int location, const void* value, bool transpose = false);
        bool SetUniform(const char* name, const void* value, bool transpose = false);

        bool Link();
        bool Binary(GLenum binaryFormat, const void *binary, GLsizei length);
    private:
        bool CheckProgram();
        bool SetUniformInternal(const ProgramInput* uniform, const void* value, bool transpose = false);
    };
}