#pragma once
#include "GL/glew.h"

namespace sk {

    class Renderer;

    class BaseObject {
        friend class Renderer;
        GLuint m_handle = 0;
        Renderer* m_renderer = nullptr;

    protected:
        BaseObject(Renderer* r, GLuint h) : m_handle(h), m_renderer(r) {}
    public:

        Renderer& renderer() const { return *m_renderer; }
        GLuint handle() const { return m_handle; }

        BaseObject& operator=(const BaseObject&) = delete;
        BaseObject(const BaseObject&) = delete;

        BaseObject& operator=(BaseObject&& f) {
            auto tmp = f.m_handle;
            f.m_handle = m_handle;
            m_handle = tmp;
            m_renderer = f.m_renderer;
            return *this;
        }
        BaseObject(BaseObject&& f) : BaseObject(f.m_renderer, f.m_handle) {
            f.m_handle = 0;
        }
    };
}
