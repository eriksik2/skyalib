#pragma once
#include "BaseObject.h"

namespace sk {

    class ShaderObject : public BaseObject {
        friend class Renderer;

        GLenum type;
        GLint compile_status = 0; // GL_FALSE

        ShaderObject(Renderer* r, GLuint h, GLenum t)
            : BaseObject(r, h)
            , type(t) {}
    public:

        ShaderObject& operator=(ShaderObject&&) = default;
        ShaderObject(ShaderObject&&) = default;

        ~ShaderObject();

        void Source(GLsizei count, const GLchar** string, const GLint* length);
        bool Compile();
    };
}