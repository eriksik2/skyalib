#pragma once
#include "BaseObject.h"

namespace sk {

    class VertexArrayObject : public BaseObject {
        friend class Renderer;
        using BaseObject::BaseObject;

        GLuint element_array_buffer_binding = 0;

    public:

        VertexArrayObject(VertexArrayObject&&) = default;
        VertexArrayObject& operator=(VertexArrayObject&&) = default;

        ~VertexArrayObject();

        void ElementBuffer(GLuint buffer);

        void EnableAttributeArray(GLuint attributeindex);
        void DisableAttributeArray(GLuint attributeindex);

        void AttributePointer(GLuint buffer, GLuint attributeindex, GLint size, GLenum type, GLboolean normalized, GLsizei stride, const GLvoid* pointer);
    };
}