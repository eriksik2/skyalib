#pragma once
#include "GL/glew.h"
#include "GLFW/glfw3.h"

#include "BufferObject.h"
#include "FramebufferObject.h"
#include "ProgramObject.h"
#include "ShaderObject.h"
#include "TextureObject.h"
#include "VertexArrayObject.h"

namespace sk {

    class Renderer {
        static void APIENTRY DebugLogCallback(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar* message, const void* userParam);

        void init_msg() const;

        bool is_current = false;

        // Buffer bindings
        GLuint array_buffer_binding = 0;
        GLuint atomic_counter_buffer_binding = 0;
        GLuint copy_read_buffer_binding = 0;
        GLuint copy_write_buffer_binding = 0;
        GLuint dispatch_indirect_buffer_binding = 0;
        GLuint draw_indirect_buffer_binding = 0;
        GLuint pixel_pack_buffer_binding = 0;
        GLuint pixel_unpack_buffer_binding = 0;
        GLuint query_buffer_binding = 0;
        GLuint shader_storage_buffer_binding = 0;
        GLuint transform_feedback_buffer_binding = 0;
        GLuint uniform_buffer_binding = 0;

        // Framebuffer bindings
        GLuint draw_framebuffer_binding = 0;
        GLuint read_framebuffer_binding = 0;


        // Texture bindings
        GLuint texture_1D_binding = 0;
        GLuint texture_2D_binding = 0;
        GLuint texture_3D_binding = 0;
        GLuint texture_1D_array_binding = 0;
        GLuint texture_2D_array_binding = 0;
        GLuint texture_rectangle_binding = 0;
        GLuint texture_cube_map_binding = 0;
        GLuint texture_cube_map_array_binding = 0;
        GLuint texture_buffer_binding = 0;
        GLuint texture_2D_multisample_binding = 0;
        GLuint texture_2D_multisample_array_binding = 0;

        GLint max_combined_texture_image_units = 0;

        GLuint vertex_array_binding = 0;
        GLuint current_program = 0;

        GLuint* Binding(GLenum target);
    public:
        // points to the most recent Renderer instance
        static Renderer* current;

        GLFWwindow* window = nullptr;

        // inits glew and glfw
        Renderer();
        // terminates glfw
        ~Renderer();

        TextureObject CreateTexture(GLenum target);
        void CreateTextures(GLenum target, GLsizei n, GLuint* textures);
        void DeleteTextures(GLsizei n, const GLuint* textures);

        BufferObject CreateBuffer(GLenum target);
        void CreateBuffers(GLenum target, GLsizei n, GLuint* buffers);
        void DeleteBuffers(GLsizei n, const GLuint* buffers);

        FramebufferObject CreateFramebuffer();
        void CreateFramebuffers(GLsizei n, GLuint* ids);
        void DeleteFramebuffers(GLsizei n, const GLuint* ids);

        VertexArrayObject CreateVertexArray();
        void CreateVertexArrays(GLsizei n, GLuint* arrays);
        void DeleteVertexArrays(GLsizei n, const GLuint* buffers);

        ShaderObject CreateShader(GLenum shaderType);
        void DeleteShader(GLuint shader);

        ProgramObject CreateProgram();
        void DeleteProgram(GLuint program);

        GLuint GetBinding(GLenum target) const;
        GLuint GetVertexArrayBinding() const;
        GLuint GetCurrentProgram() const;

        void BindTextureUnit(GLuint unit, GLuint texture);
        void BindTexture(GLenum target, GLuint texture);
        void BindBuffer(GLenum target, GLuint buffer);
        void BindFramebuffer(GLenum target, GLuint fb);
        void BindVertexArray(GLuint array);
        void UseProgram(GLuint program);
    };
}
