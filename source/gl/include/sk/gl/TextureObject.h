#pragma once
#include "BaseObject.h"

namespace sk {

    class FramebufferObject;

    class TextureObject : public BaseObject {
        friend class Renderer;

        GLenum depth_stencil_mode = 0x1902; // GL_DEPTH_COMPONENT
        GLint base_level = 0;
        GLfloat border_color[4] = { 0.0f, 0.0f, 0.0f, 0.0f };
        GLenum compare_func;
        GLenum compare_mode = 0; // GL_NONE
        GLfloat lod_bias = 0.0f;
        GLenum min_filter = 0x2702; // GL_NEAREST_MIPMAP_LINEAR
        GLenum mag_filter = 0x2601; // GL_LINEAR
        GLfloat min_lod = -1000;
        GLfloat max_lod = 1000;
        GLint max_level = 1000;
        GLenum swizzle_r = 0x1903; // GL_RED
        GLenum swizzle_g = 0x1904; // GL_GREEN
        GLenum swizzle_b = 0x1905; // GL_BLUE
        GLenum swizzle_a = 0x1906; // GL_ALPHA
        GLenum wrap_s = 0x2901; // GL_REPEAT
        GLenum wrap_t = 0x2901; // GL_REPEAT
        GLenum wrap_r = 0x2901; // GL_REPEAT

        void Parameteri(GLenum pname, GLint param);
        void Parameterf(GLenum pname, GLfloat param);
        void Parameteriv(GLenum pname, GLint* param);
        void Parameterfv(GLenum pname, GLfloat* param);

        TextureObject(Renderer* r, GLuint h, GLenum t)
            : BaseObject(r, h)
            , target(t) {}

    public:
        const GLenum target;

        TextureObject(TextureObject&&) = default;
        TextureObject& operator=(TextureObject&&) = default;

        ~TextureObject();

        void SetDepthStencilMode(GLenum mode);
        void SetBaseLevel(GLint level);
        void SetBorderColor(GLfloat color[4]);
        void SetCompareFunc(GLenum func);
        void SetCompareNode(GLenum mode);
        void SetLodBias(GLfloat bias);

        void SetMinFilter(GLenum filter);
        void SetMagFilter(GLenum filter);
        void SetMinMagFilter(GLenum min_filter, GLenum mag_filter);
        void SetMinMagFilter(GLenum minmag_filter);

        void SetMinLod(GLfloat lod);
        void SetMaxLod(GLfloat lod);
        void SetMaxLevel(GLint level);
        void SetSwizzleR(GLenum color);
        void SetSwizzleG(GLenum color);
        void SetSwizzleB(GLenum color);
        void SetSwizzleA(GLenum color);
        void SetSwizzleRGBA(GLenum color[4]);
        void SetWrapS(GLenum wrap);
        void SetWrapT(GLenum wrap);
        void SetWrapR(GLenum wrap);

        void GenerateMipmap();

        void Storage1D(GLint levels, GLenum internalformat, GLsizei width);
        void Storage2D(GLint levels, GLenum internalformat, GLsizei width, GLsizei height);
        void Storage3D(GLint levels, GLenum internalformat, GLsizei width, GLsizei height, GLsizei depth);

        void Image1D(GLint level, GLint internalformat, GLsizei width, GLenum format, GLenum type, void* data);
        void Image2D(GLint level, GLint internalformat, GLsizei width, GLsizei height, GLenum format, GLenum type, void* data);
        void Image3D(GLint level, GLint internalformat, GLsizei width, GLsizei height, GLsizei depth, GLenum format, GLenum type, void* data);

        void SubImage1D(GLint level, GLint xoffset, GLsizei width, GLenum format, GLenum type, void* data);
        void SubImage2D(GLint level, GLint xoffset, GLint yoffset, GLsizei width, GLsizei height, GLenum format, GLenum type, void* data);
        void SubImage3D(GLint level, GLint xoffset, GLint yoffset, GLint zoffset, GLsizei width, GLsizei height, GLsizei depth, GLenum format, GLenum type, void* data);


        void CopyImage1D(const FramebufferObject& framebuffer, GLint level, GLenum internalformat, GLint x, GLint y, GLsizei width);
        void CopyImage2D(const FramebufferObject& framebuffer, GLint level, GLenum internalformat, GLint x, GLint y, GLsizei width, GLsizei height);

        void CopySubImage1D(const FramebufferObject& framebuffer, GLint level, GLint xoffset, GLint x, GLint y, GLsizei width);
        void CopySubImage2D(const FramebufferObject& framebuffer, GLint level, GLint xoffset, GLint yoffset, GLint x, GLint y, GLsizei width, GLsizei height);
        void CopySubImage3D(const FramebufferObject& framebuffer, GLint level, GLint xoffset, GLint yoffset, GLint zoffset, GLint x, GLint y, GLsizei width, GLsizei height);
    };
}