#define SKGL_MAJOR 4
#define SKGL_MINOR 1

// -----

#ifndef SKGL_MAJOR
#define SKGL_MAJOR 3
#endif
#ifndef SKGL_MINOR
#define SKGL_MINOR 3
#endif

#define SKGL_VER(maj, min) (SKGL_MAJOR > maj || (SKGL_MAJOR >= maj && SKGL_MINOR >= min))
//#define SKGL_VER(maj, min) (GLEW_VERSION_##maj##_##min)

