#pragma once
#include "BaseObject.h"

namespace sk {
    class BufferObject : public BaseObject {
        friend class Renderer;

        GLenum target = 0;

        BufferObject(Renderer* r, GLuint h, GLenum t)
            : BaseObject(r, h)
            , target(t) {}
    public:

        BufferObject& operator=(BufferObject&&) = default;
        BufferObject(BufferObject&&) = default;

        ~BufferObject();

        void Data(GLsizeiptr size, const GLvoid* data, GLenum usage);
        void SubData(GLintptr offset, GLsizeiptr size, const GLvoid* data);

        void CopySubData(GLuint toBuffer, GLintptr readOffset, GLintptr writeOffset, GLsizeiptr size);

        void* MapRange(GLintptr offset, GLsizeiptr length, GLbitfield access);
    };
}