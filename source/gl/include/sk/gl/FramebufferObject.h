#pragma once
#include "BaseObject.h"

namespace sk {

    class TextureObject;

    class FramebufferObject : public BaseObject {
        friend class Renderer;
        using BaseObject::BaseObject;
    public:
        FramebufferObject(FramebufferObject&&) = default;
        FramebufferObject& operator=(FramebufferObject&&) = default;

        ~FramebufferObject();

        void AttachTexture(GLenum attachment, const TextureObject& texture, GLint level);
        void AttachTexture(GLenum attachment, const TextureObject& texture, GLint level, GLint layer);

        template<class...T>
        void SetDrawBuffers(T...bufs) {
            GLenum b[sizeof...(T)] = { GLenum(bufs)... };
            SetDrawBuffers((GLsizei)sizeof...(T), (const GLenum*)b);
        }

        void SetDrawBuffers(GLsizei n, const GLenum* bufs);
        void SetDrawBuffer(GLenum mode);
        void SetReadBuffer(GLenum mode);

        void ReadPixels(GLint x, GLint y, GLsizei width, GLsizei height, GLenum format, GLenum type, GLvoid * data);
    };
}