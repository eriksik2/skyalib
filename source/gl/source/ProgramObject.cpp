#include "ProgramObject.h"
#include "Defines.h"

#include "Renderer.h"
#include "ShaderObject.h"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

GLint sk::ProgramObject::GenericGetLocation(const array<ProgramInput>& list, const GLchar * name) {
    for(int i = 0; i < list.size(); ++i)
    {
        if(list[i].name == name) return list[i].location;
    }
    return -1;
}

void sk::ProgramObject::DeleteInfo() {}

sk::ProgramObject::~ProgramObject() {
    renderer().DeleteProgram(handle());
    DeleteInfo();
}

sk::ProgramObject& sk::ProgramObject::operator=(ProgramObject&& f) {
    BaseObject::operator=(const_cast<ProgramObject&&>(f));
    DeleteInfo();
    link_status = f.link_status;
    attributes = f.attributes;
    uniforms = f.uniforms;
    return *this;
}

sk::ProgramObject::ProgramObject(ProgramObject&& f) : BaseObject(static_cast<ProgramObject&&>(f)) {
    link_status = f.link_status;
    attributes = f.attributes;
    uniforms = f.uniforms;
}

void sk::ProgramObject::AttachShader(const ShaderObject & shader) {
    glAttachShader(handle(), shader.handle());
}

void sk::ProgramObject::DetachShader(const ShaderObject & shader) {
    glDetachShader(handle(), shader.handle());
}

GLint sk::ProgramObject::GetUniformLocation(const GLchar * name) {
    return GenericGetLocation(uniforms, name);
}

GLint sk::ProgramObject::GetAttributeLocation(const GLchar * name) {
    return GenericGetLocation(attributes, name);
}

bool sk::ProgramObject::SetUniform(int location, const void * value, bool transpose) {
    ProgramInput* uniform = nullptr;
    for(int i = 0; i < uniforms.size(); ++i)
        if(uniforms[i].location == location)
        {
            uniform = &uniforms[i];
            break;
        }
    if(!uniform) return false;
    return SetUniformInternal(uniform, value, transpose);
}

bool sk::ProgramObject::SetUniform(const char * name, const void * value, bool transpose) {
    ProgramInput* uniform = nullptr;
    for(int i = 0; i < uniforms.size(); ++i)
        if(strcmp(uniforms[i].name.c_str(), name)==0)
        {
            uniform = &uniforms[i];
            break;
        }
    if(!uniform) return false;
    return SetUniformInternal(uniform, value, transpose);
}



bool sk::ProgramObject::Link() {
    glLinkProgram(handle());
    return CheckProgram();
}

bool sk::ProgramObject::Binary(GLenum binaryFormat, const void *binary, GLsizei length) {
    glProgramBinary(handle(), binaryFormat, binary, length);
    return CheckProgram();
}

bool sk::ProgramObject::CheckProgram() {
    glGetProgramiv(handle(), GL_LINK_STATUS, &link_status);
    if(link_status == GL_FALSE)
    {
        GLint logLength;
        glGetProgramiv(handle(), GL_INFO_LOG_LENGTH, &logLength);

        GLchar* infoLog = (GLchar*)malloc(logLength*sizeof(GLchar));
        glGetProgramInfoLog(handle(), logLength, nullptr, infoLog);
        printf("SHADER PROGRAM FAILED TO LINK: %s\n", infoLog);
        free(infoLog);
        return false;
    }

    GLint len1, len2, attribute_count, uniform_count;
    glGetProgramiv(handle(), GL_ACTIVE_ATTRIBUTES, &attribute_count);
    glGetProgramiv(handle(), GL_ACTIVE_UNIFORMS, &uniform_count);
    glGetProgramiv(handle(), GL_ACTIVE_ATTRIBUTE_MAX_LENGTH, &len1);
    glGetProgramiv(handle(), GL_ACTIVE_UNIFORM_MAX_LENGTH, &len2);
    GLint longestName = len1 > len2 ? len1 : len2;

    GLchar* nameBuffer = (GLchar*)malloc(longestName*sizeof(GLchar));

    attributes.set(attribute_count);
    for(int i = 0; i < attribute_count; ++i)
    {
        glGetActiveAttrib(handle(), i, longestName, 0, &attributes[i].size, &attributes[i].type, nameBuffer);
        attributes[i].name = nameBuffer;
        attributes[i].location = glGetAttribLocation(handle(), attributes[i].name.c_str());
    }

    uniforms.set(uniform_count);
    for(int i = 0; i < uniform_count; ++i)
    {
        glGetActiveUniform(handle(), i, longestName, 0, &uniforms[i].size, &uniforms[i].type, nameBuffer);
        uniforms[i].name = nameBuffer;
        uniforms[i].location = glGetUniformLocation(handle(), uniforms[i].name.c_str());
    }

    free(nameBuffer);

    return true;
}

bool sk::ProgramObject::SetUniformInternal(const ProgramInput * uniform, const void * value, bool transpose) {
    renderer().UseProgram(handle());

    GLint location = uniform->location;
    GLint size = uniform->size;

    switch(uniform->type)
    {
    case GL_DOUBLE:
    case GL_FLOAT:
        glUniform1fv(location, size, (GLfloat*)value);
        return true;
    case GL_DOUBLE_VEC2:
    case GL_FLOAT_VEC2:
        glUniform2fv(location, size, (GLfloat*)value);
        return true;
    case GL_DOUBLE_VEC3:
    case GL_FLOAT_VEC3:
        glUniform3fv(location, size, (GLfloat*)value);
        return true;
    case GL_DOUBLE_VEC4:
    case GL_FLOAT_VEC4:
        glUniform4fv(location, size, (GLfloat*)value);
        return true;
    case GL_DOUBLE_MAT2:
    case GL_FLOAT_MAT2:
        glUniformMatrix2fv(location, size, transpose, (GLfloat*)value);
        return true;
    case GL_DOUBLE_MAT3:
    case GL_FLOAT_MAT3:
        glUniformMatrix3fv(location, size, transpose, (GLfloat*)value);
        return true;
    case GL_DOUBLE_MAT4:
    case GL_FLOAT_MAT4:
        glUniformMatrix4fv(location, size, transpose, (GLfloat*)value);
        return true;
    case GL_DOUBLE_MAT2x3:
    case GL_FLOAT_MAT2x3:
        glUniformMatrix2x3fv(location, size, transpose, (GLfloat*)value);
        return true;
    case GL_DOUBLE_MAT2x4:
    case GL_FLOAT_MAT2x4:
        glUniformMatrix2x4fv(location, size, transpose, (GLfloat*)value);
        return true;
    case GL_DOUBLE_MAT3x2:
    case GL_FLOAT_MAT3x2:
        glUniformMatrix3x2fv(location, size, transpose, (GLfloat*)value);
        return true;
    case GL_DOUBLE_MAT3x4:
    case GL_FLOAT_MAT3x4:
        glUniformMatrix3x4fv(location, size, transpose, (GLfloat*)value);
        return true;
    case GL_DOUBLE_MAT4x2:
    case GL_FLOAT_MAT4x2:
        glUniformMatrix4x2fv(location, size, transpose, (GLfloat*)value);
        return true;
    case GL_FLOAT_MAT4x3:
    case GL_DOUBLE_MAT4x3:
        glUniformMatrix4x3fv(location, size, transpose, (GLfloat*)value);
        return true;
    default: // default: it's a sampler.
    case GL_BOOL:
    case GL_INT:
        glUniform1iv(location, size, (GLint*)value);
        return true;
    case GL_BOOL_VEC2:
    case GL_INT_VEC2:
        glUniform2iv(location, size, (GLint*)value);
        return true;
    case GL_BOOL_VEC3:
    case GL_INT_VEC3:
        glUniform3iv(location, size, (GLint*)value);
        return true;
    case GL_BOOL_VEC4:
    case GL_INT_VEC4:
        glUniform4iv(location, size, (GLint*)value);
        return true;
    case GL_UNSIGNED_INT_ATOMIC_COUNTER:
    case GL_UNSIGNED_INT:
        glUniform1uiv(location, size, (GLuint*)value);
        return true;
    case GL_UNSIGNED_INT_VEC2:
        glUniform2uiv(location, size, (GLuint*)value);
        return true;
    case GL_UNSIGNED_INT_VEC3:
        glUniform3uiv(location, size, (GLuint*)value);
        return true;
    case GL_UNSIGNED_INT_VEC4:
        glUniform4uiv(location, size, (GLuint*)value);
        return true;
    }
}