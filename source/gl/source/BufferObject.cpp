#include "BufferObject.h"
#include "Defines.h"

#include "Renderer.h"

sk::BufferObject::~BufferObject() {
    auto h = handle();
    renderer().DeleteBuffers(1, &h);
}


void sk::BufferObject::Data(GLsizeiptr size, const GLvoid * data, GLenum usage) {
    if SKGL_VER(4, 5)
        glNamedBufferData(handle(), size, data, usage);
    else
    {
        renderer().BindBuffer(GL_COPY_READ_BUFFER, handle());
        glBufferData(GL_COPY_READ_BUFFER, size, data, usage);
    }
}

void sk::BufferObject::SubData(GLintptr offset, GLsizeiptr size, const GLvoid * data) {
    if SKGL_VER(4, 5)
        glNamedBufferSubData(handle(), offset, size, data);
    else
    {
        renderer().BindBuffer(GL_COPY_READ_BUFFER, handle());
        glBufferSubData(GL_COPY_READ_BUFFER, offset, size, data);
    }
}

void sk::BufferObject::CopySubData(GLuint toBuffer, GLintptr readOffset, GLintptr writeOffset, GLsizeiptr size) {
    if SKGL_VER(4, 5)
        glCopyNamedBufferSubData(handle(), toBuffer, readOffset, writeOffset, size);
    else
    {
        renderer().BindBuffer(GL_COPY_READ_BUFFER, handle());
        renderer().BindBuffer(GL_COPY_WRITE_BUFFER, toBuffer);

        glCopyBufferSubData(GL_COPY_READ_BUFFER, GL_COPY_WRITE_BUFFER, readOffset, writeOffset, size);
    }
}

void * sk::BufferObject::MapRange(GLintptr offset, GLsizeiptr length, GLbitfield access) {
    if SKGL_VER(4, 5)
        return glMapNamedBufferRange(handle(), offset, length, access);

    renderer().BindBuffer(GL_COPY_READ_BUFFER, handle());
    return glMapBufferRange(GL_COPY_READ_BUFFER, offset, length, access);
}
