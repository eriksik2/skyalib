#include "FramebufferObject.h"
#include "Defines.h"

#include "Renderer.h"
#include "TextureObject.h"

sk::FramebufferObject::~FramebufferObject() {
    auto h = handle();
    renderer().DeleteFramebuffers(1, &h);
}

void sk::FramebufferObject::AttachTexture(GLenum attachment, const TextureObject& texture, GLint level) {
    if SKGL_VER(4, 5) {
        glNamedFramebufferTexture(handle(), attachment, texture.handle(), level);
        return;
    }
    renderer().BindFramebuffer(GL_DRAW_FRAMEBUFFER, handle());
    glFramebufferTexture(GL_DRAW_FRAMEBUFFER, attachment, texture.handle(), level);
}

void sk::FramebufferObject::AttachTexture(GLenum attachment, const TextureObject& texture, GLint level, GLint layer) {
    if SKGL_VER(4, 5) {
        glNamedFramebufferTextureLayer(handle(), attachment, texture.handle(), level, layer);
        return;
    }

    renderer().BindFramebuffer(GL_DRAW_FRAMEBUFFER, handle());

    if(texture.target == GL_TEXTURE_CUBE_MAP)
    {
        GLenum face = GL_TEXTURE_CUBE_MAP_POSITIVE_X + layer%6;
        glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER, attachment, face, texture.handle(), level);
    }
    else
    {
        glFramebufferTextureLayer(GL_DRAW_FRAMEBUFFER, attachment, texture.handle(), level, layer);
    }
}

void sk::FramebufferObject::SetDrawBuffers(GLsizei n, const GLenum * bufs) {
    if SKGL_VER(4, 5) {
        glNamedFramebufferDrawBuffers(handle(), n, bufs);
        return;
    }
    renderer().BindFramebuffer(GL_DRAW_FRAMEBUFFER, handle());
    glDrawBuffers(n, bufs);
}

void sk::FramebufferObject::SetDrawBuffer(GLenum mode) {
    if SKGL_VER(4, 5) {
        glNamedFramebufferDrawBuffer(handle(), mode);
        return;
    }
    renderer().BindFramebuffer(GL_DRAW_FRAMEBUFFER, handle());
    glDrawBuffer(mode);
}

void sk::FramebufferObject::SetReadBuffer(GLenum mode) {
    if SKGL_VER(4, 5) {
        glNamedFramebufferReadBuffer(handle(), mode);
        return;
    }
    renderer().BindFramebuffer(GL_READ_FRAMEBUFFER, handle());
    glReadBuffer(mode);
}

void sk::FramebufferObject::ReadPixels(GLint x, GLint y, GLsizei width, GLsizei height, GLenum format, GLenum type, GLvoid * data) {
    renderer().BindFramebuffer(GL_READ_FRAMEBUFFER, handle());
    glReadPixels(x, y, width, height, format, type, data);
}
