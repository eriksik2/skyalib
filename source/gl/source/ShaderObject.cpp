#include "ShaderObject.h"
#include "Defines.h"

#include "Renderer.h"

#include <stdlib.h>
#include <stdio.h>

sk::ShaderObject::~ShaderObject() {
    renderer().DeleteShader(handle());
}

void sk::ShaderObject::Source(GLsizei count, const GLchar ** string, const GLint * length) {
    glShaderSource(handle(), count, string, length);
}

bool sk::ShaderObject::Compile() {
    glCompileShader(handle());
    glGetShaderiv(handle(), GL_COMPILE_STATUS, &compile_status);
    if(compile_status == GL_FALSE)
    {
        GLint logLength;
        glGetShaderiv(handle(), GL_INFO_LOG_LENGTH, &logLength);
        GLchar* infoLog = (GLchar*)malloc(logLength*sizeof(GLchar));
        glGetShaderInfoLog(handle(), logLength, nullptr, infoLog);
        printf("SHADER FAILED TO COMPILE: %s\n", infoLog);
        free(infoLog);
        return false;
    }
    return true;
}
