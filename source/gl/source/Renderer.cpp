#include "Renderer.h"
#include "Defines.h"

#include <stdio.h>

using namespace sk;

void APIENTRY Renderer::DebugLogCallback(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar* message, const void* userParam) {
    const char *source_s, *type_s, *severity_s;
    switch(source)
    {
    case GL_DEBUG_SOURCE_API:             source_s = "Core: ";            break;
    case GL_DEBUG_SOURCE_WINDOW_SYSTEM:   source_s = "Window: ";          break;
    case GL_DEBUG_SOURCE_SHADER_COMPILER: source_s = "Shader Compiler: "; break;
    case GL_DEBUG_SOURCE_THIRD_PARTY:     source_s = "Third Party: ";     break;
    case GL_DEBUG_SOURCE_APPLICATION:     source_s = "User: ";            break;
    case GL_DEBUG_SOURCE_OTHER:           source_s = "Other: ";           break;
    default: source_s = ""; break;
    }
    switch(type)
    {
    case GL_DEBUG_TYPE_ERROR: type_s = "GL_DEBUG_TYPE_ERROR: "; break;
    case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR: type_s = "Deprecated: "; break;
    case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR: type_s = "Undefined behavior: "; break;
    case GL_DEBUG_TYPE_PORTABILITY: type_s = "Portability: "; break;
    case GL_DEBUG_TYPE_PERFORMANCE: type_s = "Performance: "; break;
    case GL_DEBUG_TYPE_MARKER: type_s = "GL_DEBUG_TYPE_MARKER: "; break;
    case GL_DEBUG_TYPE_PUSH_GROUP: type_s = "GL_DEBUG_TYPE_PUSH_GROUP: "; break;
    case GL_DEBUG_TYPE_POP_GROUP: type_s = "GL_DEBUG_TYPE_POP_GROUP: "; break;
    case GL_DEBUG_TYPE_OTHER: type_s = "GL_DEBUG_TYPE_OTHER: "; break;
    default: type_s = ""; break;
    }
    switch(severity)
    {
    case GL_DEBUG_SEVERITY_HIGH:         severity_s = "ERROR: "; break;
    case GL_DEBUG_SEVERITY_MEDIUM:       severity_s = "WARNING: "; break;
    case GL_DEBUG_SEVERITY_LOW:          severity_s = "NOTE: "; break;
    case GL_DEBUG_SEVERITY_NOTIFICATION: severity_s = ""; break;
    default: severity_s = ""; break;
    }
    printf("OpenGL (id: %d): %s%s%s%s\n", id, source_s, severity_s, type_s, message);
}

void sk::Renderer::init_msg() const {
    int glmajor, glminor;
    sscanf((const char*)glGetString(GL_VERSION), "%d.%d", &glmajor, &glminor);
    printf("Program Running OpenGL %d.%d\n", glmajor, glminor);
    printf("\n");
}

Renderer* Renderer::current = nullptr;

Renderer::Renderer() {
    if(!glfwInit())
    {
        printf("Couldn't initialize glfw.\n");
        return;
    }
    glfwWindowHint(GLFW_CLIENT_API, GLFW_OPENGL_API);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, SKGL_MAJOR);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, SKGL_MINOR);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint(GLFW_SAMPLES, 1);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
#if _DEBUG
    glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GLFW_TRUE);
#endif

    window = glfwCreateWindow(1, 1, "", nullptr, nullptr);
    if(!window)
    {
        printf("Couldn't create OpenGL context.\n");
        glfwTerminate();
        return;
    }
    glfwMakeContextCurrent(window);

    glewExperimental = true;
    if(glewInit() != GLEW_OK)
    {
        printf("Couldn't initialize glew.\n");
        glfwTerminate();
        return;
    }

    if(current) current->is_current = false;
    current = this;
    is_current = true;

    glGetIntegerv(GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS, &max_combined_texture_image_units);
    --max_combined_texture_image_units;
    glActiveTexture(GL_TEXTURE0 + max_combined_texture_image_units);

#if _DEBUG
    glEnable(GL_DEBUG_OUTPUT);
    glDebugMessageCallback((GLDEBUGPROC)&DebugLogCallback, 0);
    GLuint ids[1] = { 131185 };
    glDebugMessageControl(GL_DEBUG_SOURCE_API, GL_DEBUG_TYPE_OTHER, GL_DONT_CARE, 1, ids, GL_FALSE);
#endif
    init_msg();
}

Renderer::~Renderer() {
    if(is_current) current = nullptr;
    glfwDestroyWindow(window);
    glfwTerminate();
}

TextureObject Renderer::CreateTexture(GLenum target) {
    GLuint handle;
    CreateTextures(target, 1, &handle);
    return TextureObject(this, handle, target);
}

void Renderer::CreateTextures(GLenum target, GLsizei n, GLuint *textures) {
    if SKGL_VER(4, 5) {
        glCreateTextures(target, n, textures);
        return;
    }
    else
    {
        glGenTextures(n, textures);
        for(int i = 0; i < n; ++i)
            glBindTexture(target, textures[i]);
        glBindTexture(target, *Binding(target));
        return;
    }
}

void sk::Renderer::DeleteTextures(GLsizei n, const GLuint * textures) {
    glDeleteTextures(n, textures);
    for(int i = 0; i < 11; ++i)
        for(int j = 0; j < n; ++j)
            if(textures[j]==(&texture_1D_binding)[i]) (&texture_1D_binding)[i] = 0;
}

BufferObject Renderer::CreateBuffer(GLenum target) {
    GLuint handle;
    CreateBuffers(target, 1, &handle);
    return BufferObject(this, handle, target);
}

void Renderer::CreateBuffers(GLenum target, GLsizei n, GLuint *buffers) {
    if SKGL_VER(4, 5) {
        glCreateBuffers(n, buffers);
        return;
    }
    else
    {
        if(target==GL_ELEMENT_ARRAY_BUFFER) target = GL_ARRAY_BUFFER;
        GLint binding = *Binding(target);

        glGenBuffers(n, buffers);
        for(int i = 0; i < n; ++i)
            glBindBuffer(target, buffers[i]);
        glBindBuffer(target, binding);
        return;
    }
}

void sk::Renderer::DeleteBuffers(GLsizei n, const GLuint * buffers) {
    glDeleteBuffers(n, buffers);
    for(int i = 0; i < 12; ++i)
        for(int j = 0; j < n; ++j)
            if(buffers[j]==(&array_buffer_binding)[i]) (&array_buffer_binding)[i] = 0;
}

FramebufferObject Renderer::CreateFramebuffer() {
    GLuint handle;
    CreateFramebuffers(1, &handle);
    return FramebufferObject(this, handle);
}

void Renderer::CreateFramebuffers(GLsizei n, GLuint *ids) {
    if SKGL_VER(4, 5) {
        glCreateFramebuffers(n, ids);
        return;
    }
    else if SKGL_VER(3, 0) {
        glGenFramebuffers(n, ids);
        for(int i = 0; i < n; ++i)
            glBindFramebuffer(GL_READ_FRAMEBUFFER, ids[i]);
        glBindFramebuffer(GL_READ_FRAMEBUFFER, read_framebuffer_binding);
        return;
    }
}

void sk::Renderer::DeleteFramebuffers(GLsizei n, const GLuint * ids) {
    glDeleteFramebuffers(n, ids);
    for(int i = 0; i < n; ++i)
    {
        if(ids[i]==draw_framebuffer_binding) draw_framebuffer_binding = 0;
        if(ids[i]==read_framebuffer_binding) read_framebuffer_binding = 0;
    }
}

VertexArrayObject Renderer::CreateVertexArray() {
    GLuint handle;
    CreateVertexArrays(1, &handle);
    return VertexArrayObject(this, handle);
}

void Renderer::CreateVertexArrays(GLsizei n, GLuint *arrays) {
    if SKGL_VER(4, 5) {
        glCreateVertexArrays(n, arrays);
        return;
    }
    else if SKGL_VER(3, 0) {
        glGenVertexArrays(n, arrays);
        for(int i = 0; i < n; ++i)
            glBindVertexArray(arrays[i]);
        glBindVertexArray(vertex_array_binding);
        return;
    }
}

void sk::Renderer::DeleteVertexArrays(GLsizei n, const GLuint * arrays) {
    glDeleteVertexArrays(n, arrays);
    for(int i = 0; i < n; ++i)
        if(arrays[i]==vertex_array_binding) vertex_array_binding = 0;
}

GLuint* Renderer::Binding(GLenum target) {
    switch(target)
    {
        // Texture
    case GL_TEXTURE_1D: return &texture_1D_binding;
    case GL_TEXTURE_2D: return &texture_2D_binding;
    case GL_TEXTURE_3D: return &texture_3D_binding;
    case GL_TEXTURE_1D_ARRAY: return &texture_1D_array_binding;
    case GL_TEXTURE_2D_ARRAY: return &texture_2D_array_binding;
    case GL_TEXTURE_RECTANGLE: return &texture_rectangle_binding;
    case GL_TEXTURE_CUBE_MAP: return &texture_cube_map_binding;
    case GL_TEXTURE_CUBE_MAP_ARRAY: return &texture_cube_map_array_binding;
    case GL_TEXTURE_BUFFER: return &texture_buffer_binding;
    case GL_TEXTURE_2D_MULTISAMPLE: return &texture_2D_multisample_binding;
    case GL_TEXTURE_2D_MULTISAMPLE_ARRAY: return &texture_2D_multisample_array_binding;
        // Buffer
    case GL_ARRAY_BUFFER: return &array_buffer_binding;
    case GL_ATOMIC_COUNTER_BUFFER: return &atomic_counter_buffer_binding;
    case GL_COPY_READ_BUFFER: return &copy_read_buffer_binding;
    case GL_COPY_WRITE_BUFFER: return &copy_write_buffer_binding;
    case GL_DISPATCH_INDIRECT_BUFFER: return &dispatch_indirect_buffer_binding;
    case GL_DRAW_INDIRECT_BUFFER: return &draw_indirect_buffer_binding;
    case GL_PIXEL_PACK_BUFFER: return &pixel_pack_buffer_binding;
    case GL_PIXEL_UNPACK_BUFFER: return &pixel_unpack_buffer_binding;
    case GL_QUERY_BUFFER: return &query_buffer_binding;
    case GL_SHADER_STORAGE_BUFFER: return &shader_storage_buffer_binding;
    case GL_TRANSFORM_FEEDBACK_BUFFER: return &transform_feedback_buffer_binding;
    case GL_UNIFORM_BUFFER: return &uniform_buffer_binding;
        // Framebuffer
    case GL_DRAW_FRAMEBUFFER: return &draw_framebuffer_binding;
    case GL_READ_FRAMEBUFFER: return &read_framebuffer_binding;
    default: break;
    }
    return 0;
}

GLuint Renderer::GetBinding(GLenum target) const {
    return *const_cast<Renderer*>(this)->Binding(target);
}

GLuint Renderer::GetVertexArrayBinding() const {
    return vertex_array_binding;
}
GLuint Renderer::GetCurrentProgram() const {
    return current_program;
}

void Renderer::BindTextureUnit(GLuint unit, GLuint texture) {
    //if SKGL_VER(4, 5)
    //    glBindTextureUnit(unit, texture);
    //else
    {
        glActiveTexture(GL_TEXTURE0 + unit);

        glBindTexture(GL_TEXTURE_2D, texture);

        glActiveTexture(GL_TEXTURE0 + max_combined_texture_image_units);
        if(texture_2D_binding!=texture)
            glBindTexture(GL_TEXTURE_2D, texture_2D_binding);
    }
}

void Renderer::BindTexture(GLenum target, GLuint texture) {
    GLuint* binding = Binding(target);
    if(*binding==texture) return;
    *binding = texture;
    glBindTexture(target, texture);
}

void Renderer::BindBuffer(GLenum target, GLuint buffer) {
    GLuint* binding = Binding(target);
    if(*binding==buffer) return;
    *binding = buffer;
    glBindBuffer(target, buffer);
}

void Renderer::BindFramebuffer(GLenum target, GLuint fb) {
    if(target==GL_FRAMEBUFFER)
    {
        if(draw_framebuffer_binding==fb && read_framebuffer_binding==fb) return;
        draw_framebuffer_binding = fb;
        read_framebuffer_binding = fb;
    }
    else
    {
        GLuint* binding = Binding(target);
        if(*binding==fb) return;
        *binding = fb;
    }
    glBindFramebuffer(target, fb);
}

void Renderer::BindVertexArray(GLuint array) {
    if(vertex_array_binding==array) return;
    vertex_array_binding = array;
    glBindVertexArray(array);
}

ShaderObject sk::Renderer::CreateShader(GLenum shaderType) {
    return ShaderObject(this, glCreateShader(shaderType), shaderType);
}

void sk::Renderer::DeleteShader(GLuint shader) {
    glDeleteShader(shader);
}

ProgramObject Renderer::CreateProgram() {
    return ProgramObject(this, glCreateProgram());
}

void sk::Renderer::DeleteProgram(GLuint program) {
    glDeleteProgram(program);
}

void Renderer::UseProgram(GLuint program) {
    if(current_program==program) return;
    current_program = program;
    glUseProgram(program);
}
