#include "TextureObject.h"
#include "Defines.h"

#include "Renderer.h"
#include "FramebufferObject.h"

void sk::TextureObject::Parameteri(GLenum pname, GLint param) {
    if SKGL_VER(4, 5) {
        glTextureParameteri(handle(), pname, param);
        return;
    }
    renderer().BindTexture(target, handle());
    glTexParameteri(target, pname, param);
}
void sk::TextureObject::Parameterf(GLenum pname, GLfloat param) {
    if SKGL_VER(4, 5) {
        glTextureParameterf(handle(), pname, param);
        return;
    }
    renderer().BindTexture(target, handle());
    glTexParameterf(target, pname, param);
}
void sk::TextureObject::Parameteriv(GLenum pname, GLint* param) {
    if SKGL_VER(4, 5) {
        glTextureParameteriv(handle(), pname, param);
        return;
    }
    renderer().BindTexture(target, handle());
    glTexParameteriv(target, pname, param);
}
void sk::TextureObject::Parameterfv(GLenum pname, GLfloat* param) {
    if SKGL_VER(4, 5) {
        glTextureParameterfv(handle(), pname, param);
        return;
    }
    renderer().BindTexture(target, handle());
    glTexParameterfv(target, pname, param);
}


sk::TextureObject::~TextureObject() {
    auto h = handle();
    renderer().DeleteTextures(1, &h);
}


void sk::TextureObject::SetDepthStencilMode(GLenum param) {
    if(depth_stencil_mode==param) return;
    depth_stencil_mode = param;
    Parameteri(GL_DEPTH_STENCIL_TEXTURE_MODE, param);
}

void sk::TextureObject::SetBaseLevel(GLint param) {
    if(base_level==param) return;
    base_level = param;
    Parameteri(GL_TEXTURE_BASE_LEVEL, param);
}

void sk::TextureObject::SetBorderColor(GLfloat param[4]) {
    bool same = true;
    for(int i = 0; i < 4; ++i)
    {
        if(border_color[i] != param[i]) same = false;
        border_color[i] = param[i];
    }
    if(same) return;
    Parameterfv(GL_TEXTURE_BASE_LEVEL, param);
}

void sk::TextureObject::SetCompareFunc(GLenum param) {
    if(compare_func==param) return;
    compare_func = param;
    Parameteri(GL_TEXTURE_COMPARE_FUNC, param);
}

void sk::TextureObject::SetCompareNode(GLenum param) {
    if(compare_mode==param) return;
    compare_mode = param;
    Parameteri(GL_TEXTURE_COMPARE_MODE, param);
}

void sk::TextureObject::SetLodBias(GLfloat param) {
    if(lod_bias==param) return;
    lod_bias = param;
    Parameterf(GL_TEXTURE_LOD_BIAS, param);
}

void sk::TextureObject::SetMinFilter(GLenum param) {
    if(min_filter==param) return;
    min_filter = param;
    Parameteri(GL_TEXTURE_MIN_FILTER, param);
}

void sk::TextureObject::SetMagFilter(GLenum param) {
    if(mag_filter==param) return;
    mag_filter = param;
    Parameteri(GL_TEXTURE_MAG_FILTER, param);
}

void sk::TextureObject::SetMinMagFilter(GLenum min_filter, GLenum mag_filter) {
    SetMinFilter(min_filter);
    SetMagFilter(mag_filter);
}
void sk::TextureObject::SetMinMagFilter(GLenum minmag_filter) {
    SetMinFilter(minmag_filter);
    SetMagFilter(minmag_filter);
}

void sk::TextureObject::SetMinLod(GLfloat param) {
    if(min_lod==param) return;
    min_lod = param;
    Parameterf(GL_TEXTURE_MIN_LOD, param);
}

void sk::TextureObject::SetMaxLod(GLfloat param) {
    if(max_lod==param) return;
    max_lod = param;
    Parameterf(GL_TEXTURE_MAX_LOD, param);
}

void sk::TextureObject::SetMaxLevel(GLint param) {
    if(max_level==param) return;
    max_level = param;
    Parameteri(GL_TEXTURE_MAX_LEVEL, param);
}

void sk::TextureObject::SetSwizzleR(GLenum param) {
    if(swizzle_r==param) return;
    swizzle_r = param;
    Parameteri(GL_TEXTURE_SWIZZLE_R, param);
}

void sk::TextureObject::SetSwizzleG(GLenum param) {
    if(swizzle_g==param) return;
    swizzle_g = param;
    Parameteri(GL_TEXTURE_SWIZZLE_G, param);
}

void sk::TextureObject::SetSwizzleB(GLenum param) {
    if(swizzle_b==param) return;
    swizzle_b = param;
    Parameteri(GL_TEXTURE_SWIZZLE_B, param);
}

void sk::TextureObject::SetSwizzleA(GLenum param) {
    if(swizzle_a==param) return;
    swizzle_a = param;
    Parameteri(GL_TEXTURE_SWIZZLE_A, param);
}

void sk::TextureObject::SetSwizzleRGBA(GLenum param[4]) {
    if(swizzle_r==param[0]) return;
    if(swizzle_g==param[1]) return;
    if(swizzle_b==param[2]) return;
    if(swizzle_a==param[3]) return;
    swizzle_r = param[0];
    swizzle_g = param[1];
    swizzle_b = param[2];
    swizzle_a = param[3];
    Parameteriv(GL_TEXTURE_SWIZZLE_RGBA, (GLint*)param);
}

void sk::TextureObject::SetWrapS(GLenum param) {
    if(wrap_s==param) return;
    wrap_s = param;
    Parameteri(GL_TEXTURE_WRAP_S, param);
}

void sk::TextureObject::SetWrapT(GLenum param) {
    if(wrap_t==param) return;
    wrap_t = param;
    Parameteri(GL_TEXTURE_WRAP_T, param);
}

void sk::TextureObject::SetWrapR(GLenum param) {
    if(wrap_r==param) return;
    wrap_r = param;
    Parameteri(GL_TEXTURE_WRAP_R, param);
}

void sk::TextureObject::GenerateMipmap() {
    if SKGL_VER(4, 5) {
        glGenerateTextureMipmap(handle());
        return;
    }
    renderer().BindTexture(target, handle());
    glGenerateMipmap(target);
}

GLsizei max(GLsizei a, GLsizei b) {
    return a > b ? a : b;
}

void sk::TextureObject::Storage1D(GLint levels, GLenum internalformat, GLsizei width) {
    if SKGL_VER(4, 5) {
        glTextureStorage1D(handle(), levels, internalformat, width);
        return;
    }
    renderer().BindTexture(target, handle());

    if SKGL_VER(4, 2)
        glTexStorage1D(target, levels, internalformat, width);
    else for(int i = 0; i < levels; i++)
    {
        glTexImage1D(target, i, internalformat, width, 0, 0, 0, NULL);
        width = max(1, (width / 2));
    }
}

void sk::TextureObject::Storage2D(GLint levels, GLenum internalformat, GLsizei width, GLsizei height) {
    if SKGL_VER(4, 5) {
        glTextureStorage2D(handle(), levels, internalformat, width, height);
        return;
    }
    renderer().BindTexture(target, handle());

    if SKGL_VER(4, 2)
        glTexStorage2D(target, levels, internalformat, width, height);
    else
    {
        GLenum format = GL_RGBA;
        if(internalformat==GL_DEPTH_COMPONENT) format = GL_DEPTH_COMPONENT;
        if(target==GL_TEXTURE_CUBE_MAP) for(int i = 0; i < levels; i++)
        {
            glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X, i, internalformat, width, height, 0, format, GL_UNSIGNED_BYTE, NULL);
            glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_X, i, internalformat, width, height, 0, format, GL_UNSIGNED_BYTE, NULL);
            glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Y, i, internalformat, width, height, 0, format, GL_UNSIGNED_BYTE, NULL);
            glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Y, i, internalformat, width, height, 0, format, GL_UNSIGNED_BYTE, NULL);
            glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Z, i, internalformat, width, height, 0, format, GL_UNSIGNED_BYTE, NULL);
            glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Z, i, internalformat, width, height, 0, format, GL_UNSIGNED_BYTE, NULL);
            width = max(1, (width / 2));
            height = max(1, (height / 2));
        }
        else for(int i = 0; i < levels; i++)
        {
            glTexImage2D(target, i, internalformat, width, height, 0, format, GL_UNSIGNED_BYTE, NULL);
            width = max(1, (width / 2));
            if(target!=GL_TEXTURE_1D && target!=GL_TEXTURE_1D_ARRAY)
                height = max(1, (height / 2));
        }
    }
}

void sk::TextureObject::Storage3D(GLint levels, GLenum internalformat, GLsizei width, GLsizei height, GLsizei depth) {
    if SKGL_VER(4, 5) {
        glTextureStorage3D(handle(), levels, internalformat, width, height, depth);
        return;
    }
    renderer().BindTexture(target, handle());

    if SKGL_VER(4, 2)
        glTexStorage3D(target, levels, internalformat, width, height, depth);
    else for(int i = 0; i < levels; i++)
    {
        glTexImage3D(target, i, internalformat, width, height, depth, 0, 0, 0, NULL);
        width = max(1, (width / 2));
        height = max(1, (height / 2));
        if(target==GL_TEXTURE_3D||target==GL_PROXY_TEXTURE_3D)
            depth = max(1, (depth / 2));
    }
}

void sk::TextureObject::Image1D(GLint level, GLint internalformat, GLsizei width, GLenum format, GLenum type, void * data) {
    renderer().BindTexture(target, handle());
    glTexImage1D(target, level, internalformat, width, 0, format, type, data);
}

void sk::TextureObject::Image2D(GLint level, GLint internalformat, GLsizei width, GLsizei height, GLenum format, GLenum type, void * data) {
    renderer().BindTexture(target, handle());
    glTexImage2D(target, level, internalformat, width, height, 0, format, type, data);
}

void sk::TextureObject::Image3D(GLint level, GLint internalformat, GLsizei width, GLsizei height, GLsizei depth, GLenum format, GLenum type, void * data) {
    renderer().BindTexture(target, handle());
    glTexImage3D(target, level, internalformat, width, height, depth, 0, format, type, data);
}

void sk::TextureObject::SubImage1D(GLint level, GLint xoffset, GLsizei width, GLenum format, GLenum type, void * data) {
    if SKGL_VER(4, 5)
        glTextureSubImage1D(handle(), level, xoffset, width, format, type, data);
    else
    {
        renderer().BindTexture(target, handle());
        glTexSubImage1D(target, level, xoffset, width, format, type, data);
    }
}

void sk::TextureObject::SubImage2D(GLint level, GLint xoffset, GLint yoffset, GLsizei width, GLsizei height, GLenum format, GLenum type, void * data) {
    if SKGL_VER(4, 5)
        glTextureSubImage2D(handle(), level, xoffset, yoffset, width, height, format, type, data);
    else
    {
        renderer().BindTexture(target, handle());
        glTexSubImage2D(target, level, xoffset, yoffset, width, height, format, type, data);
    }
}

void sk::TextureObject::SubImage3D(GLint level, GLint xoffset, GLint yoffset, GLint zoffset, GLsizei width, GLsizei height, GLsizei depth, GLenum format, GLenum type, void * data) {
    if SKGL_VER(4, 5)
        glTextureSubImage3D(handle(), level, xoffset, yoffset, zoffset, width, height, depth, format, type, data);
    else
    {
        renderer().BindTexture(target, handle());
        glTexSubImage3D(target, level, xoffset, yoffset, zoffset, width, height, depth, format, type, data);
    }
}

void sk::TextureObject::CopyImage1D(const FramebufferObject& framebuffer, GLint level, GLenum internalformat, GLint x, GLint y, GLsizei width) {
    renderer().BindFramebuffer(GL_READ_FRAMEBUFFER, framebuffer.handle());
    renderer().BindTexture(target, handle());
    glCopyTexImage1D(target, level, internalformat, x, y, width, 0);
}

void sk::TextureObject::CopyImage2D(const FramebufferObject& framebuffer, GLint level, GLenum internalformat, GLint x, GLint y, GLsizei width, GLsizei height) {
    renderer().BindFramebuffer(GL_READ_FRAMEBUFFER, framebuffer.handle());
    renderer().BindTexture(target, handle());
    glCopyTexImage2D(target, level, internalformat, x, y, width, height, 0);
}

void sk::TextureObject::CopySubImage1D(const FramebufferObject& framebuffer, GLint level, GLint xoffset, GLint x, GLint y, GLsizei width) {
    renderer().BindFramebuffer(GL_READ_FRAMEBUFFER, framebuffer.handle());

    if SKGL_VER(4, 5) {
        glCopyTextureSubImage1D(handle(), level, xoffset, x, y, width);
        return;
    }

    renderer().BindTexture(target, handle());
    glCopyTexSubImage1D(target, level, xoffset, x, y, width);
}

void sk::TextureObject::CopySubImage2D(const FramebufferObject& framebuffer, GLint level, GLint xoffset, GLint yoffset, GLint x, GLint y, GLsizei width, GLsizei height) {
    renderer().BindFramebuffer(GL_READ_FRAMEBUFFER, framebuffer.handle());

    if SKGL_VER(4, 5) {
        glCopyTextureSubImage2D(handle(), level, xoffset, yoffset, x, y, width, height);
        return;
    }

    renderer().BindTexture(target, handle());
    glCopyTexSubImage2D(target, level, xoffset, yoffset, x, y, width, height);
}

void sk::TextureObject::CopySubImage3D(const FramebufferObject& framebuffer, GLint level, GLint xoffset, GLint yoffset, GLint zoffset, GLint x, GLint y, GLsizei width, GLsizei height) {
    renderer().BindFramebuffer(GL_READ_FRAMEBUFFER, framebuffer.handle());

    if SKGL_VER(4, 5) {
        glCopyTexSubImage3D(handle(), level, xoffset, yoffset, zoffset, x, y, width, height);
        return;
    }

    renderer().BindTexture(target, handle());
    glCopyTextureSubImage3D(target, level, xoffset, yoffset, zoffset, x, y, width, height);
}
