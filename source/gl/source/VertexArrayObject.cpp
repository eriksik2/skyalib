#include "VertexArrayObject.h"
#include "Defines.h"

#include "Renderer.h"

sk::VertexArrayObject::~VertexArrayObject() {
    auto h = handle();
    renderer().DeleteVertexArrays(1, &h);
}

void sk::VertexArrayObject::ElementBuffer(GLuint buffer) {
    if(element_array_buffer_binding == buffer) return;
    if SKGL_VER(4, 5)
        glVertexArrayElementBuffer(handle(), buffer);
    else
    {
        renderer().BindVertexArray(handle());
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buffer);
    }
    element_array_buffer_binding = buffer;
}

void sk::VertexArrayObject::EnableAttributeArray(GLuint attributeindex) {
    if SKGL_VER(4, 5)
        glEnableVertexArrayAttrib(handle(), attributeindex);
    else
    {
        renderer().BindVertexArray(handle());
        glEnableVertexAttribArray(attributeindex);
    }
}

void sk::VertexArrayObject::DisableAttributeArray(GLuint attributeindex) {
    if SKGL_VER(4, 5)
        glDisableVertexArrayAttrib(handle(), attributeindex);
    else
    {
        renderer().BindVertexArray(handle());
        glDisableVertexAttribArray(attributeindex);
    }
}

void sk::VertexArrayObject::AttributePointer(GLuint buffer, GLuint attributeindex, GLint size, GLenum type, GLboolean normalized, GLsizei stride, const GLvoid * pointer) {
    renderer().BindVertexArray(handle());
    renderer().BindBuffer(GL_ARRAY_BUFFER, buffer);
    glVertexAttribPointer(attributeindex, size, type, normalized, stride, pointer);
}
