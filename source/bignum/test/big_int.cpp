#include "gtest/gtest.h"

#include <sk/bignum.h>

TEST(big_int, ctor) {
    sk::big_int<64> big1; // def ctor
    EXPECT_EQ(int(big1), 0);
    
    sk::big_int<64> big2 = 265536; // widening value ctor
    EXPECT_EQ(int(big2), 265536);
    
    sk::big_int<32> big3(265532l); // narrowing value ctor
    EXPECT_EQ(int(big3), 265532);
    
    sk::big_int<64> big4 = big3; // widening copy ctor
    EXPECT_EQ(int(big4), 265532);
    
    big4 = big2;
    EXPECT_EQ(int(big4), 265536);
    big4 = big1;
    EXPECT_EQ(int(big4), 0);
    big4 = 2655369;
    EXPECT_EQ(int(big4), 2655369);
    
    sk::big_int<32> big5(big2); // narrowing copy ctor
    EXPECT_EQ(int(big5), 265536);
}

TEST(big_int, add) {
    sk::big_int<sizeof(int)*8> a(sk::limits<int>::max);
    sk::big_int<128> b(sk::limits<int>::max + 10ll);
    
    static_assert(sk::is_same<decltype(a + b  ), decltype(b)>, "");
    static_assert(sk::is_same<decltype(a + 0ll), long long>,   "");
    static_assert(sk::is_same<decltype(a + 0  ), decltype(a)>, "");
    static_assert(sk::is_same<decltype(b + a  ), decltype(b)>, "");
    static_assert(sk::is_same<decltype(b + 0ll), decltype(b)>, "");
    static_assert(sk::is_same<decltype(b + 0  ), decltype(b)>, "");
    
    long long vb = sk::limits<int>::max*2ll + 10ll;
    EXPECT_EQ((long long)(a + b), vb);
    
    auto res = a + b;
    EXPECT_EQ((long long)(res), vb);
    
    res += sk::limits<int>::max;
    EXPECT_EQ((long long)(res), vb + (long long)(sk::limits<int>::max));
}

TEST(big_int, subtract) {
    sk::big_int<sizeof(int)*8> a(sk::limits<int>::max);
    sk::big_int<128> b((long long)(sk::limits<int>::max) + 1324ll);
    
    static_assert(sk::is_same<decltype(a + b  ), decltype(b)>, "");
    static_assert(sk::is_same<decltype(a + 0ll), long long>,   "");
    static_assert(sk::is_same<decltype(a + 0  ), decltype(a)>, "");
    static_assert(sk::is_same<decltype(b + a  ), decltype(b)>, "");
    static_assert(sk::is_same<decltype(b + 0ll), decltype(b)>, "");
    static_assert(sk::is_same<decltype(b + 0  ), decltype(b)>, "");
    
    long long vb = -1324;
    EXPECT_EQ((long long)(a - b), vb);
    EXPECT_EQ((long long)(b - a), -vb);
    
    auto res = b - a;
    res -= sk::limits<int>::max;
    EXPECT_EQ((long long)(res), -vb - (long long)(sk::limits<int>::max));
}

TEST(big_int, ones_compliment) {
    sk::big_int<128> a(392457098723457834);
    EXPECT_EQ((long long)(a), 392457098723457834);
    EXPECT_EQ((long long)(~a), ~392457098723457834);
    
    sk::big_int<16> b(15);
    EXPECT_EQ(int(b), 15);
    EXPECT_EQ(int(~b), ~15);
}

TEST(big_int, unary_minus) {
    sk::big_int<128> a(392457098723457834ll);
    EXPECT_EQ((long long)(a), 392457098723457834ll);
    EXPECT_EQ((long long)(-a), -392457098723457834ll);
    
    sk::big_int<16> b(15);
    EXPECT_EQ(int(b), 15);
    EXPECT_EQ(int(-b), -15);
}

TEST(big_int, left_shift) {
    sk::big_int<128> a(1);
    EXPECT_EQ((long long)(a), 1);
    
    auto b = a << (sizeof(long long)*8 - 1);
    EXPECT_EQ((long long)(b), 1ll << (sizeof(long long)*8 - 1));
    
    auto c = a << (sizeof(unsigned int)*8);
    EXPECT_EQ((long long)(c), 1ll << (sizeof(unsigned int)*8));
    
    auto d = a << 23;
    EXPECT_EQ((long long)(d), 1ll << 23);
}

TEST(big_int, right_shift) {
    sk::big_int<128> a(sk::limits<long long>::max);
    EXPECT_EQ((long long)(a), sk::limits<long long>::max);
    
    auto b = a >> (sizeof(long long)*8 - 3);
    EXPECT_EQ((long long)(b), sk::limits<long long>::max >> (sizeof(long long)*8 - 3));
    
    auto c = a >> (sizeof(unsigned int)*8);
    EXPECT_EQ((long long)(c), sk::limits<long long>::max >> (sizeof(unsigned int)*8));
    
    auto d = a >> 23;
    EXPECT_EQ((long long)(d), sk::limits<long long>::max >> 23);
}

TEST(big_int, multiplication) {
    
    auto t0 = sk::big_int<128>(10) * sk::big_int<128>(15);
    EXPECT_EQ((long long)(t0), 150);
    
    auto t1 = sk::big_int<128>(sk::limits<int>::max) * sk::big_int<128>(15);
    EXPECT_EQ((long long)(t1), sk::limits<int>::max*15ll);
    
    sk::big_int<128> t2 = sk::limits<long long>::max/10;
    EXPECT_EQ((long long)(t2), sk::limits<long long>::max/10);
    
    auto b = t2 * sk::big_int<128>(10);
    EXPECT_EQ((long long)(b), sk::limits<long long>::max);
}
