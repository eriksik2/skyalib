#pragma once

#include <sk/utility.h>
#include <sk/type_traits.h>

#include <sk/statarray.h>

namespace sk {
    namespace impl {
        template<sk::size_t byte_length>
        class big_int_impl {
        public:
            using byte_t = unsigned int;
            using big_t = unsigned long long;
            constexpr static int array_length = 1 + (byte_length - 1)/sizeof(byte_t);
            constexpr static int size = array_length*sizeof(byte_t);
            
            using type = sk::conditional<(size > sizeof(byte_t)), big_int_impl, byte_t>;
        private:
            sk::statarray<array_length, byte_t> m_data;
        public:
            
            constexpr big_int_impl() {}
            
            template<sk::size_t Isize2, sk::enable_if<Isize2 <= size> = 0> // widening copy constructor
            constexpr big_int_impl(const big_int_impl<Isize2>& copy) : m_data(copy.data()) {}
            
            template<sk::size_t Isize2, sk::enable_if<(Isize2 > size)> = 0> // narrowing copy constructor
            explicit constexpr big_int_impl(const big_int_impl<Isize2>& copy) : m_data(copy.data()) {}
            
            
            template<class T, sk::enable_if<sk::is_arithmetic<T> && sizeof(T) <= size> = 0> // widening value constructor
            constexpr big_int_impl(T val) {
                if constexpr(sizeof(T) <= sizeof(byte_t)){
                    m_data[0] = *reinterpret_cast<byte_t*>((void*)&val);
                }else{
                    *static_cast<T*>((void*)&m_data[0]) = val;
                }
            }
            
            template<class T, sk::enable_if<sk::is_arithmetic<T> && (sizeof(T) > size)> = 0> // narrowing value constructor
            explicit constexpr big_int_impl(T val) {
                sk::statarray<sizeof(T), byte_t> buf;
                *reinterpret_cast<T*>((void*)&buf[0]) = val;
                m_data = buf;
            }
            
            template<class T, sk::enable_if<sk::is_arithmetic<T> && (sizeof(T) <= size)> = 0> // narrowing conversion
            explicit constexpr operator T() const {
                return *reinterpret_cast<T*>((void*)&m_data[0]);
            }
            
            template<class T, sk::enable_if<sk::is_arithmetic<T> && (sizeof(T) > size)> = 0> // widening conversion
            constexpr operator T() const {
                sk::statarray<sizeof(T), byte_t> buf(m_data);
                return *reinterpret_cast<T*>((void*)&buf[0]);
            }
            
            template<class T, sk::enable_if<sk::is_arithmetic<T> && sizeof(T) <= size> = 0> // widening value assignment
            constexpr big_int_impl& operator=(T val) {
                *reinterpret_cast<T*>((void*)&m_data[0]) = val;
                return *this;
            }
            
            // big_int addition assignment. Only for big_ints equal to or smaller than this, bigger big_ints have to be explicitly cast down.
            template<sk::size_t Ismaller, sk::enable_if<(Ismaller <= size)> = 0>
            constexpr big_int_impl& operator+=(const big_int_impl<Ismaller>& rhs) {
                big_t carry = 0;
                for(int i = 0; (i < rhs.array_length || carry) && i < array_length; ++i){
                    big_t rhsval = (i < rhs.array_length) ? big_t(rhs.data()[i]) : 0;
                    big_t value = big_t(m_data[i]) + rhsval + carry;
                    if(value <= sk::limits<byte_t>::max) carry = 0;
                    else carry = value/sk::limits<byte_t>::max;
                    m_data[i] = value;
                }
                return *this;
            }
            // arithmetic addition assignment. Same as above, only smaller or equal types.
            template<class T, sk::enable_if<sk::is_arithmetic<T> && (sizeof(T) <= size)> = 0>
            constexpr big_int_impl& operator+=(T rhs) {
                return operator+=(big_int_impl<sizeof(T)>(rhs));
            }
            
            // big_int subtraction assignment. Only for big_ints equal to or smaller than this, bigger big_ints have to be explicitly cast down.
            template<sk::size_t Ismaller, sk::enable_if<(Ismaller <= size)> = 0>
            constexpr big_int_impl& operator-=(const big_int_impl<Ismaller>& rhs) {
                big_t carry = 0;
                for(int i = 0; (i < rhs.array_length || carry) && i < array_length; ++i){
                    big_t rhsval = (i < rhs.array_length) ? big_t(rhs.data()[i]) : 0;
                    big_t value = (big_t(m_data[i]) - rhsval) + carry;
                    if(value <= sk::limits<byte_t>::max) carry = 0;
                    else carry = (value - sk::limits<byte_t>::max)/sk::limits<byte_t>::max;
                    m_data[i] = value;
                }
                return *this;
            }
            // arithmetic subtraction assignment. Same as above, only smaller or equal types.
            template<class T, sk::enable_if<sk::is_arithmetic<T> && (sizeof(T) <= size)> = 0>
            constexpr big_int_impl& operator-=(T rhs) {
                return operator-=(big_int_impl<sizeof(T)>(rhs));
            }
            
            // ones compliment
            constexpr big_int_impl operator~() const & { // const lvalue
                big_int_impl res(*this); // Make a copy of this object.
                for(int i = 0; i < array_length; ++i)
                    res.m_data[i] = ~res.m_data[i]; // Perform ones compliment on each induvidual int32
                                                    // on copied object.
                return res; // Return copied object.
            }
            constexpr big_int_impl&& operator~() && { // rvalue
                for(int i = 0; i < array_length; ++i)
                    m_data[i] = ~m_data[i]; // Perform ones compliment on each induvidual int32 on this object.
                return sk::move(*this); // Return an rvalue reference to this object.
            }
            
            // unary minus / twos compliment
            constexpr auto operator-() const & { // const lvalue
                // Twos compliment is just Ones compliment + 1.
                return ~big_int_impl(*this) + 1;
            }
            constexpr big_int_impl&& operator-() && { // rvalue
                // Pass *this to operator~ as an rvalue, add assign it by one, then return it as an rvalue.
                return sk::move(~sk::move(*this) += 1);
            }
            
            // left shift operator
            constexpr big_int_impl operator<<(int bits) const & { // const lvalue
                return big_int_impl(*this).operator<<(bits);
            }
            constexpr big_int_impl&& operator<<(int bits) && { // rvalue
                constexpr int byte_size = sizeof(byte_t)*8;
                if(bits >= byte_size){
                    for(int i = array_length - 2; i >= 0; --i)
                        m_data[i + 1] = m_data[i];
                    m_data[0] = 0;
                    if(bits == byte_size) return sk::move(*this);
                    return sk::move(*this).operator<<(bits - byte_size);
                }
                byte_t carry = 0;
                for(int i = 0; i < array_length; ++i){
                    byte_t last_carry = carry;
                    carry = m_data[i] >> (byte_size - bits);
                    m_data[i] = (m_data[i] << bits) + last_carry;
                }
                return sk::move(*this);
            }
            
            // right shift operator
            constexpr big_int_impl operator>>(int bits) const & { // const lvalue
                return big_int_impl(*this).operator>>(bits);
            }
            constexpr big_int_impl&& operator>>(int bits) && { // rvalue
                constexpr int byte_size = sizeof(byte_t)*8;
                if(bits >= byte_size){
                    for(int i = 0; i < array_length - 1; ++i)
                        m_data[i] = m_data[i + 1];
                    m_data[array_length - 1] = 0;
                    if(bits == byte_size) return sk::move(*this);
                    return sk::move(*this).operator>>(bits - byte_size);
                }
                byte_t carry = 0;
                for(int i = array_length - 1; i >= 0; --i){
                    byte_t last_carry = carry;
                    carry = m_data[i] << (byte_size - bits);
                    m_data[i] = (m_data[i] >> bits) + last_carry;
                }
                return sk::move(*this);
            }
            
            constexpr const sk::statarray<array_length, byte_t>& data() const { return m_data; }
            constexpr sk::statarray<array_length, byte_t>& data() { return m_data; }
        };
        
        // MULTIPLICATION
        
        template<sk::size_t s1, sk::size_t s2, sk::size_t big = (s1 > s2 ? s1 : s2)>
        constexpr big_int_impl<big> operator*(const big_int_impl<s1>& lhs, const big_int_impl<s2>& rhs) {
            auto x1 = typename big_int_impl<s1/2>::type(lhs >> (big_int_impl<s1>::size*4));
            auto x2 = typename big_int_impl<s1/2>::type((lhs << (big_int_impl<s1>::size*4)) >> (big_int_impl<s1>::size*4));
            auto y1 = typename big_int_impl<s2/2>::type(rhs >> (big_int_impl<s2>::size*4));
            auto y2 = typename big_int_impl<s2/2>::type((rhs << (big_int_impl<s2>::size*4)) >> (big_int_impl<s2>::size*4));
            
            auto z2 = x1 * y1;
            auto z0 = x2 * y2;
            auto z1 = (x1 + x2)*(y1 + y2) - z2 - z0;//x1*y2 + x2*y1;
            
            constexpr auto base = sk::limits<typename big_int_impl<big>::byte_t>::max;
            return big_int_impl<big>(z2*(base*base) + z1*base + z0);
        }
        
        template<sk::size_t s, class T, sk::enable_if<sk::is_arithmetic<T>> = 0>
        constexpr big_int_impl<s> operator*(const big_int_impl<s>& lhs, T rhs) {
            auto x1 = static_cast<typename big_int_impl<s/2+1>::type>(lhs >> (big_int_impl<s>::size*4));
            auto x2 = static_cast<typename big_int_impl<s/2+1>::type>((lhs << (big_int_impl<s>::size*4)) >> (big_int_impl<s>::size*4));
            auto y1 = rhs >> (sizeof(T)*4);
            auto y2 = (rhs << (sizeof(T)*4)) >> (sizeof(T)*4);
            
            auto z2 = x1 * y1;
            auto z0 = x2 * y2;
            auto z1 = (x1 + x2)*(y1 + y2) - z2 - z0;
            
            constexpr auto base = sk::limits<typename big_int_impl<s>::byte_t>::max;
            return big_int_impl<s>(z2*(base*base) + z1*base + z0);
        }
        template<sk::size_t s, class T, sk::enable_if<sk::is_arithmetic<T>> = 0>
        constexpr big_int_impl<s> operator*(T lhs, const big_int_impl<s>& rhs){
            return rhs * lhs;
        }
        
        // ADDITION
        
        // const big_int + big arithmetic
        template<sk::size_t s, class T, sk::enable_if<sk::is_arithmetic<T> && (sizeof(T) > big_int_impl<s>::size)> = 0>
        constexpr T operator+(const big_int_impl<s>& lhs, T rhs) { return T(lhs) + rhs; }
        template<sk::size_t s, class T, sk::enable_if<sk::is_arithmetic<T> && (sizeof(T) > big_int_impl<s>::size)> = 0>
        constexpr T operator+(T lhs, const big_int_impl<s>& rhs) { return lhs + T(rhs); }
        
        // const big_int + small arithmetic
        template<sk::size_t s, class T, sk::enable_if<sk::is_arithmetic<T> && (sizeof(T) <= big_int_impl<s>::size)> = 0>
        constexpr big_int_impl<s> operator+(const big_int_impl<s>& lhs, T rhs) { return big_int_impl<s>(lhs) += rhs; }
        template<sk::size_t s, class T, sk::enable_if<sk::is_arithmetic<T> && (sizeof(T) <= big_int_impl<s>::size)> = 0>
        constexpr big_int_impl<s> operator+(T lhs, const big_int_impl<s>& rhs) { return big_int_impl<s>(rhs) += lhs; }
        
        // nonconst big_int + small arithmetic
        template<sk::size_t s, class T, sk::enable_if<sk::is_arithmetic<T> && (sizeof(T) <= big_int_impl<s>::size)> = 0>
        constexpr big_int_impl<s>&& operator+(big_int_impl<s>&& lhs, T rhs) { return sk::move(lhs += rhs); }
        template<sk::size_t s, class T, sk::enable_if<sk::is_arithmetic<T> && (sizeof(T) <= big_int_impl<s>::size)> = 0>
        constexpr big_int_impl<s>&& operator+(T lhs, big_int_impl<s>&& rhs) { return sk::move(rhs += lhs); }
        
        // const big_int + const big_int
        template<sk::size_t s1, sk::size_t s2, sk::size_t big = (s1 > s2) ? s1 : s2>
        constexpr big_int_impl<big> operator+(const big_int_impl<s1>& lhs, const big_int_impl<s2>& rhs) { return big_int_impl<big>(lhs) += rhs; }
        
        // nonconst big big_int + const small big_int
        template<sk::size_t big, sk::size_t small, sk::enable_if<(big_int_impl<big>::size > big_int_impl<small>::size)> = 0>
        constexpr big_int_impl<big>&& operator+(big_int_impl<big>&& lhs, const big_int_impl<small>& rhs) { return sk::move(lhs += rhs); }
        template<sk::size_t big, sk::size_t small, sk::enable_if<(big_int_impl<big>::size > big_int_impl<small>::size)> = 0>
        constexpr big_int_impl<big>&& operator+(const big_int_impl<small>& lhs, big_int_impl<big>&& rhs) { return sk::move(rhs += lhs); }
        
        // SUBTRACTION
        
        // const big_int - big arithmetic
        template<sk::size_t s, class T, sk::enable_if<sk::is_arithmetic<T> && (sizeof(T) > big_int_impl<s>::size)> = 0>
        constexpr T operator-(const big_int_impl<s>& lhs, T rhs) { return T(lhs) - rhs; }
        template<sk::size_t s, class T, sk::enable_if<sk::is_arithmetic<T> && (sizeof(T) > big_int_impl<s>::size)> = 0>
        constexpr T operator-(T lhs, const big_int_impl<s>& rhs) { return lhs - T(rhs); }
        
        // const big_int - small arithmetic
        template<sk::size_t s, class T, sk::enable_if<sk::is_arithmetic<T> && (sizeof(T) <= big_int_impl<s>::size)> = 0>
        constexpr big_int_impl<s> operator-(const big_int_impl<s>& lhs, T rhs) { return big_int_impl<s>(lhs) -= rhs; }
        template<sk::size_t s, class T, sk::enable_if<sk::is_arithmetic<T> && (sizeof(T) <= big_int_impl<s>::size)> = 0>
        constexpr big_int_impl<s> operator-(T lhs, const big_int_impl<s>& rhs) { return big_int_impl<s>(lhs) -= rhs; }
        
        // nonconst big_int - small arithmetic
        template<sk::size_t s, class T, sk::enable_if<sk::is_arithmetic<T> && (sizeof(T) <= big_int_impl<s>::size)> = 0>
        constexpr big_int_impl<s>&& operator-(big_int_impl<s>&& lhs, T rhs) { return sk::move(lhs -= rhs); }
        template<sk::size_t s, class T, sk::enable_if<sk::is_arithmetic<T> && (sizeof(T) <= big_int_impl<s>::size)> = 0>
        constexpr big_int_impl<s>&& operator-(T lhs, big_int_impl<s>&& rhs) { return sk::move((-rhs) += lhs); }
        
        // const big_int - const big_int
        template<sk::size_t s1, sk::size_t s2, sk::size_t big = (s1 > s2) ? s1 : s2>
        constexpr big_int_impl<big> operator-(const big_int_impl<s1>& lhs, const big_int_impl<s2>& rhs) { return big_int_impl<big>(lhs) -= rhs; }
        
        // nonconst big big_int - const small big_int
        template<sk::size_t big, sk::size_t small, sk::enable_if<(big_int_impl<big>::size > big_int_impl<small>::size)> = 0>
        constexpr big_int_impl<big>&& operator-(big_int_impl<big>&& lhs, const big_int_impl<small>& rhs) { return sk::move(lhs -= rhs); }
        template<sk::size_t big, sk::size_t small, sk::enable_if<(big_int_impl<big>::size > big_int_impl<small>::size)> = 0>
        constexpr big_int_impl<big>&& operator-(const big_int_impl<small>& lhs, big_int_impl<big>&& rhs) { return sk::move((-rhs) += lhs); }
        
        template<sk::size_t Ibits>
        using big_int = big_int_impl<Ibits/8>;
    }
    
    template<sk::size_t Ibits>
    using big_int = impl::big_int<Ibits>;
}
