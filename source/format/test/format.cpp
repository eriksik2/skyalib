#include "gtest/gtest.h"

#include <sk/format.h>

TEST(format, format) {
    EXPECT_EQ(sk::format("Hello {}!", "World"), "Hello World!");
    
    EXPECT_EQ(sk::format("Hello {} {}!", "World", 5), "Hello World 5!");
    
    EXPECT_EQ(sk::format("Hello {1 } {  0}{ }", sk::string("World"), 5), "Hello 5 World5");
    
    EXPECT_EQ(sk::format("{1} {}{0}", sk::string(" last"), "first", 2), "first 2 last");
}
