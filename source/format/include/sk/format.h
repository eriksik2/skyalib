#pragma once

#include <sk/array_view.h>
#include <sk/string.h>

#include <stdio.h> // fwrite, fputs, sscanf

namespace sk {
    template<class Tfirst, class...Targs>
    sk::string format(sk::const_string_view fmt, Tfirst&& arg1, Targs&&...args) {
        sk::string fmt_args[sizeof...(Targs) + 1] =
            { to_string(sk::forward<Tfirst>(arg1)), to_string(sk::forward<Targs>(args))... };
        sk::string output = fmt;
        int arg = -1, offset = 0, count = 0, count2 = 0;
        char* match = nullptr;
        while((match = strchr(output.begin() + offset, '{'))){
            offset = match - output.begin();
            if(!sscanf(match, "{ %n%d }%n", &count, &arg, &count)){
                sscanf(match + count, " }%n", &count2);
                count += count2;
                ++arg;
            }
            assert(arg >= 0 && arg < sizeof...(Targs) + 1);
            offset += fmt_args[arg].size();
            output.replace(match, count, fmt_args[arg].c_str());
        }
        return output;
    }
    
    template<class...Targs>
    void print(FILE* stream, sk::const_string_view fmt, Targs&&...args){
        sk::string fmt_args[sizeof...(Targs)] = { to_string(sk::forward<Targs>(args))... };
        int arg = -1;
        const char* match = fmt.begin(), *last = fmt.begin();
        while((match = strchr(last, '{'))){
            if(!sscanf(match, "{ %d }", &arg)) ++arg;
            assert(arg >= 0 && arg < sizeof...(Targs));
            fwrite(last, sizeof(char), match - last, stream);
            fwrite(fmt_args[arg].c_str(), sizeof(char), fmt_args[arg].size()+1, stream);
            last = strchr(match, '}')+1;
        }
        fputs(last, stream);
    }
    template<class...Targs>
    void print(sk::const_string_view fmt, Targs&&...args){
        print(stdout, fmt, sk::forward<Targs>(args)...);
    }
    
    template<class...Targs>
    void println(FILE* stream, sk::const_string_view fmt, Targs&&...args){
        print(stream, fmt, sk::forward<Targs>(args)...);
        fputc('\n', stream);
    }
    template<class...Targs>
    void println(sk::const_string_view fmt, Targs&&...args){
        print(stdout, fmt, sk::forward<Targs>(args)...);
        fputc('\n', stdout);
    }
}
