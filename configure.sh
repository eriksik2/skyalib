#!/bin/sh

# This script is called by CMake

# Make sure working directory is consistent
cd `dirname $0`

if [ -d "source/gl" ]; then
    # glew requires make to be called in glew/auto/ then in glew/, or cmake will fail when configuring.
    cd "source/gl/deps/glew/auto" || exit 1
    make || exit 1
    cd ".."
    # glew.pc gets remade everytime make is run, even if it exists, so run make silently here
    make --silent || exit 1
    # Back to root (skyalib)
    cd "../../../.."
    # glew wont compile on some platforms/configurations because of compile flags -nodefaultlib -noentry
    if ! git apply --reverse --check --ignore-space-change --ignore-whitespace "glew_fix.patch" 2> /dev/null
    then
        git apply --ignore-space-change --ignore-whitespace "glew_fix.patch" || exit 1
    fi
fi